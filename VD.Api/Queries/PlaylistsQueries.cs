﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VD.Api.Queries
{
    public class PlaylistsQueries : VDApiQueryBase
    {
    }

    public class PlaylistListQuery:PlaylistsQueries
    {
        public bool? IsPublic { get; set; }
    }

    public class PlaylistByCategory : PlaylistsQueries
    {
        public bool? IsPublic { get; set; }
        public int CategoryId { get; set; }
    }
}