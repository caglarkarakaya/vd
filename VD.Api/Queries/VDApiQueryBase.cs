﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VD.Api.Queries
{
    public class VDApiQueryBase
    {
        public int? Skip { get; set; }
        public int? Top { get; set; }
    }
}