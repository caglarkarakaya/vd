﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VD.Api.Queries
{
    public class VideosQueries : VDApiQueryBase
    {
    }

    public class HottestQuery : VideosQueries
    {

    }

    public class TrendingQuery : VideosQueries
    {

    }

    public class VideoInfoQuery : VideosQueries
    {

    }

    public class VideoInfoByChannelAndCategoryQuery : VideosQueries
    {
        public int ChannelId { get; set; }
        public int CategoryId { get; set; }
    }

    public class UpNextQuery : VideosQueries
    {
        public string SlugKey { get; set; }
        public int ChannelId { get; set; }
        public int CategoryId { get; set; }
    }

    public class PlaylistVideoQuery : VideosQueries
    {
        public int PlaylistId { get; set; }
    }
}