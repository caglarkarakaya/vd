﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using VD.Api.ActionFilters;

namespace VD.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            //GlobalConfiguration.Configuration.Formatters.Add(new System.Net.Http.Formatting.JsonMediaTypeFormatter());//json

            //var formatResolver = new System.Net.Http.Formatting.JsonMediaTypeFormatter();
            //formatResolver.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //formatResolver.UseDataContractJsonSerializer = false;

            //GlobalConfiguration.Configuration.Formatters.Add(formatResolver);//json
            //GlobalConfiguration.Configuration.Filters.Add(new LogActionWebApiFilter());
        }
    }
}
