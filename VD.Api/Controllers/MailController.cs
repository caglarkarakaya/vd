﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VD.Business.Concrete;
using VD.Common.Models;

namespace VD.Api.Controllers
{
    public class MailController : VDApiControllerBase
    {
        [HttpPost]
        [Route("ContactUs")]
        public async Task<bool> ContactUs(ContactUsMailModel contactUsForm)
        {
            try
            {
                MailBusinessObject mailBusinessObject = new MailBusinessObject();

                return await mailBusinessObject.SendContactUsMail(contactUsForm);
            }
            catch
            {
                return false;
            }
        }
    }
}
