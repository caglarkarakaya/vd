﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VD.Api.Queries;
using VD.Business.Concrete;
using VD.DTO.Concrete;

namespace VD.Api.Controllers
{
    public class ChannelsController : VDApiControllerBase
    {
        [HttpGet]
        [Route("Channels/ChannelList")]
        public async Task<ChannelsDTO> GetChannelList([FromUri] ChannelsQueries query)
        {
            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            PartnerBusinessObject _partnerBusinessObject = new PartnerBusinessObject();
            return await _partnerBusinessObject.GetChannelInfosAsync(skip, take);
        }

        [HttpGet]
        [Route("Channels/ChannelInfo/{channelId}")]
        public async Task<ChannelInfoDTO> GetChannelInfo(int channelId)
        {
            PartnerBusinessObject _partnerBusinessObject = new PartnerBusinessObject();
            return await _partnerBusinessObject.GetChannelInfoAsync(channelId);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Channels/ChannelList2")]
        [Obsolete]
        public async Task<IEnumerable<ChannelInfoDTO>> GetChannelList2()
        {
            PartnerBusinessObject _partnerBusinessObject = new PartnerBusinessObject();
            return await _partnerBusinessObject.GetChannelInfosAsync2();
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Channels/ChannelInfo2/{channelId}")]
        [Obsolete]
        public async Task<ChannelInfoDTO> GetChannelInfo2(int channelId)
        {
            PartnerBusinessObject _partnerBusinessObject = new PartnerBusinessObject();
            return await _partnerBusinessObject.GetChannelInfoAsync2(channelId);
        }
    }
}
