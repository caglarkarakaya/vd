﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VD.Business.Concrete;
using VD.DTO.Concrete;

namespace VD.Api.Controllers
{
    public class CategoriesController : VDApiControllerBase
    {
        [HttpGet]
        [Route("Categories/CategoryList")]
        public async Task<IEnumerable<CategoryDTO>> CategoryList()
        {
            CategoryBusinessObject _categoryBusinessObject = new CategoryBusinessObject();
            return await _categoryBusinessObject.GetCategoriesAsync();
        }

        [HttpGet]
        [Route("Categories/{categoryId}")]
        public async Task<CategoryDTO> GetCategory(int categoryId)
        {
            CategoryBusinessObject _categoryBusinessObject = new CategoryBusinessObject();
            return await _categoryBusinessObject.GetCategoryAsync(categoryId);
        }

        [HttpGet]
        [Route("Categories/Channel/{channelId}")]
        public async Task<IEnumerable<ChannelsCategoryInfoDTO>> GetCategoriesOfChannel(int channelId)
        {
            CategoryBusinessObject _categoryBusinessObject = new CategoryBusinessObject();
            return await _categoryBusinessObject.GetCategoriesOfChannelAsync(channelId);
        }

        //TO BE DELETED
        [Obsolete]
        [HttpGet]
        [Route("Categories/CategoryList2")]
        public async Task<IEnumerable<CategoryDTO>> CategoryList2()
        {
            CategoryBusinessObject _categoryBusinessObject = new CategoryBusinessObject();
            return await _categoryBusinessObject.GetCategoriesAsync2();
        }

        //TO BE DELETED
        [Obsolete]
        [HttpGet]
        [Route("Categories2/{categoryId}")]
        public async Task<CategoryDTO> GetCategory2(int categoryId)
        {
            CategoryBusinessObject _categoryBusinessObject = new CategoryBusinessObject();
            return await _categoryBusinessObject.GetCategoryAsync2(categoryId);
        }

        //TO BE DELETED
        [Obsolete]
        [HttpGet]
        [Route("Categories/Channel2/{channelId}")]
        public async Task<IEnumerable<ChannelsCategoryInfoDTO>> GetCategoriesOfChannel2(int channelId)
        {
            CategoryBusinessObject _categoryBusinessObject = new CategoryBusinessObject();
            return await _categoryBusinessObject.GetCategoriesOfChannelAsync2(channelId);
        }
    }
}
