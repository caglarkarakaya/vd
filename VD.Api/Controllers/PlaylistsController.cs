﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VD.Api.Queries;
using VD.Business.Concrete;
using VD.DTO.Concrete;

namespace VD.Api.Controllers
{
    public class PlaylistsController : VDApiControllerBase
    {
        [HttpGet]
        [Route("Playlists/PlaylistList")]
        public async Task<PlaylistsDTO> GetPlaylistsList([FromUri] PlaylistListQuery query)
        {
            PlaylistBusinessObject _playlistBusinessObject = new PlaylistBusinessObject();

            if (query != null && query.IsPublic.HasValue)
            {
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                return await _playlistBusinessObject.GetPlaylistsAsync(query.IsPublic, skip, take);
                //return await (query.Top.HasValue ? _playlistBusinessObject.GetPlaylistsAsync(query.IsPublic, query.Top.Value)
                //                                 : _playlistBusinessObject.GetPlaylistsAsync(query.IsPublic));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [Route("Playlists/Category")]
        public async Task<IEnumerable<PlaylistWithCategoryDTO>> GetPlaylistsListWithCategory([FromUri] PlaylistByCategory query)
        {
            PlaylistBusinessObject _playlistBusinessObject = new PlaylistBusinessObject();

            if (query != null && query.CategoryId != 0 && query.IsPublic.HasValue)
            {
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                return await _playlistBusinessObject.GetPlaylistsWithCategoryAsync(query.IsPublic, query.CategoryId, skip, take);
                //return await (query.Top.HasValue ? _playlistBusinessObject.GetPlaylistsWithCategoryAsync(query.IsPublic, query.CategoryId, query.Top.Value)
                //                                 : _playlistBusinessObject.GetPlaylistsWithCategoryAsync(query.IsPublic, query.CategoryId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [Route("Playlists/{PlaylistId}")]
        public async Task<PlaylistInfoDTO> GetPlaylistDetail(int playlistId, [FromUri] PlaylistListQuery query)
        {
            PlaylistBusinessObject _playlistBusinessObject = new PlaylistBusinessObject();

            if (query != null)
            {
                return await _playlistBusinessObject.GetPlaylistDetailAsync(playlistId, query.IsPublic);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //TO BE DELETED
        [Obsolete]
        [HttpGet]
        [Route("Playlists/PlaylistList2")]
        public async Task<IEnumerable<PlaylistInfoDTO>> GetPlaylistsList2([FromUri] PlaylistListQuery query)
        {
            PlaylistBusinessObject _playlistBusinessObject = new PlaylistBusinessObject();

            if (query != null && query.IsPublic.HasValue)
            {
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                return await _playlistBusinessObject.GetPlaylistsAsync2(query.IsPublic, skip, take);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //TO BE DELETED
        [Obsolete]
        [HttpGet]
        [Route("Playlists/Category2")]
        public async Task<IEnumerable<PlaylistWithCategoryDTO>> GetPlaylistsListWithCategory2([FromUri] PlaylistByCategory query)
        {
            PlaylistBusinessObject _playlistBusinessObject = new PlaylistBusinessObject();

            if (query != null && query.CategoryId != 0 && query.IsPublic.HasValue)
            {
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                return await _playlistBusinessObject.GetPlaylistsWithCategoryAsync2(query.IsPublic, query.CategoryId, skip, take);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //TO BE DELETED
        [Obsolete]
        [HttpGet]
        [Route("Playlists2/{PlaylistId}")]
        public async Task<PlaylistInfoDTO> GetPlaylistDetail2(int playlistId, [FromUri] PlaylistListQuery query)
        {
            PlaylistBusinessObject _playlistBusinessObject = new PlaylistBusinessObject();

            if (query != null)
            {
                return await _playlistBusinessObject.GetPlaylistDetailAsync2(playlistId, query.IsPublic);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
