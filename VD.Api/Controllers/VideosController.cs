﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VD.Api.ActionFilters;
using VD.Api.Queries;
using VD.Business.Concrete;
using VD.DTO.Concrete;
using VD.Elastic.Models;

namespace VD.Api.Controllers
{
    public class VideosController : VDApiControllerBase
    {
        [HttpGet]
        [Route("Videos/Hottest")]
        public async Task<IEnumerable<HottestVideoDTO>> Hottest([FromUri] HottestQuery query)
        {
            try
            {
                VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetHottestVideosAsync(skip, take);
            }
            catch (Exception ex)
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                var writingMessage = DateTime.Now.ToString() + "VDElasticChannelSynchronizer failed" + ex.Message;
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("Videos/Trending")]
        public async Task<IEnumerable<TrendingVideoDTO>> Trending([FromUri] TrendingQuery query)
        {
            StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
            var writingMessage = "Trending started at " + DateTime.Now.ToString();

            sw.WriteLine(writingMessage);
            sw.Flush();
            sw.Close();

            try
            {
                VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                var result = await _videoBusinessObject.GetTrendingVideosAsync(skip, take);
                writingMessage = "Trending ended at " + DateTime.Now.ToString() + " " + stopwatch.Elapsed;

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                return result;
            }
            catch (Exception ex)
            {
                writingMessage = "Trending failed at " + DateTime.Now.ToString() + ex.Message;

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("Videos/Recommended")]
        public async Task<IEnumerable<RecommendedVideoDTO>> GetRecommendedVideos([FromUri] VideosQueries query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            return await _videoBusinessObject.GetRecommendedVideosAsync(skip, take);
        }

        [HttpGet]
        [Route("Videos/Category/{id}")]
        public async Task<VideosDTO> GetVideosByCategory(int id, [FromUri] VideoInfoQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            return await _videoBusinessObject.GetVideosByCategoryAsync(id, skip, take);
        }

        [HttpGet]
        [Route("Videos/Channel/{id}")]
        public async Task<VideosDTO> GetVideosByChannel(int id, [FromUri] VideoInfoQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            return await _videoBusinessObject.GetVideosByChannelAsync(id, skip, take);
        }

        [HttpGet]
        [Route("Videos")]
        public async Task<VideosDTO> GetVideosByChannelAndCategoryAsync([FromUri] VideoInfoByChannelAndCategoryQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            if (query != null && query.ChannelId != 0 && query.CategoryId != 0)
            {
                int skip = query.Skip ?? 0;
                int take = query.Top ?? 10;

                return await _videoBusinessObject.GetVideosByChannelsCategoriesAsync(query.ChannelId, query.CategoryId, skip, take);
                //return await ((query != null && query.Top.HasValue) ? _videoBusinessObject.GetVideosByChannelsCategoriesAsync(query.ChannelId, query.CategoryId, query.Top.Value) : _videoBusinessObject.GetVideosByChannelsCategoriesAsync(query.ChannelId, query.CategoryId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [Route("Videos/Playlist")]
        public async Task<IEnumerable<PlaylistVideoInfoDTO>> GetPlaylistVideos([FromUri] PlaylistVideoQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            if (query != null && query.PlaylistId != 0)
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int? take = (query.Top.HasValue) ? query.Top.Value : (int?)null;

                return await _videoBusinessObject.GetVideosByPlaylistAsync(query.PlaylistId, take, skip);
                //return await (query.Top.HasValue ? _videoBusinessObject.GetVideosByPlaylistAsync(query.PlaylistId, query.Top.Value) : _videoBusinessObject.GetVideosByPlaylistAsync(query.PlaylistId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [Route("Videos/VideoDetail/SlugKey={slugKey}")]
        public async Task<VideoDetailDTO> GetVideoDetailsBySlugKey(string slugKey)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            return await _videoBusinessObject.GetVideoDetailsBySlugKeyAsync(slugKey);
        }

        [HttpGet]
        [Route("Videos/VideoDetail/VideoMD5={videoMD5}")]
        public async Task<VideoDetailDTO> GetVideoDetailsByVideoMD5(string videoMD5)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            return await _videoBusinessObject.GetVideoDetailsByVideoMD5Async(videoMD5);
        }

        [HttpGet]
        [Route("Videos/VideoDetail/VideoId={videoId}")]
        public async Task<VideoDetailDTO> GetVideoDetailsByVideoId(int videoId)
        {
            StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
            var writingMessage = "VideoDetail started at " + DateTime.Now.ToString();

            sw.WriteLine(writingMessage);
            sw.Flush();
            sw.Close();
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
                var result = await _videoBusinessObject.GetVideoDetailsByVideoIdAsync(videoId);
                writingMessage = "VideoDetail ended at " + DateTime.Now.ToString() + " " + stopwatch.Elapsed;

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();
                return result;
            }
            catch (Exception ex)
            {
                writingMessage = "VideoDetail failed at " + DateTime.Now.ToString() + ex.Message;

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("Videos/Stunning/{CategoryId}")]
        public async Task<IEnumerable<StunningVideoDTO>> GetStunningVideos(int categoryId, [FromUri] VideosQueries query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            if (query != null)
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetStunningVideosAsync(categoryId, skip, take);
                //return await (query.Top.HasValue ? _videoBusinessObject.GetStunningVideosAsync(categoryId, query.Top.Value)
                //                                 : _videoBusinessObject.GetStunningVideosAsync(categoryId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [Route("Videos/UpNext")]
        public async Task<IEnumerable<VideoInfoDTO>> GetUpNextVideos([FromUri] UpNextQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            if (query != null && !string.IsNullOrEmpty(query.SlugKey) && query.CategoryId != 0 && query.ChannelId != 0)
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetUpNextVideosAsync(query.SlugKey, query.ChannelId, query.CategoryId, skip, take);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [Route("Videos/Search/{SearchText}")]
        public SearchDTO SearchVideos(string searchText, [FromUri] SearchQueries query)
        {
            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            VideoBusinessObject videoBusinessObject = new VideoBusinessObject();
            var result = videoBusinessObject.SearchVideos(searchText, skip, take);
            stopwatch.Stop();

            StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
            var writingMessage = DateTime.Now.ToString() + " " + stopwatch.Elapsed;
            sw.WriteLine(writingMessage);
            sw.Flush();
            sw.Close();

            return result;
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/VideoDetail2/SlugKey={slugKey}")]
        [Obsolete]
        public async Task<VideoDetailDTO> GetVideoDetailsBySlugKey2(string slugKey)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            return await _videoBusinessObject.GetVideoDetailsBySlugKeyAsync2(slugKey);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/VideoDetail2/VideoMD5={videoMD5}")]
        [Obsolete]
        public async Task<VideoDetailDTO> GetVideoDetailsByVideoMD52(string videoMD5)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            return await _videoBusinessObject.GetVideoDetailsByVideoMD5Async2(videoMD5);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/VideoDetail2/VideoId={videoId}")]
        [Obsolete]
        public async Task<VideoDetailDTO> GetVideoDetailsByVideoId2(int videoId)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            return await _videoBusinessObject.GetVideoDetailsByVideoIdAsync2(videoId);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Playlist2")]
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetPlaylistVideos2([FromUri] PlaylistVideoQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            if (query != null && query.PlaylistId != 0)
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetVideosByPlaylistAsync2(query.PlaylistId, skip, take);
                //return await (query.Top.HasValue ? _videoBusinessObject.GetVideosByPlaylistAsync(query.PlaylistId, query.Top.Value) : _videoBusinessObject.GetVideosByPlaylistAsync(query.PlaylistId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Channel2/{id}")]
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetVideosByChannel2(int id, [FromUri] VideoInfoQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            return await _videoBusinessObject.GetVideosByChannelAsync2(id, skip, take);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos2")]
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetVideosByChannelAndCategoryAsync2([FromUri] VideoInfoByChannelAndCategoryQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            if (query != null && query.ChannelId != 0 && query.CategoryId != 0)
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetVideosByChannelsCategoriesAsync2(query.ChannelId, query.CategoryId, skip, take);
                //return await ((query != null && query.Top.HasValue) ? _videoBusinessObject.GetVideosByChannelsCategoriesAsync(query.ChannelId, query.CategoryId, query.Top.Value) : _videoBusinessObject.GetVideosByChannelsCategoriesAsync(query.ChannelId, query.CategoryId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Category2/{id}")]
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetVideosByCategory2(int id, [FromUri] VideoInfoQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
            int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            return await _videoBusinessObject.GetVideosByCategoryAsync2(id, skip, take);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Hottest2")]
        [Obsolete]
        public async Task<IEnumerable<HottestVideoDTO>> Hottest2([FromUri] HottestQuery query)
        {
            try
            {
                VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetHottestVideosAsync2(skip, take);
            }
            catch (Exception ex)
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                var writingMessage = DateTime.Now.ToString() + "VDElasticChannelSynchronizer failed" + ex.Message;
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Trending2")]
        [Obsolete]
        public async Task<IEnumerable<TrendingVideoDTO>> Trending2([FromUri] TrendingQuery query)
        {
            StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
            var writingMessage = "Trending started at " + DateTime.Now.ToString();

            sw.WriteLine(writingMessage);
            sw.Flush();
            sw.Close();

            try
            {
                VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();
                int skip = (query != null && query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                var result = await _videoBusinessObject.GetTrendingVideosAsync2(skip, take);
                writingMessage = "Trending ended at " + DateTime.Now.ToString() + " " + stopwatch.Elapsed;

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                return result;
            }
            catch (Exception ex)
            {
                writingMessage = "Trending failed at " + DateTime.Now.ToString() + ex.Message;

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Recommended2")]
        [Obsolete]
        public async Task<IEnumerable<StunningVideoDTO>> GetRecommendedVideos2([FromUri] VideosQueries query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            int take = (query != null && query.Top.HasValue) ? query.Top.Value : 10;

            return await _videoBusinessObject.GetRandomVideosAsync(take);
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/Stunning2/{CategoryId}")]
        [Obsolete]
        public async Task<IEnumerable<StunningVideoDTO>> GetStunningVideos2(int categoryId, [FromUri] VideosQueries query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            if (query != null)
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetStunningVideosAsync2(categoryId, skip, take);
                //return await (query.Top.HasValue ? _videoBusinessObject.GetStunningVideosAsync(categoryId, query.Top.Value)
                //                                 : _videoBusinessObject.GetStunningVideosAsync(categoryId));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //TO BE DELETED
        [HttpGet]
        [Route("Videos/UpNext2")]
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetUpNextVideos2([FromUri] UpNextQuery query)
        {
            VideoBusinessObject _videoBusinessObject = new VideoBusinessObject();

            if (query != null && !string.IsNullOrEmpty(query.SlugKey))
            {
                int skip = (query.Skip.HasValue) ? query.Skip.Value : 0;
                int take = (query.Top.HasValue) ? query.Top.Value : 10;

                return await _videoBusinessObject.GetRelatedAsync(query.SlugKey, skip, take);
                //return await (query.Top.HasValue ? _videoBusinessObject.GetRelatedAsync(query.SlugKey, query.Top.Value) : _videoBusinessObject.GetRelatedAsync(query.SlugKey));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
