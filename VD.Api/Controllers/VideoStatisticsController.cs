﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VD.Business.Concrete;

namespace VD.Api.Controllers
{
    public class VideoStatisticsController : VDApiControllerBase
    {
        [HttpGet]
        [Route("VideoStatistics/IncreaseViewCount/{VideoId}")]
        public HttpResponseMessage IncreaseViewCount(int videoId)
        {
            try
            {
                VideoStatisticBusinessObject _videoStatisticBusinessObject = new VideoStatisticBusinessObject();
                _videoStatisticBusinessObject.IncreaseViewCount(videoId);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Something went wrong. ");
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
