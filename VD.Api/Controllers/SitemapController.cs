﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using VD.Business.Concrete;

namespace VD.Api.Controllers
{
    public class SitemapController : VDApiControllerBase
    {
        [HttpGet]
        [Route("Sitemap/UpdateSitemap")]
        public HttpResponseMessage UpdateSitemap()
        {
            try
            {
                SitemapBusinessObject sitemapBusinessObject = new SitemapBusinessObject();
                sitemapBusinessObject.CreateSitemap();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Something went wrong. ");
            }
        }

        [HttpGet]
        [Route("Sitemap/UpdateSitemapAsync")]
        public async Task<HttpResponseMessage> UpdateSitemapAsync()
        {
            try
            {
                SitemapBusinessObject sitemapBusinessObject = new SitemapBusinessObject();
                await sitemapBusinessObject.CreateSitemapAsync();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Something went wrong. ");
            }
        }
    }
}