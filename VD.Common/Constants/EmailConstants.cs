﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Common.Constants
{
    public class EmailConstants
    {
        public const string VidisAdminDisplayName = "Video Distribution Admin";
        public const string VidisAdminMailAddress = "admin@videodistribution.com";
        public static string VidisDefaultCcEmail { get; } = "";
        public static string VidisDefaultToEmail { get; } = "batuhan.karagol@tunnl.com";
        public static string VidisDefaultBccEmail { get; } = "batuhan.karagol@tunnl.com";
        public static string VidisDefaultBccDisplayName { get; } = "Batuhan Karagöl";
        public static string VidisDefaultBccEmail2 { get; } = "caglar.karakaya@tunnl.com";
        public static string VidisDefaultBccDisplayName2 { get; } = "Çağlar Karakaya";
    }
}
