﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Common.Enums
{
    public enum EmailTypeEnum
    {
        NOREPLY,
        INFO,
        SUPPORT,
        ADMIN
    }
}
