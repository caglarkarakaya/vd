﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Common.Enums
{
    public static class SitemapEnums
    {
        public enum ChangeFrequency
        {
            always,
            hourly,
            daily,
            weekly,
            monthly,
            yearly,
            never
        }

        public enum Priority
        {
            top,
            medium,
            low
        }

        public static string Value(this Priority priority)
        {
            string value;
            switch (priority)
            {
                case Priority.top:
                    value = "1.0";
                    break;
                case Priority.medium:
                    value = "0.8";
                    break;
                case Priority.low:
                    value = "0.5";
                    break;
                default:
                    value = "0.5";
                    break;
            }

            return value;
        }
    }
}
