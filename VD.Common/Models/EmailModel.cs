﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Common.Enums;

namespace VD.Common.Models
{
    public class EmailModel
    {
        public EmailTypeEnum EmailConfigCode { get; set; }
        public string ToEmail { get; set; }
        public string ToDisplayName { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public bool IsSignatureNecessary { get; set; }
        public string SenderIp { get; set; }
        public bool UseTemplate { get; set; }
        public Dictionary<string, string> TemplateParams { get; set; }
    }
}
