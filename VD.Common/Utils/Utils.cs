﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Common.Utils
{
    public class Utils
    {

        public static void WriteLog(string message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                var writingMessage = DateTime.Now.ToString() + " " + message;
                sw.WriteLine(writingMessage);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {

            }
        }
    }
}
