﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Storage.v1;
using Google.Apis.Upload;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using static Google.Apis.Storage.v1.ObjectsResource;
using Object = Google.Apis.Storage.v1.Data.Object;
using Google.Cloud.Storage.V1;
using VD.Common.Utils;

namespace VD.Common.Cloud.Google
{
    public static class GoogleCloudStorageProperties
    {
        public static string VidisAccountEmail = "video-storage-admin@vooxe-video.iam.gserviceaccount.com";
        public static string VidisCertificateFilePath = @"Google\VideoDistribution\vidis_new.p12";
        public static string VidisBucketName = "cdn-videodistribution-bucket";
        public static string VidisVideosFolderPath = "videos/" + DateTime.Now.ToString("yyyyMM");
        public static string VidisImagesFolderPath = "images/" + DateTime.Now.ToString("yyyyMM");
        public static string VidisPartnerImagesFolderPath = "images/partner";
    }

    public class GoogleCloudStorage
    {
        private StorageService _storageService;
        private StorageClient _storageClient;

        public GoogleCloudStorage()
        {
        }

        public GoogleCloudStorage(string certificatePath, string accountMail)
        {
            var certificate
                     = new X509Certificate2(PathHelper.ResolvePath(certificatePath), "notasecret", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet);

            var credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(accountMail)
                {
                    Scopes = new[] { StorageService.Scope.DevstorageReadWrite }
                }.FromCertificate(certificate));

            _storageService = new StorageService(
                new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential
                });

            GoogleCredential googleCredential = GoogleCredential.FromServiceAccountCredential(credential);

            _storageClient = StorageClient.Create(googleCredential);
        }

        public bool UploadFile(Stream stream, string bucketName, string fileName, string contentType)
        {
            var result = false;
            try
            {
                Object uploadObject = new Object();
                uploadObject.Bucket = bucketName;
                uploadObject.Name = fileName;

                InsertMediaUpload insertMediaUpload = new InsertMediaUpload(_storageService, uploadObject, bucketName, stream, contentType);
                IUploadProgress uploadProgress = insertMediaUpload.Upload();

                result = uploadProgress.Status == UploadStatus.Completed ? true : false;
                //result.Message = uploadProgress.Exception != null ? uploadProgress.Exception.ToString() : fileName;
            }
            catch (Exception ex)
            {
                result = false;
                Utils.Utils.WriteLog(string.Format("Error Message: {0}\n All Exception Details Json: {1}", ex.Message.ToString(), JsonConvert.SerializeObject(ex)));
                //result.Message = ex.ToString();
            }
            return result;
        }

        public async Task<bool> UploadFileAsync(Stream stream, string bucketName, string fileName, string contentType)
        {
            var result = false;
            try
            {
                Object uploadObject = new Object();
                uploadObject.Bucket = bucketName;
                uploadObject.Name = fileName;

                InsertMediaUpload insertMediaUpload = new InsertMediaUpload(_storageService, uploadObject, bucketName, stream, contentType);
                IUploadProgress uploadProgress = await insertMediaUpload.UploadAsync();

                result = uploadProgress.Status == UploadStatus.Completed ? true : false;
                //result.Message = uploadProgress.Exception != null ? uploadProgress.Exception.ToString() : fileName;
            }
            catch (Exception ex)
            {
                result = false;
                Utils.Utils.WriteLog(string.Format("Error Message: {0}\n All Exception Details Json: {1}", ex.Message.ToString(), JsonConvert.SerializeObject(ex)));
                //result.Message = ex.ToString();
            }
            return result;
        }

        public void RenameFile(string bucketName, string sourceObjectName, string destObjectName)
        {   
            _storageClient.CopyObject(bucketName, sourceObjectName, bucketName,
                destObjectName);
            _storageClient.DeleteObject(bucketName, sourceObjectName);
        }

        public async Task RenameFileAsync(string bucketName, string sourceObjectName, string destObjectName)
        {
            await _storageClient.CopyObjectAsync(bucketName, sourceObjectName, bucketName,
                destObjectName);
            await _storageClient.DeleteObjectAsync(bucketName, sourceObjectName);
        }
    }
}
