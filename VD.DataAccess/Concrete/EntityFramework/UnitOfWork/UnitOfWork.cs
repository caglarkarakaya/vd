﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.DataAccess.Repository;
using VD.Core.DataAccess.UnitOfWork;
using VD.Core.Entity;
using VD.DataAccess.Concrete.EntityFramework.Context;
using VD.DataAccess.Concrete.EntityFramework.Repository;

namespace VD.DataAccess.Concrete.EntityFramework.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private VDContext _context;
        private bool _disposed = false;

        public UnitOfWork()
        {
            _context = new VDContext();
        }

        public UnitOfWork(string connectionString)
        {
            _context = new VDContext(connectionString);
        }

        public VDContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
            }
        }
        
        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public IRepository<T> GetRepository<T>()
            where T : class, IEntity, new()
        {
            return RepositoryFactory.GetRepository<T>(_context);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
