﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VD.Core.DataAccess.Repository;
using VD.Core.Entity;

namespace VD.DataAccess.Concrete.EntityFramework.Repository
{
    public class VDRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity, new()
    {
        private DbContext _context;

        public VDRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null ? _context.Set<TEntity>() : _context.Set<TEntity>().Where(filter);
        }

        public TEntity Insert(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Added;

            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            return entity;
        }

        public void Delete(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }
    }
}
