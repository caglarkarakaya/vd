﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Entity;

namespace VD.DataAccess.Concrete.EntityFramework.Repository
{
    public class RepositoryFactory
    {
        public RepositoryFactory()
        {
        }

        public static VDRepository<T> GetRepository<T>(DbContext context)
            where T : class, IEntity, new()
        {
            VDRepository<T> repository = new VDRepository<T>(context);

            return repository;
        }
    }
}
