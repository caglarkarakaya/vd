﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class LanguageMap : VDEntityMapBase<LanguageEntity>
    {
        public LanguageMap()
        {
            ToTable(@"Languages", @"public");

            Property(x => x.CultureCode).HasColumnName("CultureCode");
            Property(x => x.Name).HasColumnName("Name");
        }
    }
}
