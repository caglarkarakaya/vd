﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoStatisticMap : VDEntityMapBase<VideoStatisticEntity>
    {
        public VideoStatisticMap()
        {
            ToTable(@"VideoStatistics", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.LoadCount).HasColumnName("LoadCount");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.VideoId).HasColumnName("VideoId");
            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
            Property(x => x.ViewCount).HasColumnName("ViewCount");
        }
    }
}
