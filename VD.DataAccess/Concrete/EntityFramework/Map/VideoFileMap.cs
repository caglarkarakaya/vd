﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoFileMap : VDEntityMapBase<VideoFileEntity>
    {
        public VideoFileMap()
        {
            ToTable(@"VideoFiles", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Fps).HasColumnName("Fps");
            Property(x => x.Height).HasColumnName("Height");
            Property(x => x.Quality).HasColumnName("Quality");
            Property(x => x.Size).HasColumnName("Size");
            Property(x => x.Url).HasColumnName("Url");
            Property(x => x.UrlSecure).HasColumnName("UrlSecure");
            Property(x => x.VideoId).HasColumnName("VideoId");
            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
            Property(x => x.VideoTypeId).HasColumnName("VideoTypeId");
            Property(x => x.Width).HasColumnName("Width");
        }
    }
}
