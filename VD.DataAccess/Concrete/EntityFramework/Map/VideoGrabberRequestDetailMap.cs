﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoGrabberRequestDetailMap : VDEntityMapBase<VideoGrabberRequestDetailEntity>
    {
        public VideoGrabberRequestDetailMap()
        {
            ToTable(@"VideoGrabberRequestDetails", @"public");

            Property(x => x.CategoryId).HasColumnName("CategoryId");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.LanguageId).HasColumnName("LanguageId");
            Property(x => x.ProcessResult).HasColumnName("ProcessResult");
            Property(x => x.State).HasColumnName("State");
            Property(x => x.VideoDescription).HasColumnName("VideoDescription");
            Property(x => x.VideoDownloadUrl).HasColumnName("VideoDownloadUrl");
            Property(x => x.VideoGrabberRequestId).HasColumnName("VideoGrabberRequestId");
            Property(x => x.VideoId).HasColumnName("VideoId");
            Property(x => x.VideoImage).HasColumnName("VideoImage");
            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
            Property(x => x.VideoSourceId).HasColumnName("VideoSourceId");
            Property(x => x.VideoTitle).HasColumnName("VideoTitle");
        }
    }
}
