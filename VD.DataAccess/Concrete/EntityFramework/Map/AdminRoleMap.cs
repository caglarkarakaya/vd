﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class AdminRoleMap : VDEntityMapBase<AdminRoleEntity>
    {
        public AdminRoleMap()
        {
            ToTable(@"AdminRoles", @"public");

            Property(x => x.RoleName).HasColumnName("RoleName");
            Property(x => x.IsActive).HasColumnName("IsActive");
        }
    }
}
