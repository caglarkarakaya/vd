﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class SupportRequestMap : VDEntityMapBase<SupportRequestEntity>
    {
        public SupportRequestMap()
        {
            ToTable(@"SupportRequests", @"public");

            Property(x => x.BrowserCookies).HasColumnName("BrowserCookies");
            Property(x => x.BrowserFrames).HasColumnName("BrowserFrames");
            Property(x => x.BrowserIsAOL).HasColumnName("BrowserIsAOL");
            Property(x => x.BrowserIsBeta).HasColumnName("BrowserIsBeta");
            Property(x => x.BrowserIsWin32).HasColumnName("BrowserIsWin32");
            Property(x => x.BrowserName).HasColumnName("BrowserName");
            Property(x => x.BrowserPlatform).HasColumnName("BrowserPlatform");
            Property(x => x.BrowserTables).HasColumnName("BrowserTables");
            Property(x => x.BrowserType).HasColumnName("BrowserType");
            Property(x => x.BrowserVersion).HasColumnName("BrowserVersion");
            Property(x => x.CompanyName).HasColumnName("CompanyName");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Email).HasColumnName("Email");
            Property(x => x.IpAddress).HasColumnName("IpAddress");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.Message).HasColumnName("Message");
            Property(x => x.Name).HasColumnName("Name");
            Property(x => x.Phone).HasColumnName("Phone");
            Property(x => x.Subject).HasColumnName("Subject");
            Property(x => x.UserPartnerId).HasColumnName("UserPartnerId");
            Property(x => x.Website).HasColumnName("Website");
        }
    }
}
