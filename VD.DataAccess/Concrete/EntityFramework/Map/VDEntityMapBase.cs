﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Entity;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public abstract class VDEntityMapBase<T> : EntityTypeConfiguration<T> where T : VDEntityBase
    {
        public VDEntityMapBase()
        {
            HasKey(x => x.Id);
        }
    }
}
