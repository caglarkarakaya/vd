﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class CountryMap : VDEntityMapBase<CountryEntity>
    {
        public CountryMap()
        {
            ToTable(@"Countries", @"public");

            Property(x => x.ContinentCode).HasColumnName("ContinentCode");
            Property(x => x.ContinentName).HasColumnName("ContinentName");
            Property(x => x.ISO).HasColumnName("ISO");
            Property(x => x.ISO3).HasColumnName("ISO3");
            Property(x => x.Name).HasColumnName("Name");
        }
    }
}
