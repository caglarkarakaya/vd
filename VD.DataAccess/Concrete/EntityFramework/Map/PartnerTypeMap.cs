﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class PartnerTypeMap : VDEntityMapBase<PartnerTypeEntity>
    {
        public PartnerTypeMap()
        {
            ToTable(@"PartnerTypes", @"public");

            Property(x => x.BandwidthLimit).HasColumnName("BandwidthLimit");
            Property(x => x.ChannelLimit).HasColumnName("ChannelLimit");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.DomainLimit).HasColumnName("DomainLimit");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.Name).HasColumnName("Name");
            Property(x => x.PlaylistLimit).HasColumnName("PlaylistLimit");
            Property(x => x.StorageLimit).HasColumnName("StorageLimit");
            Property(x => x.UserLimit).HasColumnName("UserLimit");
        }
    }
}
