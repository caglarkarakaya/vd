﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class ReportedVideoMap : VDEntityMapBase<ReportedVideoEntity>
    {
        public ReportedVideoMap()
        {
            ToTable(@"ReportedVideos", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.IsPositive).HasColumnName("IsPositive");
            Property(x => x.IsResolved).HasColumnName("IsResolved");
            Property(x => x.ReasonId).HasColumnName("ReasonId");
            Property(x => x.Url).HasColumnName("Url");
            Property(x => x.VideoId).HasColumnName("VideoId");
            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
        }
    }
}
