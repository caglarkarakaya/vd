﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class UserPartnerMap : VDEntityMapBase<UserPartnerEntity>
    {
        public UserPartnerMap()
        {
            ToTable(@"UsersPartners", @"public");

            Property(x => x.IsContactUser).HasColumnName("IsContactUser");
            Property(x => x.PartnerId).HasColumnName("PartnerId");
            Property(x => x.UserId).HasColumnName("UserId");
            Property(x => x.UserRoleId).HasColumnName("UserRoleId");
        }
    }
}
