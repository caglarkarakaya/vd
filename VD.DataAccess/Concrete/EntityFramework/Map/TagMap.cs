﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class TagMap : VDEntityMapBase<TagEntity>
    {
        public TagMap()
        {
            ToTable(@"Tags", @"public");

            Property(x => x.Title).HasColumnName("Title");
        }
    }
}
