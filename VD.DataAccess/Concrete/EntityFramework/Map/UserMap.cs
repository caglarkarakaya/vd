﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class UserMap : VDEntityMapBase<UserEntity>
    {
        public UserMap()
        {
            ToTable(@"Users", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Email).HasColumnName("Email");
            Property(x => x.FirstName).HasColumnName("FirstName");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.LastLoginIp).HasColumnName("LastLoginIp");
            Property(x => x.LastName).HasColumnName("LastName");
            Property(x => x.Password).HasColumnName("Password");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
        }
    }
}
