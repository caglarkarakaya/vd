﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoMap : VDEntityMapBase<VideoEntity>
    {
        public VideoMap()
        {
            ToTable(@"Videos", @"public");

            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
            Property(x => x.UserPartnerId).HasColumnName("UserPartnerId");
            Property(x => x.CategoryId).HasColumnName("CategoryId");
            Property(x => x.PrivacyId).HasColumnName("PrivacyId");
            Property(x => x.Title).HasColumnName("Title");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.Width).HasColumnName("Width");
            Property(x => x.Height).HasColumnName("Height");
            Property(x => x.Thumbnail).HasColumnName("Thumbnail");
            Property(x => x.ApproveStatusId).HasColumnName("ApproveStatusId");
            Property(x => x.ApprovalChangedAt).HasColumnName("ApprovalChangedAt");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.TranscodeId).HasColumnName("TranscodeId");
            Property(x => x.TranscodeStatusId).HasColumnName("TranscodeStatusId");
            Property(x => x.TranscodeStartedAt).HasColumnName("TranscodeStartedAt");
            Property(x => x.TranscodeEndedAt).HasColumnName("TranscodeEndedAt");
            Property(x => x.TranscodeDescription).HasColumnName("TranscodeDescription");
            Property(x => x.TranscodeSubfolder).HasColumnName("TranscodeSubfolder");
            Property(x => x.Duration).HasColumnName("Duration");
            Property(x => x.ResourceGuid).HasColumnName("ResourceGuid");
            Property(x => x.RssFeedId).HasColumnName("RssFeedId");
            Property(x => x.BannedCountries).HasColumnName("BannedCountries");
            Property(x => x.VideoSize).HasColumnName("VideoSize");
            Property(x => x.ResourceUrl).HasColumnName("ResourceUrl");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.LanguageId).HasColumnName("LanguageId");
            Property(x => x.RejectReason).HasColumnName("RejectReason");
            Property(x => x.LikeCount).HasColumnName("LikeCount");
            Property(x => x.DislikeCount).HasColumnName("DislikeCount");
            Property(x => x.IsHomePageTopVideo).HasColumnName("IsHomePageTopVideo");
            Property(x => x.VimeoAccountId).HasColumnName("VimeoAccountId");
            Property(x => x.VimeoUri).HasColumnName("VimeoUri");
            Property(x => x.VimeoState).HasColumnName("VimeoState");
            Property(x => x.TranscodeCheckTrialCount).HasColumnName("TranscodeCheckTrialCount");

        }
    }
}
