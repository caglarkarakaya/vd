﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class AdminUserMap : VDEntityMapBase<AdminUserEntity>
    {
        public AdminUserMap()
        {
            ToTable(@"AdminUsers", @"public");

            Property(x => x.FirstName).HasColumnName("FirstName");
            Property(x => x.LastName).HasColumnName("LastName");
            Property(x => x.Email).HasColumnName("Email");
            Property(x => x.Password).HasColumnName("Password");
            Property(x => x.AdminRoleId).HasColumnName("AdminRoleId");
            Property(x => x.IsActive).HasColumnName("IsActive");
        }
    }
}
