﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class CountryIpRangeMap : VDEntityMapBase<CountryIpRangeEntity>
    {
        public CountryIpRangeMap()
        {
            ToTable(@"CountriesIpRanges", @"public");

            Property(x => x.CountryId).HasColumnName("CountryId");
            Property(x => x.IpEnd).HasColumnName("IpEnd");
            Property(x => x.IpStart).HasColumnName("IpStart");
            Property(x => x.ISOCode).HasColumnName("ISOCode");
        }
    }
}
