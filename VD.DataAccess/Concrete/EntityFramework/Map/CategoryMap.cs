﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class CategoryMap : VDEntityMapBase<CategoryEntity>
    {
        public CategoryMap()
        {
            ToTable(@"Categories", @"public");

            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.Title).HasColumnName("Title");
        }
    }
}
