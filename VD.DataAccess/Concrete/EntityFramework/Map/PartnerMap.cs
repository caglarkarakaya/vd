﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class PartnerMap : VDEntityMapBase<PartnerEntity>
    {
        public PartnerMap()
        {
            ToTable(@"Partners", @"public");

            Property(x => x.Address).HasColumnName("Address");
            Property(x => x.City).HasColumnName("City");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.IsTrusted).HasColumnName("IsTrusted");
            Property(x => x.MultiUploadAccess).HasColumnName("MultiUploadAccess");
            Property(x => x.Name).HasColumnName("Name");
            Property(x => x.PartnerTypeId).HasColumnName("PartnerTypeId");
            Property(x => x.Phone).HasColumnName("Phone");
            Property(x => x.ReportPartnerId).HasColumnName("ReportPartnerId");
            Property(x => x.ShortCode).HasColumnName("ShortCode");
            Property(x => x.State).HasColumnName("State");
            Property(x => x.SupportAccess).HasColumnName("SupportAccess");
            Property(x => x.TotalStorage).HasColumnName("TotalStorage");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.ZipCode).HasColumnName("ZipCode");
            Property(x => x.BackgroundImageUrl).HasColumnName("BackgroundImageUrl");
            Property(x => x.AvatarUrl).HasColumnName("AvatarUrl");
            Property(x => x.CardImageUrl).HasColumnName("CardImageUrl");
            Property(x => x.Description).HasColumnName("Description");
        }
    }
}
