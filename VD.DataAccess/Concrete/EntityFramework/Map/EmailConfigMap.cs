﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class EmailConfigMap : VDEntityMapBase<EmailConfigEntity>
    {
        public EmailConfigMap()
        {
            ToTable(@"EmailConfigs", @"public");

            Property(x => x.Code).HasColumnName("Code");
            Property(x => x.DisplayName).HasColumnName("DisplayName");
            Property(x => x.Email).HasColumnName("Email");
            Property(x => x.EnableSsl).HasColumnName("EnableSsl");
            Property(x => x.Host).HasColumnName("Host");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.Password).HasColumnName("Password");
            Property(x => x.Port).HasColumnName("Port");
            Property(x => x.Signature).HasColumnName("Signature");
            Property(x => x.Template).HasColumnName("Template");
            Property(x => x.TemplateParams).HasColumnName("TemplateParams");
        }
    }
}
