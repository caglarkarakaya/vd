﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class SlugMap : VDEntityMapBase<SlugEntity>
    {
        public SlugMap()
        {
            ToTable(@"Slugs", @"public");

            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.Key).HasColumnName("Key");
            Property(x => x.VideoId).HasColumnName("VideoId");
            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
        }
    }
}
