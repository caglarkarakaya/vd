﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class ElasticSearchSyncMap : VDEntityMapBase<ElasticSearchSyncEntity>
    {
        public ElasticSearchSyncMap()
        {
            ToTable(@"ElasticSearchSyncTable", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.ExecutionResult).HasColumnName("ExecutionResult");
            Property(x => x.LastExecutionDateTime).HasColumnName("LastExecutionDateTime");
            Property(x => x.Type).HasColumnName("Type");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
        }
    }
}
