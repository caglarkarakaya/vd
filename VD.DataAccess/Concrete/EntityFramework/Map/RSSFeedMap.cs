﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class RssFeedMap : VDEntityMapBase<RssFeedEntity>
    {
        public RssFeedMap()
        {
            ToTable(@"RssFeeds", @"public");

            Property(x => x.CategoryId).HasColumnName("CategoryId");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.LanguageId).HasColumnName("LanguageId");
            Property(x => x.PartnerId).HasColumnName("PartnerId");
            Property(x => x.ProviderId).HasColumnName("ProviderId");
            Property(x => x.ProviderName).HasColumnName("ProviderName");
            Property(x => x.ProviderType).HasColumnName("ProviderType");
            Property(x => x.ScheduleInMinute).HasColumnName("ScheduleInMinute");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.Url).HasColumnName("Url");
        }
    }
}
