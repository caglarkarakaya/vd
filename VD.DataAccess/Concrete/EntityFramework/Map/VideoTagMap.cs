﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoTagMap : VDEntityMapBase<VideoTagEntity>
    {
        public VideoTagMap()
        {
            ToTable(@"VideoTags", @"public");

            Property(x => x.TagId).HasColumnName("TagId");
            Property(x => x.VideoId).HasColumnName("VideoId");
        }
    }
}
