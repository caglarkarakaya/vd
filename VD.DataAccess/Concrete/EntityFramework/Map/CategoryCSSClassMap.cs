﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class CategoryCssClassMap : VDEntityMapBase<CategoryCssClassEntity>
    {
        public CategoryCssClassMap()
        {
            ToTable(@"CategoryCSSClasses", @"public");
            
            Property(x => x.CSSClassName).HasColumnName("CSSClassName");
            Property(x => x.CategoryId).HasColumnName("CategoryId");
            Property(x => x.IsActive).HasColumnName("IsActive");
        }
    }
}
