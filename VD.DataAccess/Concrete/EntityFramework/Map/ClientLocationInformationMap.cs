﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class ClientLocationInformationMap : VDEntityMapBase<ClientLocationInformationEntity>
    {
        public ClientLocationInformationMap()
        {
            ToTable(@"ClientLocationInformations", @"public");

            Property(x => x.As).HasColumnName("as");
            Property(x => x.City).HasColumnName("city");
            Property(x => x.Country).HasColumnName("country");
            Property(x => x.CountryCode).HasColumnName("countryCode");
            Property(x => x.CreatedAt).HasColumnName("createdAt");
            Property(x => x.Isp).HasColumnName("isp");
            Property(x => x.Lat).HasColumnName("lat");
            Property(x => x.Lon).HasColumnName("lon");
            Property(x => x.Org).HasColumnName("org");
            Property(x => x.Query).HasColumnName("query");
            Property(x => x.Region).HasColumnName("region");
            Property(x => x.RegionName).HasColumnName("regionName");
            Property(x => x.Status).HasColumnName("status");
            Property(x => x.Timezone).HasColumnName("timezone");
            Property(x => x.Zip).HasColumnName("zip");
        }
    }
}
