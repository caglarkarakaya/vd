﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoGrabberRequestMap : VDEntityMapBase<VideoGrabberRequestEntity>
    {
        public VideoGrabberRequestMap()
        {
            ToTable(@"VideoGrabberRequests", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.EncryptRequestId).HasColumnName("EncryptRequestId");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.Name).HasColumnName("Name");
            Property(x => x.ProcessResult).HasColumnName("ProcessResult");
            Property(x => x.SourceUrl).HasColumnName("SourceUrl");
            Property(x => x.State).HasColumnName("State");
            Property(x => x.UserPartnerId).HasColumnName("UserPartnerId");
            Property(x => x.VideoGrabberTypeId).HasColumnName("VideoGrabberTypeId");
        }
    }
}
