﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class PartnerDomainMap : VDEntityMapBase<PartnerDomainEntity>
    {
        public PartnerDomainMap()
        {
            ToTable(@"PartnerDomains", @"public");

            Property(x => x.AdsAutoStart).HasColumnName("AdsAutoStart");
            Property(x => x.AdsEnabled).HasColumnName("AdsEnabled");
            Property(x => x.ColorBG).HasColumnName("ColorBG");
            Property(x => x.ColorButton).HasColumnName("ColorButton");
            Property(x => x.ColorProgressBar).HasColumnName("ColorProgressBar");
            Property(x => x.ColorText).HasColumnName("ColorText");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy");
            Property(x => x.Domain).HasColumnName("Domain");
            Property(x => x.FullScreenEnabled).HasColumnName("FullScreenEnabled");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.LogoEnabled).HasColumnName("LogoEnabled");
            Property(x => x.LogoUrl).HasColumnName("LogoUrl");
            Property(x => x.NotificationEnabled).HasColumnName("NotificationEnabled");
            Property(x => x.PartnerId).HasColumnName("PartnerId");
            Property(x => x.PreRollEnabled).HasColumnName("PreRollEnabled");
            Property(x => x.PreRollSecond).HasColumnName("PreRollSecond");
            Property(x => x.ShareButtonEnabled).HasColumnName("ShareButtonEnabled");
            Property(x => x.StateId).HasColumnName("StateId");
            Property(x => x.SubBannerEnabled).HasColumnName("SubBannerEnabled");
            Property(x => x.SubBannerSecond).HasColumnName("SubBannerSecond");
            Property(x => x.ThemeUrl).HasColumnName("ThemeUrl");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy");
        }
    }
}
