﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class ErrorLogMap : VDEntityMapBase<ErrorLogEntity>
    {
        public ErrorLogMap()
        {
            ToTable(@"ErrorLogs", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.ErrorMessage).HasColumnName("ErrorMessage");
            Property(x => x.ErrorType).HasColumnName("ErrorType");
            Property(x => x.Ip).HasColumnName("Ip");
        }
    }
}
