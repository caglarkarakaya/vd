﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class PlaylistVideoMap : EntityTypeConfiguration<PlaylistVideoEntity>
    {
        public PlaylistVideoMap()
        {
            ToTable(@"PlaylistVideos", @"public");

            Property(x => x.Order).HasColumnName("Order");
            Property(x => x.PlaylistId).HasColumnName("PlaylistId");
            Property(x => x.VideoId).HasColumnName("VideoId");

            HasKey(x => new { x.PlaylistId, x.VideoId });
        }
    }
}
