﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class PlaylistMap : VDEntityMapBase<PlaylistEntity>
    {
        public PlaylistMap()
        {
            ToTable(@"Playlist", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Description).HasColumnName("Description");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.IsPublic).HasColumnName("IsPublic");
            Property(x => x.Name).HasColumnName("Name");
            Property(x => x.Thumbnail).HasColumnName("Thumbnail");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.UserPartnerId).HasColumnName("UserPartnerId");
        }
    }
}
