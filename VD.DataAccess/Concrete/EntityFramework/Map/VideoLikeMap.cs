﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoLikeMap : VDEntityMapBase<VideoLikeEntity>
    {
        public VideoLikeMap()
        {
            ToTable(@"VideoLikes", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.IsLike).HasColumnName("IsLike");
            Property(x => x.UpdatedAt).HasColumnName("UpdatedAt");
            Property(x => x.UserPartnerId).HasColumnName("UserPartnerId");
            Property(x => x.VideoId).HasColumnName("VideoId");
        }
    }
}
