﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class MailLogMap : VDEntityMapBase<MailLogEntity>
    {
        public MailLogMap()
        {
            ToTable(@"MailLogs", @"public");

            Property(x => x.Bcc).HasColumnName("Bcc");
            Property(x => x.Body).HasColumnName("Body");
            Property(x => x.Cc).HasColumnName("Cc");
            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.FromEmail).HasColumnName("FromEmail");
            Property(x => x.FromName).HasColumnName("FromName");
            Property(x => x.SenderIp).HasColumnName("SenderIp");
            Property(x => x.Subject).HasColumnName("Subject");
            Property(x => x.ToEmail).HasColumnName("ToEmail");
            Property(x => x.ToName).HasColumnName("ToName");
        }
    }
}
