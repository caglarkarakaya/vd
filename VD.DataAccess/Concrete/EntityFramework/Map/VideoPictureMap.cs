﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Entity.Concrete;

namespace VD.DataAccess.Concrete.EntityFramework.Map
{
    public class VideoPictureMap : VDEntityMapBase<VideoPictureEntity>
    {
        public VideoPictureMap()
        {
            ToTable(@"VideoPictures", @"public");

            Property(x => x.CreatedAt).HasColumnName("CreatedAt");
            Property(x => x.Height).HasColumnName("Height");
            Property(x => x.Url).HasColumnName("Url");
            Property(x => x.VideoId).HasColumnName("VideoId");
            Property(x => x.VideoMD5).HasColumnName("VideoMD5");
            Property(x => x.Width).HasColumnName("Width");
        }
    }
}
