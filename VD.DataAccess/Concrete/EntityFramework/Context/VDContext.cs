﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.DataAccess;
using VD.DataAccess.Concrete.EntityFramework.Map;

namespace VD.DataAccess.Concrete.EntityFramework.Context
{
    public class VDContext : DbContext
    {
        public VDContext()
        {
            Database.SetInitializer<VDContext>(null);
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public VDContext(string connectionString)
        {
            Database.Connection.ConnectionString = connectionString;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AdminRoleMap());
            modelBuilder.Configurations.Add(new AdminUserMap());
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new VideoMap());
            modelBuilder.Configurations.Add(new CategoryCssClassMap());
            modelBuilder.Configurations.Add(new ClientLocationInformationMap());
            modelBuilder.Configurations.Add(new CountryMap());
            modelBuilder.Configurations.Add(new CountryIpRangeMap());
            modelBuilder.Configurations.Add(new ElasticSearchSyncMap());
            modelBuilder.Configurations.Add(new EmailConfigMap());
            modelBuilder.Configurations.Add(new ErrorLogMap());
            modelBuilder.Configurations.Add(new LanguageMap());
            modelBuilder.Configurations.Add(new MailLogMap());
            modelBuilder.Configurations.Add(new PartnerDomainMap());
            modelBuilder.Configurations.Add(new PartnerTypeMap());
            modelBuilder.Configurations.Add(new PartnerMap());
            modelBuilder.Configurations.Add(new PlaylistMap());
            modelBuilder.Configurations.Add(new ReportedVideoMap());
            modelBuilder.Configurations.Add(new RssFeedMap());
            modelBuilder.Configurations.Add(new SlugMap());
            modelBuilder.Configurations.Add(new SupportRequestMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserPartnerMap());
            modelBuilder.Configurations.Add(new VideoFileMap());
            modelBuilder.Configurations.Add(new VideoGrabberRequestDetailMap());
            modelBuilder.Configurations.Add(new VideoGrabberRequestMap());
            modelBuilder.Configurations.Add(new VideoGrabberTypeMap());
            modelBuilder.Configurations.Add(new VideoLikeMap());
            modelBuilder.Configurations.Add(new VideoPictureMap());
            modelBuilder.Configurations.Add(new VideoStatisticMap());
            modelBuilder.Configurations.Add(new VideoTagMap());
            modelBuilder.Configurations.Add(new PlaylistVideoMap());
        }
    }
}
