﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DataAccess.Concrete.EntityFramework.DbConfig
{
    public class VDDbConfiguration : DbConfiguration
    {
        public VDDbConfiguration()
            : base()
        {
            var path = Path.GetDirectoryName(this.GetType().Assembly.Location);
            SetModelStore(new DefaultDbModelStore(path));
        }
    }
}
