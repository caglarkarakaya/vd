﻿using System.ServiceProcess;

namespace VD.ElasticSynchronizer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new VDElasticVideoSynchronizerService(),
                new VDElasticChannelSynchronizerService(),
                new VDElasticPlaylistSynchronizerService(),
                new VDElasticHottestVideoSynchronizerService(),
                new VDElasticTrendingVideoSynchronizerService(),
                new VDElasticRecommendedVideoSynchronizerService(),
                new VDElasticCategorySynchronizerService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
