﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace VD.ElasticSynchronizer
{
    [RunInstaller(true)]
    public partial class VDElasticSynchronizerServiceInstaller : Installer
    {
        private ServiceInstaller _channelInstaller;
        private ServiceInstaller _playlistInstaller;
        private ServiceInstaller _videoInstaller;
        private ServiceInstaller _hottestvideoInstaller;
        private ServiceInstaller _trendingvideoInstaller;
        private ServiceInstaller _recommendedvideoInstaller;
        private ServiceInstaller _categoryInstaller;
        private ServiceProcessInstaller processInstaller;

        public VDElasticSynchronizerServiceInstaller()
        {
            processInstaller = new ServiceProcessInstaller();

            _channelInstaller = new ServiceInstaller();
            _playlistInstaller = new ServiceInstaller();
            _videoInstaller = new ServiceInstaller();
            _hottestvideoInstaller = new ServiceInstaller();
            _trendingvideoInstaller = new ServiceInstaller();
            _recommendedvideoInstaller = new ServiceInstaller();
            _categoryInstaller = new ServiceInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;

            _channelInstaller.StartType = ServiceStartMode.Automatic;
            _playlistInstaller.StartType = ServiceStartMode.Automatic;
            _videoInstaller.StartType = ServiceStartMode.Automatic;
            _hottestvideoInstaller.StartType = ServiceStartMode.Automatic;
            _trendingvideoInstaller.StartType = ServiceStartMode.Automatic;
            _recommendedvideoInstaller.StartType = ServiceStartMode.Automatic;
            _categoryInstaller.StartType = ServiceStartMode.Automatic;

            _channelInstaller.ServiceName = "VDElasticChannelSynchronizerService";
            _playlistInstaller.ServiceName = "VDElasticPlaylistSynchronizerService";
            _videoInstaller.ServiceName = "VDElasticVideoSynchronizerService";
            _hottestvideoInstaller.ServiceName = "VDElasticHottestVideoSynchronizerService";
            _trendingvideoInstaller.ServiceName = "VDElasticTrendingVideoSynchronizerService";
            _recommendedvideoInstaller.ServiceName = "VDElasticRecommendedVideoSynchronizerService";
            _categoryInstaller.ServiceName = "VDElasticCategorySynchronizerService";

            Installers.Add(_playlistInstaller);
            Installers.Add(_channelInstaller);
            Installers.Add(_videoInstaller);
            Installers.Add(_hottestvideoInstaller);
            Installers.Add(_trendingvideoInstaller);
            Installers.Add(_recommendedvideoInstaller);
            Installers.Add(_categoryInstaller);

            Installers.Add(processInstaller);
        }
    }
}
