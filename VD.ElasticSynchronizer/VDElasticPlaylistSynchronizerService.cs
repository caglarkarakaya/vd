﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using VD.Business.Concrete;

namespace VD.ElasticSynchronizer
{
    partial class VDElasticPlaylistSynchronizerService : ServiceBase
    {
        bool _isPlaylistSynchronizerProcessing;
        Thread _playlistSynchronizerWorker;

        public VDElasticPlaylistSynchronizerService()
        {
            InitializeComponent();
        }

        private void SynchronizePlaylists()
        {
            while (_isPlaylistSynchronizerProcessing)
            {
                Thread.Sleep(new TimeSpan(0, 1, 0));

                try
                {
                    PlaylistBusinessObject playlistBusinessObject = new PlaylistBusinessObject();
                    playlistBusinessObject.SynchronizeElasticPlaylistDetails();
                }
                catch (Exception ex)
                {
                    StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                    var writingMessage = DateTime.Now.ToString() + "VDElasticPlaylistSynchronizer failed" + ex.Message;
                    sw.WriteLine(writingMessage);
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        protected override void OnStart(string[] args)
        {
            _isPlaylistSynchronizerProcessing = true;
            _playlistSynchronizerWorker = new Thread(new ThreadStart(SynchronizePlaylists));
            _playlistSynchronizerWorker.Start();
        }

        protected override void OnStop()
        {
            _isPlaylistSynchronizerProcessing = false;
            _playlistSynchronizerWorker.Join();
        }
    }
}
