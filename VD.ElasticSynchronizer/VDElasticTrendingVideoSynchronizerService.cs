﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VD.Business.Concrete;

namespace VD.ElasticSynchronizer
{
    partial class VDElasticTrendingVideoSynchronizerService : ServiceBase
    {
        bool _isProcessing;
        Thread _synchronizeWorker;

        public VDElasticTrendingVideoSynchronizerService()
        {
            InitializeComponent();
        }

        private void SynchronizeNext()
        {
            while (_isProcessing)
            {
                Thread.Sleep(new TimeSpan(0, 1, 0));

                try
                {
                    VideoBusinessObject videoBusinessObject = new VideoBusinessObject();
                    videoBusinessObject.SynchronizeElasticTrendingVideoDetails();
                }
                catch (Exception ex)
                {
                    StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                    var writingMessage = DateTime.Now.ToString() + "VDElasticTrendingVideoSynchronizerService failed" + ex.Message;
                    sw.WriteLine(writingMessage);
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        protected override void OnStart(string[] args)
        {
            _isProcessing = true;
            _synchronizeWorker = new Thread(new ThreadStart(SynchronizeNext));
            _synchronizeWorker.Start();
        }

        protected override void OnStop()
        {
            _isProcessing = false;
            _synchronizeWorker.Join();
        }
    }
}
