﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using VD.Business.Concrete;

namespace VD.ElasticSynchronizer
{
    partial class VDElasticChannelSynchronizerService : ServiceBase
    {
        bool _isProcessing;
        Thread _synchronizeWorker;

        public VDElasticChannelSynchronizerService()
        {
            InitializeComponent();
        }

        private void SynchronizeNext()
        {
            while (_isProcessing)
            {
                Thread.Sleep(new TimeSpan(0, 1, 0));

                try
                {
                    PartnerBusinessObject partnerBusinessObject = new PartnerBusinessObject();
                    partnerBusinessObject.SynchronizeElasticChannelDetails();
                }
                catch (Exception ex)
                {
                    StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                    var writingMessage = DateTime.Now.ToString() + "VDElasticChannelSynchronizer failed" + ex.Message;
                    sw.WriteLine(writingMessage);
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        protected override void OnStart(string[] args)
        {
            _isProcessing = true;
            _synchronizeWorker = new Thread(new ThreadStart(SynchronizeNext));
            _synchronizeWorker.Start();
        }

        protected override void OnStop()
        {
            _isProcessing = false;
            _synchronizeWorker.Join();
        }
    }
}
