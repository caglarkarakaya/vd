﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Entity;

namespace VD.Entity.Concrete
{
    public class PlaylistVideoEntity : IEntity
    {
        public int PlaylistId { get; set; }
        public int VideoId { get; set; }
        public int Order { get; set; }
        [NotMapped]
        public int Id { get; set; }
    }
}
