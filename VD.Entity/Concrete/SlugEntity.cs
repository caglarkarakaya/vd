﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class SlugEntity : VDEntityBase
    {
        public string VideoMD5 { get; set; }
        public string Key { get; set; }
        public bool IsActive { get; set; }
        public int VideoId { get; set; }
    }
}
