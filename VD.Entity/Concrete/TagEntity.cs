﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class TagEntity : VDEntityBase
    {
        public string Title { get; set; }
    }
}
