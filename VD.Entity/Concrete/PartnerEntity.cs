﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class PartnerEntity :VDEntityBase
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int CountryId { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
        public string ReportPartnerId { get; set; }
        public DateTime CreatedAt { get; set; }
        public long? TotalStorage { get; set; }
        public string ShortCode { get; set; }
        public int? PartnerTypeId { get; set; }
        public bool? SupportAccess { get; set; }
        public bool? MultiUploadAccess { get; set; }
        public bool? IsTrusted { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string CardImageUrl { get; set; }
        public string BackgroundImageUrl { get; set; }
        public string AvatarUrl { get; set; }
        public string Description { get; set; }
    }
}
