﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class ReportedVideoEntity : VDEntityBase
    {
        public short? ReasonId { get; set; }
        public string Url { get; set; }
        public string VideoMD5 { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool? IsResolved { get; set; }
        public int VideoId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPositive { get; set; }
    }
}
