﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class UserPartnerEntity : VDEntityBase
    {
        public int UserId { get; set; }
        public int PartnerId { get; set; }
        public int UserRoleId { get; set; }
        public bool IsContactUser { get; set; }
    }
}
