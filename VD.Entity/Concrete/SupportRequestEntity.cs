﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class SupportRequestEntity :VDEntityBase
    {
        public int UserPartnerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string CompanyName { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string BrowserType { get; set; }
        public string BrowserName { get; set; }
        public string BrowserVersion { get; set; }
        public string BrowserPlatform { get; set; }
        public string BrowserIsBeta { get; set; }
        public string BrowserIsAOL { get; set; }
        public string BrowserIsWin32 { get; set; }
        public string BrowserFrames { get; set; }
        public string BrowserTables { get; set; }
        public string BrowserCookies { get; set; }
        public bool IsActive { get; set; }
        public string IpAddress { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
