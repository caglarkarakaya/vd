﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class PartnerDomainEntity : VDEntityBase
    {
        public int PartnerId { get; set; }
        public string Domain { get; set; }
        public string ColorText { get; set; }
        public string ColorBG { get; set; }
        public string ColorProgressBar { get; set; }
        public string LogoUrl { get; set; }
        public string ThemeUrl { get; set; }
        public bool IsActive { get; set; }
        public bool AdsEnabled { get; set; }
        public bool NotificationEnabled { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool AdsAutoStart { get; set; }
        public string ColorButton { get; set; }
        public short StateId { get; set; }
        public bool PreRollEnabled { get; set; }
        public bool SubBannerEnabled { get; set; }
        public int? PreRollSecond { get; set; }
        public int? SubBannerSecond { get; set; }
        public bool LogoEnabled { get; set; }
        public bool FullScreenEnabled { get; set; }
        public bool ShareButtonEnabled { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
    }
}
