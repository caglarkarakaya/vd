﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class PartnerTypeEntity : VDEntityBase
    {
        public string Name { get; set; }
        public int BandwidthLimit { get; set; }
        public int StorageLimit { get; set; }
        public int DomainLimit { get; set; }
        public int UserLimit { get; set; }
        public int PlaylistLimit { get; set; }
        public int ChannelLimit { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
