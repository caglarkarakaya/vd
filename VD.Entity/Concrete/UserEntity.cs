﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class UserEntity : VDEntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string LastLoginIp { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Password { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
