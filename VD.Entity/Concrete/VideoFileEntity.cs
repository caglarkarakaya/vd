﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoFileEntity : VDEntityBase
    {
        public string VideoMD5 { get; set; }
        public string Quality { get; set; }
        public short VideoTypeId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Fps { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
        public string UrlSecure { get; set; }
        public DateTime CreatedAt { get; set; }
        public int VideoId { get; set; }
    }
}
