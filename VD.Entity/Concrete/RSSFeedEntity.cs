﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class RssFeedEntity : VDEntityBase
    {
        public string ProviderName { get; set; }
        public int? ProviderId { get; set; }
        public string Url { get; set; }
        public int? ScheduleInMinute { get; set; }
        public int PartnerId { get; set; }
        public int CategoryId { get; set; }
        public int LanguageId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool IsActive { get; set; }
        public int? ProviderType { get; set; }
    }
}
