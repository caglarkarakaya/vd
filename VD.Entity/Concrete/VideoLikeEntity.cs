﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoLikeEntity : VDEntityBase
    {
        public int VideoId { get; set; }
        public int UserPartnerId { get; set; }
        public bool IsLike { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
