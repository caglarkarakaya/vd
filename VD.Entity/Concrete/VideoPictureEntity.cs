﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoPictureEntity : VDEntityBase
    {
        public string VideoMD5 { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Url { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int VideoId { get; set; }
    }
}
