﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class CountryIpRangeEntity :VDEntityBase
    {
        public long IpStart { get; set; }
        public long IpEnd { get; set; }
        public string ISOCode { get; set; }
        public int CountryId { get; set; }
    }
}
