﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Entity;

namespace VD.Entity.Concrete
{
    public class VDEntityBase : IEntity
    {
        public int Id { get; set; }
    }
}
