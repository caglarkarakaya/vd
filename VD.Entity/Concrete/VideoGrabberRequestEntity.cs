﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoGrabberRequestEntity : VDEntityBase
    {
        public string EncryptRequestId { get; set; }
        public int UserPartnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int VideoGrabberTypeId { get; set; }
        public int State { get; set; }
        public string ProcessResult { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public string SourceUrl { get; set; }
    }
}
