﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class EmailConfigEntity : VDEntityBase
    {
        public string Code { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
        public bool IsActive { get; set; }
        public string Signature { get; set; }
        public string Template { get; set; }
        public string TemplateParams { get; set; }
    }
}
