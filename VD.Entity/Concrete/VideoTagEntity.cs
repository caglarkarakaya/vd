﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoTagEntity :VDEntityBase
    {
        public int TagId { get; set; }
        public int VideoId { get; set; }
    }
}
