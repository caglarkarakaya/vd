﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class CountryEntity : VDEntityBase
    {
        public string ISO { get; set; }
        public string ISO3 { get; set; }
        public string Name { get; set; }
        public string ContinentCode { get; set; }
        public string ContinentName { get; set; }
    }
}
