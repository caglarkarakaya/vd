﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class LanguageEntity : VDEntityBase
    {
        public string Name { get; set; }
        public string CultureCode { get; set; }
    }
}
