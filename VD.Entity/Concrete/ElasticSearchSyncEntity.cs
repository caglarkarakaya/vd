﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class ElasticSearchSyncEntity : VDEntityBase
    {
        public string Type { get; set; }
        public DateTime LastExecutionDateTime { get; set; }
        public string ExecutionResult { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Description { get; set; }
    }
}
