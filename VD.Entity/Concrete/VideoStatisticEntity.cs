﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoStatisticEntity : VDEntityBase
    {
        public string VideoMD5 { get; set; }
        public long LoadCount { get; set; }
        public long ViewCount { get; set; }
        public int VideoId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
