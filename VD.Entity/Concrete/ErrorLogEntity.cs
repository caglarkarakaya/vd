﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class ErrorLogEntity : VDEntityBase
    {
        public string Ip { get; set; }
        public short? ErrorType { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
