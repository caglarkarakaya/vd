﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoGrabberRequestDetailEntity :VDEntityBase
    {
        public int VideoGrabberRequestId { get; set; }
        public string VideoTitle { get; set; }
        public string VideoImage { get; set; }
        public string VideoDownloadUrl { get; set; }
        public int State { get; set; }
        public string ProcessResult { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public string VideoMD5 { get; set; }
        public int? CategoryId { get; set; }
        public int? LanguageId { get; set; }
        public string VideoDescription { get; set; }
        public string VideoSourceId { get; set; }
        public int? VideoId { get; set; }
    }
}
