﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class AdminRoleEntity : VDEntityBase
    {
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
    }
}
