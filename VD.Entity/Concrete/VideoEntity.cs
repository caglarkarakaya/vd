﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class VideoEntity : VDEntityBase
    {
        public string VideoMD5 { get; set; }
        public int UserPartnerId { get; set; }
        public int? CategoryId { get; set; }
        public int? PrivacyId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public string Thumbnail { get; set; }
        public short? ApproveStatusId { get; set; }
        public DateTime? ApprovalChangedAt { get; set; }
        public bool IsActive { get; set; }
        public int? TranscodeId { get; set; }
        public short? TranscodeStatusId { get; set; }
        public DateTime? TranscodeStartedAt { get; set; }
        public DateTime? TranscodeEndedAt { get; set; }
        public string TranscodeDescription { get; set; }
        public string TranscodeSubfolder { get; set; }
        public double? Duration { get; set; }
        public string ResourceGuid { get; set; }
        public int? RssFeedId { get; set; }
        public string BannedCountries { get; set; }
        public long? VideoSize { get; set; }
        public string ResourceUrl { get; set; }
        public DateTime CreatedAt { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? LanguageId { get; set; }
        public string RejectReason { get; set; }
        public long? LikeCount { get; set; }
        public long? DislikeCount { get; set; }
        public bool? IsHomePageTopVideo { get; set; }
        public int? VimeoAccountId { get; set; }
        public string VimeoUri { get; set; }
        public string VimeoState { get; set; }
        public int? TranscodeCheckTrialCount { get; set; }
    }
}