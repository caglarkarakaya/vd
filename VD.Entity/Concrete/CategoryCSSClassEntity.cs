﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class CategoryCssClassEntity : VDEntityBase
    {
        public string CSSClassName { get; set; }
        public int? CategoryId { get; set; }
        public bool IsActive { get; set; }
    }
}
