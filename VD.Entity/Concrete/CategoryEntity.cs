﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Entity.Concrete
{
    public class CategoryEntity : VDEntityBase
    {
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public string IconUrl { get; set; }
    }
}
