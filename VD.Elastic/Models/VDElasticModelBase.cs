﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Elastic.Models
{
    public abstract class VDElasticModelBase
    {
        internal abstract string IndexName { get; }
        internal abstract string TypeName { get; }
        internal abstract Uri[] Uri { get; }        
    }
}
