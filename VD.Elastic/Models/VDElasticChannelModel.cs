﻿using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Elastic.Models
{
    [ElasticsearchType(Name = "channeldetail", IdProperty = "Id")]
    [Serializable]
    public class VDElasticChannelModel : VDElasticModelBase
    {
        internal override string IndexName { get => ConfigurationManager.AppSettings["ElasticChannelDetailIndexName"]; }
        internal override string TypeName { get => "channeldetail"; }
        internal override Uri[] Uri
        {
            get
            {
                var uris = new Uri[]
                {
                    new Uri(ConfigurationManager.AppSettings["ElasticSearchUri"])
                };

                return uris;
            }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int VideoCount { get; set; }
        public long ViewCount { get; set; }
        public string CardImageUrl { get; set; }
        public string BackgroundImageUrl { get; set; }
        public string AvatarUrl { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public List<VDElasticChannelsCategoryInfoModel> CategoryInfos { get; set; }
    }
}
