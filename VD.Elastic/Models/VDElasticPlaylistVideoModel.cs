﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Elastic.Models
{
    public class VDElasticPlaylistVideoModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string SlugKey { get; set; }
        public string ThumbnailUrl { get; set; }
        public string VideoCardImageUrl { get; set; }
        public int VideoId { get; set; }
        public int Order { get; set; }
        public double? Duration { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long ViewCount { get; set; }
        public string VideoMD5 { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string ChannelAvatarUrl { get; set; }
        public int PrivacyId { get; set; }
    }
}
