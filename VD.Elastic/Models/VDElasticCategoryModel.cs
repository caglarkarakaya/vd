﻿using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Elastic.Models
{
    [ElasticsearchType(Name = "categorydetail", IdProperty = "Id")]
    [Serializable]
    public class VDElasticCategoryModel : VDElasticModelBase
    {
        internal override string IndexName { get => ConfigurationManager.AppSettings["ElasticCategoryDetailIndexName"]; }
        internal override string TypeName { get => "categorydetail"; }
        internal override Uri[] Uri
        {
            get
            {
                var uris = new Uri[]
                {
                    new Uri(ConfigurationManager.AppSettings["ElasticSearchUri"])
                };

                return uris;
            }
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string IconUrl { get; set; }
        public int VideoCount { get; set; }
    }
}
