﻿using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Elastic.Models
{
    [ElasticsearchType(Name = "playlistdetail", IdProperty = "Id")]
    [Serializable]
    public class VDElasticPlaylistModel : VDElasticModelBase
    {
        internal override string IndexName { get => ConfigurationManager.AppSettings["ElasticPlaylistDetailIndexName"]; }
        internal override string TypeName { get => "playlistdetail"; }
        internal override Uri[] Uri
        {
            get
            {
                var uris = new Uri[]
                {
                    new Uri(ConfigurationManager.AppSettings["ElasticSearchUri"])
                };

                return uris;
            }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbnailUrl { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool IsPublic { get; set; }
        public int Count { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        [Nest.Nested]
        public List<VDElasticPlaylistVideoModel> VideoDetails { get; set; }
    }
}
