﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.Elastic.Models
{
    [Serializable]
    public class VidisVideoModel : VDElasticModelBase
    {
        internal override string IndexName { get => "vidis"; }
        internal override string TypeName { get => "videos"; }
        internal override Uri[] Uri
        {
            get
            {
                var uris = new Uri[]
                {
                    new Uri(ConfigurationManager.AppSettings["ElasticSearchUri"])
                };

                return uris;
            }
        }

        public int Id { get; set; }
        public string VideoMD5 { get; set; }
        public int? UserPartnerId { get; set; }
        public int PartnerId { get; set; }
        public string PartnerInfo { get; set; }
        public string PartnerShortCode { get; set; }
        public string PartnerWithSlugKey { get; set; }
        public bool PartnerIsActive { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryNiceTitle { get; set; }
        public string CategoryTitle { get; set; }
        public string CategoryCssClass { get; set; }
        public int? PrivacyId { get; set; }
        public string Title { get; set; }
        public string TitleFull { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public string Thumbnail { get; set; }
        public short? ApproveStatusId { get; set; }
        public DateTime? ApprovalChangedAt { get; set; }
        public bool IsActive { get; set; }
        public int? TranscodeId { get; set; }
        public short? TranscodeStatusId { get; set; }
        public DateTime? TranscodeStartedAt { get; set; }
        public DateTime? TranscodeEndedAt { get; set; }
        public string TranscodeDescription { get; set; }
        public string TranscodeSubfolder { get; set; }
        public string Duration { get; set; }
        public double? DurationDouble { get; set; }
        public string BannedCountries { get; set; }
        public long? VideoSize { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int CreaterUserId { get; set; }
        public string CreatedName { get; set; }
        public bool CreaterIsActive { get; set; }
        public int? UpdatedBy { get; set; }
        public string UpdatedByName { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCultureCode { get; set; }
        public string RejectReason { get; set; }
        public string LikeCount { get; set; }
        public string DislikeCount { get; set; }
        public bool? IsHomePageTopVideo { get; set; }
        public int? VimeoAccountId { get; set; }
        public string VimeoUri { get; set; }
        public string VimeoState { get; set; }
        public int? TranscodeCheckTrialCount { get; set; }
        public string SlugKey { get; set; }
        public long ViewedCount { get; set; }
        public long LoadCount { get; set; }
        public bool? IsLike { get; set; }
        public string VideoTags { get; set; }
    }
}
