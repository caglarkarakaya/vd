﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Elastic.Models;

namespace VD.Elastic.Client
{
    public class VDElasticClient<T>
        where T : VDElasticModelBase, new()
    {
        private readonly ElasticClient _elasticClient;

        public VDElasticClient()
        {
            var model = new T();

            var connectionPool = new StaticConnectionPool(model.Uri);
            var connectionSettings = new ConnectionSettings(connectionPool);
            connectionSettings.DefaultIndex(model.IndexName);
            connectionSettings.DefaultTypeName(model.TypeName);

            _elasticClient = new ElasticClient(connectionSettings);

            //connectionSettings.DisableDirectStreaming(true);
            //connectionSettings.OnRequestCompleted(call =>
            //{
            //    Debug.Write(Encoding.UTF8.GetString(call.RequestBodyInBytes));
            //});
        }

        public ElasticClient Client
        {
            get
            {
                return _elasticClient;
            }
        }
    }
}
