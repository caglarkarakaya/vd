﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.DTO.Concrete;
using VD.Elastic.Models;

namespace VD.Elastic.Extentions
{
    public static class ElasticModelExtentions
    {
        public static VideoDetailDTO AsVideoDetailDTO(this VDElasticVideoDetailModel vdElasticVideoDetailModel)
        {
            VideoDetailDTO videoDetailDTO = new VideoDetailDTO();
            try
            {
                videoDetailDTO.CategoryId = vdElasticVideoDetailModel.CategoryId;
                videoDetailDTO.CategoryName = vdElasticVideoDetailModel.CategoryName;
                videoDetailDTO.ChannelAvatarUrl = vdElasticVideoDetailModel.ChannelAvatarUrl;
                videoDetailDTO.ChannelId = vdElasticVideoDetailModel.ChannelId;
                videoDetailDTO.ChannelName = vdElasticVideoDetailModel.ChannelName;
                videoDetailDTO.CreatedAt = vdElasticVideoDetailModel.CreatedAt.HasValue ? vdElasticVideoDetailModel.CreatedAt.Value : DateTime.MinValue;
                videoDetailDTO.Description = vdElasticVideoDetailModel.Description;
                videoDetailDTO.Duration = vdElasticVideoDetailModel.Duration;
                videoDetailDTO.SlugKey = vdElasticVideoDetailModel.SlugKey;
                videoDetailDTO.ThumbnailUrl = vdElasticVideoDetailModel.ThumbnailUrl;
                videoDetailDTO.VideoCardImageUrl = vdElasticVideoDetailModel.VideoCardImageUrl;
                videoDetailDTO.Title = vdElasticVideoDetailModel.Title;
                videoDetailDTO.VideoId = vdElasticVideoDetailModel.VideoId;
                videoDetailDTO.VideoMD5 = vdElasticVideoDetailModel.VideoMD5;
                videoDetailDTO.ViewCount = vdElasticVideoDetailModel.ViewCount;
            }
            catch
            {
                return null;
            }

            return videoDetailDTO;
        }

        public static VideoInfoDTO AsVideoInfoDTO(this VDElasticVideoDetailModel vdElasticVideoDetailModel)
        {
            VideoInfoDTO videoInfoDTO = new VideoInfoDTO();
            try
            {
                videoInfoDTO.CategoryId = vdElasticVideoDetailModel.CategoryId;
                videoInfoDTO.ChannelName = vdElasticVideoDetailModel.ChannelName;
                videoInfoDTO.CreatedAt = vdElasticVideoDetailModel.CreatedAt.HasValue ? vdElasticVideoDetailModel.CreatedAt.Value : DateTime.MinValue;
                videoInfoDTO.Duration = vdElasticVideoDetailModel.Duration;
                videoInfoDTO.SlugKey = vdElasticVideoDetailModel.SlugKey;
                videoInfoDTO.ThumbnailUrl = vdElasticVideoDetailModel.ThumbnailUrl;
                videoInfoDTO.VideoCardImageUrl = vdElasticVideoDetailModel.VideoCardImageUrl;
                videoInfoDTO.Title = vdElasticVideoDetailModel.Title;
                videoInfoDTO.VideoId = vdElasticVideoDetailModel.VideoId;
                videoInfoDTO.VideoMD5 = vdElasticVideoDetailModel.VideoMD5;
                videoInfoDTO.ViewCount = vdElasticVideoDetailModel.ViewCount;
            }
            catch
            {
                return null;
            }

            return videoInfoDTO;
        }

        public static PlaylistVideoInfoDTO AsPlaylistVideoInfoDTO(this VDElasticPlaylistVideoModel vdElasticPlaylistVideoModel)
        {
            PlaylistVideoInfoDTO playlistVideoInfoDTO = new PlaylistVideoInfoDTO();
            try
            {
                playlistVideoInfoDTO.CategoryId = vdElasticPlaylistVideoModel.CategoryId;
                playlistVideoInfoDTO.ChannelName = vdElasticPlaylistVideoModel.ChannelName;
                playlistVideoInfoDTO.CreatedAt = vdElasticPlaylistVideoModel.CreatedAt.HasValue ? vdElasticPlaylistVideoModel.CreatedAt.Value : DateTime.MinValue;
                playlistVideoInfoDTO.Duration = vdElasticPlaylistVideoModel.Duration;
                playlistVideoInfoDTO.SlugKey = vdElasticPlaylistVideoModel.SlugKey;
                playlistVideoInfoDTO.ThumbnailUrl = vdElasticPlaylistVideoModel.ThumbnailUrl;
                playlistVideoInfoDTO.VideoCardImageUrl = vdElasticPlaylistVideoModel.VideoCardImageUrl;
                playlistVideoInfoDTO.Title = vdElasticPlaylistVideoModel.Title;
                playlistVideoInfoDTO.VideoId = vdElasticPlaylistVideoModel.VideoId;
                playlistVideoInfoDTO.VideoMD5 = vdElasticPlaylistVideoModel.VideoMD5;
                playlistVideoInfoDTO.ViewCount = vdElasticPlaylistVideoModel.ViewCount;
                playlistVideoInfoDTO.Order = vdElasticPlaylistVideoModel.Order;
            }
            catch
            {
                return null;
            }

            return playlistVideoInfoDTO;
        }

        public static List<PlaylistVideoInfoDTO> AsPlaylistVideoInfoDTOList(this List<VDElasticPlaylistVideoModel> vdElasticPlaylistVideoModels)
        {
            List<PlaylistVideoInfoDTO> playlistVideoDetailDTOs = new List<PlaylistVideoInfoDTO>();

            try
            {
                foreach (var item in vdElasticPlaylistVideoModels)
                {
                    playlistVideoDetailDTOs.Add(item.AsPlaylistVideoInfoDTO());
                }
            }
            catch
            {
                return null;
            }

            return playlistVideoDetailDTOs;
        }

        public static List<VideoDetailDTO> AsVideoDetailDTOList(this List<VDElasticVideoDetailModel> vdElasticVideoDetailModels)
        {
            List<VideoDetailDTO> videoDetailDTOs = new List<VideoDetailDTO>();

            try
            {
                foreach (var item in vdElasticVideoDetailModels)
                {
                    videoDetailDTOs.Add(item.AsVideoDetailDTO());
                }
            }
            catch
            {
                return null;
            }

            return videoDetailDTOs;
        }

        public static List<VideoInfoDTO> AsVideoInfoDTOList(this List<VDElasticVideoDetailModel> vdElasticVideoDetailModels)
        {
            List<VideoInfoDTO> videoDetailDTOs = new List<VideoInfoDTO>();

            try
            {
                foreach (var item in vdElasticVideoDetailModels)
                {
                    videoDetailDTOs.Add(item.AsVideoInfoDTO());
                }
            }
            catch
            {
                return null;
            }

            return videoDetailDTOs;
        }

        public static ChannelInfoDTO AsChannelInfoDTO(this VDElasticChannelModel vdElasticChannelModel)
        {
            ChannelInfoDTO channelInfoDTO = new ChannelInfoDTO();
            try
            {
                channelInfoDTO.AvatarUrl = vdElasticChannelModel.AvatarUrl;
                channelInfoDTO.BackgroundImageUrl = vdElasticChannelModel.BackgroundImageUrl;
                channelInfoDTO.CardImageUrl = vdElasticChannelModel.CardImageUrl;
                channelInfoDTO.Description = vdElasticChannelModel.Description;
                channelInfoDTO.Id = vdElasticChannelModel.Id;
                channelInfoDTO.Name = vdElasticChannelModel.Name;
                channelInfoDTO.VideoCount = vdElasticChannelModel.VideoCount;
                channelInfoDTO.ViewCount = vdElasticChannelModel.ViewCount;
            }
            catch
            {
                return null;
            }

            return channelInfoDTO;
        }

        public static List<ChannelInfoDTO> AsChannelInfoDTOList(this List<VDElasticChannelModel> vdElasticChannelModel)
        {
            List<ChannelInfoDTO> channelInfoDTOList = new List<ChannelInfoDTO>();
            try
            {
                foreach (var item in vdElasticChannelModel)
                {
                    channelInfoDTOList.Add(item.AsChannelInfoDTO());
                }
            }
            catch
            {
                return null;
            }

            return channelInfoDTOList;
        }

        public static CategoryDTO AsCategoryDTO(this VDElasticCategoryModel vdElasticCategoryModel)
        {
            CategoryDTO categoryDTO = new CategoryDTO();
            try
            {
                categoryDTO.Id = vdElasticCategoryModel.Id;
                categoryDTO.Title = vdElasticCategoryModel.Title;
                categoryDTO.IconUrl = vdElasticCategoryModel.IconUrl;
                categoryDTO.VideoCount = vdElasticCategoryModel.VideoCount;
            }
            catch
            {
                return null;
            }

            return categoryDTO;
        }

        public static List<CategoryDTO> AsCategoryDTOList(this List<VDElasticCategoryModel> vdElasticCategoryModel)
        {
            List<CategoryDTO> categoryDTOs = new List<CategoryDTO>();
            try
            {
                foreach (var item in vdElasticCategoryModel)
                {
                    categoryDTOs.Add(item.AsCategoryDTO());
                }
            }
            catch
            {
                return null;
            }

            return categoryDTOs;
        }

        public static ChannelsCategoryInfoDTO AsChannelsCategoryInfoDTO(this VDElasticChannelsCategoryInfoModel vdElasticChannelsCategoryInfoModel)
        {
            ChannelsCategoryInfoDTO channelsCategoryInfoDTO = new ChannelsCategoryInfoDTO();

            try
            {
                channelsCategoryInfoDTO.CategoryId = vdElasticChannelsCategoryInfoModel.CategoryId;
                channelsCategoryInfoDTO.CategoryName = vdElasticChannelsCategoryInfoModel.CategoryName;
                channelsCategoryInfoDTO.Count = vdElasticChannelsCategoryInfoModel.Count;
            }
            catch
            {
                return null;
            }

            return channelsCategoryInfoDTO;
        }

        public static List<ChannelsCategoryInfoDTO> AsChannelsCategoryInfoDTOList(this List<VDElasticChannelsCategoryInfoModel> vdElasticChannelsCategoryInfoModels)
        {
            List<ChannelsCategoryInfoDTO> channelsCategoryInfoDTOList = new List<ChannelsCategoryInfoDTO>();

            try
            {
                foreach (var item in vdElasticChannelsCategoryInfoModels)
                {
                    channelsCategoryInfoDTOList.Add(item.AsChannelsCategoryInfoDTO());
                }
            }
            catch
            {
                return null;
            }

            return channelsCategoryInfoDTOList;
        }

        public static PlaylistInfoDTO AsPlaylistDTO(this VDElasticPlaylistModel vdElasticPlaylistModel)
        {
            PlaylistInfoDTO playlistDTO = new PlaylistInfoDTO();

            try
            {
                playlistDTO.Count = vdElasticPlaylistModel.Count;
                playlistDTO.CreatedAt = vdElasticPlaylistModel.CreatedAt.HasValue ? vdElasticPlaylistModel.CreatedAt.Value : DateTime.MinValue;
                playlistDTO.Description = vdElasticPlaylistModel.Description;
                playlistDTO.Id = vdElasticPlaylistModel.Id;
                playlistDTO.IsPublic = vdElasticPlaylistModel.IsPublic;
                playlistDTO.Name = vdElasticPlaylistModel.Name;
                playlistDTO.ThumbnailUrl = vdElasticPlaylistModel.ThumbnailUrl;
            }
            catch
            {
                return null;
            }

            return playlistDTO;
        }

        public static List<PlaylistInfoDTO> AsPlaylistDTOList(this List<VDElasticPlaylistModel> vdElasticPlaylistModels)
        {
            List<PlaylistInfoDTO> playlistDTOs = new List<PlaylistInfoDTO>();

            try
            {
                foreach (var item in vdElasticPlaylistModels)
                {
                    playlistDTOs.Add(item.AsPlaylistDTO());
                }
            }
            catch
            {
                return null;
            }

            return playlistDTOs;
        }

        public static PlaylistWithCategoryDTO AsPlaylistWithCategoryDTO(this VDElasticPlaylistModel vdElasticPlaylistModel)
        {
            PlaylistWithCategoryDTO playlistWithCategoryDTO = new PlaylistWithCategoryDTO();

            try
            {
                playlistWithCategoryDTO.Count = vdElasticPlaylistModel.Count;
                playlistWithCategoryDTO.CreatedAt = vdElasticPlaylistModel.CreatedAt.HasValue ? vdElasticPlaylistModel.CreatedAt.Value : DateTime.MinValue;
                playlistWithCategoryDTO.Description = vdElasticPlaylistModel.Description;
                playlistWithCategoryDTO.Id = vdElasticPlaylistModel.Id;
                playlistWithCategoryDTO.IsPublic = vdElasticPlaylistModel.IsPublic;
                playlistWithCategoryDTO.Name = vdElasticPlaylistModel.Name;
                playlistWithCategoryDTO.ThumbnailUrl = vdElasticPlaylistModel.ThumbnailUrl;
                playlistWithCategoryDTO.CategoryId = vdElasticPlaylistModel.CategoryId;
            }
            catch
            {
                return null;
            }

            return playlistWithCategoryDTO;
        }

        public static List<PlaylistWithCategoryDTO> AsPlaylistWithCategoryDTOList(this List<VDElasticPlaylistModel> vdElasticPlaylistModels)
        {
            List<PlaylistWithCategoryDTO> playlistWithCategoryDTOList = new List<PlaylistWithCategoryDTO>();

            try
            {
                foreach (var item in vdElasticPlaylistModels)
                {
                    playlistWithCategoryDTOList.Add(item.AsPlaylistWithCategoryDTO());
                }
            }
            catch
            {
                return null;
            }

            return playlistWithCategoryDTOList;
        }

        public static HottestVideoDTO AsHottestVideoDTO(this VDElasticHottestVideoModel vdElasticHottestVideoModel)
        {
            HottestVideoDTO hottestVideoDTO = new HottestVideoDTO();
            try
            {
                hottestVideoDTO.CreatedAt = vdElasticHottestVideoModel.CreatedAt.HasValue ? vdElasticHottestVideoModel.CreatedAt.Value : DateTime.MinValue;
                hottestVideoDTO.Duration = vdElasticHottestVideoModel.Duration;
                hottestVideoDTO.SlugKey = vdElasticHottestVideoModel.SlugKey;
                hottestVideoDTO.ThumbnailUrl = vdElasticHottestVideoModel.ThumbnailUrl;
                hottestVideoDTO.VideoCardImageUrl = vdElasticHottestVideoModel.VideoCardImageUrl;
                hottestVideoDTO.Title = vdElasticHottestVideoModel.Title;
                hottestVideoDTO.VideoId = vdElasticHottestVideoModel.VideoId;
                hottestVideoDTO.VideoMD5 = vdElasticHottestVideoModel.VideoMD5;
                hottestVideoDTO.ViewCount = vdElasticHottestVideoModel.ViewCount;
            }
            catch
            {
                return null;
            }

            return hottestVideoDTO;
        }

        public static List<HottestVideoDTO> AsHottestVideoDTOList(this List<VDElasticHottestVideoModel> vdElasticHottestVideoModels)
        {
            List<HottestVideoDTO> hottestVideoDTOs = new List<HottestVideoDTO>();

            try
            {
                foreach (var item in vdElasticHottestVideoModels)
                {
                    hottestVideoDTOs.Add(item.AsHottestVideoDTO());
                }
            }
            catch
            {
                return null;
            }

            return hottestVideoDTOs;
        }

        public static TrendingVideoDTO AsTrendingVideoDTO(this VDElasticTrendingVideoModel vdElasticTrendingVideoModel)
        {
            TrendingVideoDTO trendingVideoDTO = new TrendingVideoDTO();
            try
            {
                trendingVideoDTO.CreatedAt = vdElasticTrendingVideoModel.CreatedAt.HasValue ? vdElasticTrendingVideoModel.CreatedAt.Value : DateTime.MinValue;
                trendingVideoDTO.Duration = vdElasticTrendingVideoModel.Duration;
                trendingVideoDTO.SlugKey = vdElasticTrendingVideoModel.SlugKey;
                trendingVideoDTO.ThumbnailUrl = vdElasticTrendingVideoModel.ThumbnailUrl;
                trendingVideoDTO.VideoCardImageUrl = vdElasticTrendingVideoModel.VideoCardImageUrl;
                trendingVideoDTO.Title = vdElasticTrendingVideoModel.Title;
                trendingVideoDTO.VideoId = vdElasticTrendingVideoModel.VideoId;
                trendingVideoDTO.VideoMD5 = vdElasticTrendingVideoModel.VideoMD5;
                trendingVideoDTO.ViewCount = vdElasticTrendingVideoModel.ViewCount;
            }
            catch
            {
                return null;
            }

            return trendingVideoDTO;
        }

        public static List<TrendingVideoDTO> AsTrendingVideoDTOList(this List<VDElasticTrendingVideoModel> vdElasticTrendingVideoModels)
        {
            List<TrendingVideoDTO> trendingVideoDTOs = new List<TrendingVideoDTO>();

            try
            {
                foreach (var item in vdElasticTrendingVideoModels)
                {
                    trendingVideoDTOs.Add(item.AsTrendingVideoDTO());
                }
            }
            catch
            {
                return null;
            }

            return trendingVideoDTOs;
        }

        public static RecommendedVideoDTO AsRecommendedVideoDTO(this VDElasticRecommendedVideoModel vdElasticRecommendedVideoModel)
        {
            RecommendedVideoDTO recommendedVideoDTO = new RecommendedVideoDTO();
            try
            {
                recommendedVideoDTO.CreatedAt = vdElasticRecommendedVideoModel.CreatedAt.HasValue ? vdElasticRecommendedVideoModel.CreatedAt.Value : DateTime.MinValue;
                recommendedVideoDTO.Duration = vdElasticRecommendedVideoModel.Duration;
                recommendedVideoDTO.SlugKey = vdElasticRecommendedVideoModel.SlugKey;
                recommendedVideoDTO.ThumbnailUrl = vdElasticRecommendedVideoModel.ThumbnailUrl;
                recommendedVideoDTO.VideoCardImageUrl = vdElasticRecommendedVideoModel.VideoCardImageUrl;
                recommendedVideoDTO.Title = vdElasticRecommendedVideoModel.Title;
                recommendedVideoDTO.VideoId = vdElasticRecommendedVideoModel.VideoId;
                recommendedVideoDTO.VideoMD5 = vdElasticRecommendedVideoModel.VideoMD5;
                recommendedVideoDTO.ViewCount = vdElasticRecommendedVideoModel.ViewCount;
                recommendedVideoDTO.CategoryId = vdElasticRecommendedVideoModel.CategoryId;
            }
            catch
            {
                return null;
            }

            return recommendedVideoDTO;
        }

        public static List<RecommendedVideoDTO> AsRecommendedVideoDTOList(this List<VDElasticRecommendedVideoModel> vdElasticRecommendedVideoModels)
        {
            List<RecommendedVideoDTO> recommendedVideoDTOs = new List<RecommendedVideoDTO>();

            try
            {
                foreach (var item in vdElasticRecommendedVideoModels)
                {
                    recommendedVideoDTOs.Add(item.AsRecommendedVideoDTO());
                }
            }
            catch
            {
                return null;
            }

            return recommendedVideoDTOs;
        }
        public static StunningVideoDTO AsStunningVideoDTO(this VDElasticVideoDetailModel vdElasticVideoDetailModel)
        {
            StunningVideoDTO stunningVideoDTO = new StunningVideoDTO();
            try
            {
                stunningVideoDTO.CategoryId = vdElasticVideoDetailModel.CategoryId;
                stunningVideoDTO.CreatedAt = vdElasticVideoDetailModel.CreatedAt.HasValue ? vdElasticVideoDetailModel.CreatedAt.Value : DateTime.MinValue;
                stunningVideoDTO.Duration = vdElasticVideoDetailModel.Duration;
                stunningVideoDTO.SlugKey = vdElasticVideoDetailModel.SlugKey;
                stunningVideoDTO.ThumbnailUrl = vdElasticVideoDetailModel.ThumbnailUrl;
                stunningVideoDTO.VideoCardImageUrl = vdElasticVideoDetailModel.VideoCardImageUrl;
                stunningVideoDTO.Title = vdElasticVideoDetailModel.Title;
                stunningVideoDTO.VideoId = vdElasticVideoDetailModel.VideoId;
                stunningVideoDTO.VideoMD5 = vdElasticVideoDetailModel.VideoMD5;
                stunningVideoDTO.ViewCount = vdElasticVideoDetailModel.ViewCount;
            }
            catch
            {
                return null;
            }

            return stunningVideoDTO;
        }

        public static List<StunningVideoDTO> AsStunningVideoDTOList(this List<VDElasticVideoDetailModel> vdElasticVideoDetailModels)
        {
            List<StunningVideoDTO> stunningVideoDTOs = new List<StunningVideoDTO>();

            try
            {
                foreach (var item in vdElasticVideoDetailModels)
                {
                    stunningVideoDTOs.Add(item.AsStunningVideoDTO());
                }
            }
            catch
            {
                return null;
            }

            return stunningVideoDTOs;
        }

        public static SitemapVideoDetailDTO AsSitemapVideoDetailDTO(this VDElasticVideoDetailModel vdElasticVideoDetailModel)
        {
            SitemapVideoDetailDTO sitemapVideoDetailDTO = new SitemapVideoDetailDTO();

            try
            {
                sitemapVideoDetailDTO.SlugKey = vdElasticVideoDetailModel.SlugKey;
                sitemapVideoDetailDTO.Description = vdElasticVideoDetailModel.Description;
                sitemapVideoDetailDTO.Duration = vdElasticVideoDetailModel.Duration;
                sitemapVideoDetailDTO.ThumbnailUrl = vdElasticVideoDetailModel.ThumbnailUrl;
                sitemapVideoDetailDTO.Title = vdElasticVideoDetailModel.Title;
                sitemapVideoDetailDTO.VideoId = vdElasticVideoDetailModel.VideoId;
                sitemapVideoDetailDTO.VideoFile = vdElasticVideoDetailModel.VideoFiles[0].AsVideoFileDTO();
            }
            catch
            {
                return null;
            }

            return sitemapVideoDetailDTO;
        }

        public static List<SitemapVideoDetailDTO> AsSitemapVideoDetailDTO(this List<VDElasticVideoDetailModel> vdElasticVideoDetailModels)
        {
            List<SitemapVideoDetailDTO> sitemapVideoDetailDTOs = new List<SitemapVideoDetailDTO>();

            try
            {
                foreach (var item in vdElasticVideoDetailModels)
                {
                    var detail = item.AsSitemapVideoDetailDTO();
                    if (detail != null)
                        sitemapVideoDetailDTOs.Add(detail);
                }
            }
            catch
            {
                return null;
            }

            return sitemapVideoDetailDTOs;
        }

        public static VideoFileDTO AsVideoFileDTO(this VDElasticVideoFileModel vdElasticVideoFileModel)
        {
            VideoFileDTO videoFileDTO = new VideoFileDTO();

            try
            {
                videoFileDTO.Fps = vdElasticVideoFileModel.Fps;
                videoFileDTO.Height = vdElasticVideoFileModel.Height;
                videoFileDTO.Width = vdElasticVideoFileModel.Width;
                videoFileDTO.Quality = vdElasticVideoFileModel.Quality;
                videoFileDTO.Size = vdElasticVideoFileModel.Size;
                videoFileDTO.Url = vdElasticVideoFileModel.Url;
                videoFileDTO.UrlSecure = vdElasticVideoFileModel.UrlSecure;
                videoFileDTO.VideoMD5 = vdElasticVideoFileModel.VideoMD5;
            }
            catch
            {
                return null;
            }

            return videoFileDTO;
        }

        public static List<VideoFileDTO> AsVideoFileDTOList(this List<VDElasticVideoFileModel> vdElasticVideoFileModels)
        {
            List<VideoFileDTO> videoFileDTOs = new List<VideoFileDTO>();

            try
            {
                foreach (var item in vdElasticVideoFileModels)
                {
                    videoFileDTOs.Add(item.AsVideoFileDTO());
                }
            }
            catch
            {
                return null;
            }

            return videoFileDTOs;
        }

        public static VDElasticVideoFileModel AsVDElasticVideoFileModel(this VideoFileDTO videoFileDTO)
        {
            VDElasticVideoFileModel vdElasticVideoFileModel = new VDElasticVideoFileModel();

            try
            {
                vdElasticVideoFileModel.Fps = videoFileDTO.Fps;
                vdElasticVideoFileModel.Height = videoFileDTO.Height;
                vdElasticVideoFileModel.Quality = videoFileDTO.Quality;
                vdElasticVideoFileModel.Size = videoFileDTO.Size;
                vdElasticVideoFileModel.Url = videoFileDTO.Url;
                vdElasticVideoFileModel.UrlSecure = videoFileDTO.UrlSecure;
                vdElasticVideoFileModel.VideoMD5 = videoFileDTO.VideoMD5;
                vdElasticVideoFileModel.Width = videoFileDTO.Width;
            }
            catch
            {
                return null;
            }

            return vdElasticVideoFileModel;
        }

        public static List<VDElasticVideoFileModel> AsVDElasticVideoFileModelList(this List<VideoFileDTO> videoFileDTOs)
        {
            List<VDElasticVideoFileModel> vdElasticVideoFileModels = new List<VDElasticVideoFileModel>();

            try
            {
                foreach (var item in videoFileDTOs)
                {
                    vdElasticVideoFileModels.Add(item.AsVDElasticVideoFileModel());
                }
            }
            catch
            {
                return null;
            }

            return vdElasticVideoFileModels;
        }
    }
}
