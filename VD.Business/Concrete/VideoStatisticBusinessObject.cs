﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class VideoStatisticBusinessObject : VDBusinessObjectBase<VideoStatisticEntity>
    {
        public VideoStatisticBusinessObject()
        {

        }

        public VideoStatisticBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public void IncreaseViewCount(int videoId)
        {
            var videoStatistics = (from vs in Get()
                                   where vs.VideoId == videoId
                                   select vs).FirstOrDefault();

            if (videoStatistics != null)
            {
                videoStatistics.ViewCount += 1;

                Update(videoStatistics);
                CommitAsync();
            }
            else
            {
                var video = (from v in new VideoBusinessObject(UnitOfWork).Get()
                             where v.Id == videoId
                             select v).FirstOrDefault();

                VideoStatisticEntity videoStatisticEntity = new VideoStatisticEntity();
                videoStatisticEntity.VideoId = videoId;
                videoStatisticEntity.VideoMD5 = video.VideoMD5;
                videoStatisticEntity.ViewCount = 1;

                Insert(videoStatisticEntity);
                CommitAsync();
            }
        }
    }
}
