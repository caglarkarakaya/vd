﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.DTO.Concrete;
using VD.Entity.Concrete;
using VD.Core.DataAccess.UnitOfWork;
using VD.Elastic.Models;
using VD.Elastic.Client;
using Nest;
using System.Threading;
using VD.Elastic.Extentions;
using Elasticsearch.Net;

namespace VD.Business.Concrete
{
    public class PartnerBusinessObject : VDBusinessObjectBase<PartnerEntity>
    {
        public PartnerBusinessObject()
        {

        }

        public PartnerBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        internal IQueryable<ChannelInfoDTO> GetChannelsInfoQueryable()
        {
            var collect = from v in new VideoBusinessObject(UnitOfWork).Get()
                          join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                          join p in Get() on up.PartnerId equals p.Id
                          join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                          select new
                          {
                              PartnerId = p.Id,
                              PartnerName = p.Name,
                              BackgroundImageUrl = p.BackgroundImageUrl,
                              AvatarUrl = p.AvatarUrl,
                              CardImageUrl = p.CardImageUrl,
                              Description = p.Description,
                              ViewCount = vs.ViewCount
                          };

            var group = from c in collect
                        group c by new { c.PartnerId, c.PartnerName } into s
                        select new
                        {
                            Id = s.Key.PartnerId,
                            Name = s.Key.PartnerName,
                            VideoCount = s.Count(),
                            ViewCount = s.Sum(x => x.ViewCount)
                        };

            var channelInfo = from g in @group
                              join p in Get() on g.Id equals p.Id
                              select new ChannelInfoDTO()
                              {
                                  Id = p.Id,
                                  AvatarUrl = p.AvatarUrl,
                                  BackgroundImageUrl = p.BackgroundImageUrl,
                                  CardImageUrl = p.CardImageUrl,
                                  Name = p.Name,
                                  Description = p.Description,
                                  VideoCount = g.VideoCount,
                                  ViewCount = g.ViewCount
                              };

            return channelInfo;
        }

        public ChannelInfoDTO GetChannelInfoFromElasticByChannelId(int channelId)
        {
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            var searchResponse = _client.Search<VDElasticChannelModel>(s => s
                                                                            .Query(q => q
                                                                                .Ids(i => i
                                                                                    .Values(new Id(channelId)))));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsChannelInfoDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<ChannelInfoDTO> GetChannelInfoFromElasticByChannelIdAsync(int channelId)
        {
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticChannelModel>(s => s
                                                                            .Query(q => q
                                                                                .Ids(i => i
                                                                                    .Values(new Id(channelId)))));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsChannelInfoDTO();
            }
            else
            {
                return null;
            }
        }

        public ChannelInfoDTO GetChannelInfo(int channelId)
        {
            return GetChannelInfoFromElasticByChannelId(channelId);
        }

        public async Task<ChannelInfoDTO> GetChannelInfoAsync(int channelId)
        {
            return await GetChannelInfoFromElasticByChannelIdAsync(channelId);
        }

        public ChannelsDTO GetChannelInfoFromElastic(int? skip = 0, int? top = 10)
        {
            ChannelsDTO channelsDTO = new ChannelsDTO();
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            var searchResponse = _client.Search<VDElasticChannelModel>(s => s
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)
                                                                                .Field(f => f.Name)
                                                                                .Field(f => f.VideoCount)
                                                                                .Field(f => f.ViewCount)
                                                                                .Field(f => f.CardImageUrl)
                                                                                .Field(f => f.BackgroundImageUrl)
                                                                                .Field(f => f.AvatarUrl)
                                                                                .Field(f => f.Description)
                                                                            ))
                                                                        .Skip(skip.Value)
                                                                        .Take(top.Value)
                                                                        .Query(q => q.MatchAll())
                                                                        .Sort(so => so.Descending(d => d.ViewCount)));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                channelsDTO.TotalCount = searchResponse.Total;
                channelsDTO.ChannelInfos = result.AsChannelInfoDTOList();
                return channelsDTO;
            }
            else
            {
                return null;
            }
        }

        public async Task<ChannelsDTO> GetChannelInfoFromElasticAsync(int? skip = 0, int? top = 10)
        {
            ChannelsDTO channelsDTO = new ChannelsDTO();
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticChannelModel>(s => s
                                                                                    .Source(so => so
                                                                                        .Includes(i => i
                                                                                            .Field(f => f.Id)
                                                                                            .Field(f => f.Name)
                                                                                            .Field(f => f.VideoCount)
                                                                                            .Field(f => f.ViewCount)
                                                                                            .Field(f => f.CardImageUrl)
                                                                                            .Field(f => f.BackgroundImageUrl)
                                                                                            .Field(f => f.AvatarUrl)
                                                                                            .Field(f => f.Description)
                                                                                        ))
                                                                                    .Skip(skip.Value)
                                                                                    .Take(top.Value)
                                                                                    .Query(q => q.MatchAll())
                                                                                    .Sort(so => so.Descending(d => d.ViewCount)));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                channelsDTO.TotalCount = searchResponse.Total;
                channelsDTO.ChannelInfos = result.AsChannelInfoDTOList();
                return channelsDTO;
            }
            else
            {
                return null;
            }
        }

        public ChannelsDTO GetChannelInfos(int? skip = 0, int? top = 10)
        {
            return GetChannelInfoFromElastic(skip, top);
        }

        public async Task<ChannelsDTO> GetChannelInfosAsync(int? skip = 0, int? top = 10)
        {
            return await GetChannelInfoFromElasticAsync(skip, top);
        }

        public List<ChannelInfoDTO> GetAllChannelIds()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            ISearchResponse<VDElasticChannelModel> initialResponse = _client.Search<VDElasticChannelModel>(scr => scr
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)))
                                                                        .Size(5000)
                                                                        .Scroll(scrollTimeout)
                                                                    );

            List<VDElasticChannelModel> results = new List<VDElasticChannelModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticChannelModel> loopingResponse = _client.Scroll<VDElasticChannelModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            _client.ClearScroll(new ClearScrollRequest(scrollid));
            
            if (results != null)
            {
                return results.AsChannelInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<ChannelInfoDTO>> GetAllChannelIdsAsync()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            ISearchResponse<VDElasticChannelModel> initialResponse = await _client.SearchAsync<VDElasticChannelModel>(scr => scr
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)))
                                                                        .Size(5000)
                                                                        .Scroll(scrollTimeout)
                                                                    );

            List<VDElasticChannelModel> results = new List<VDElasticChannelModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticChannelModel> loopingResponse = await _client.ScrollAsync<VDElasticChannelModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            await _client.ClearScrollAsync(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsChannelInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        //TO BE DELETED
        [Obsolete]
        public List<ChannelInfoDTO> GetChannelInfos2()
        {
            return GetChannelsInfoQueryable().OrderByDescending(x => x.VideoCount).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<List<ChannelInfoDTO>> GetChannelInfosAsync2()
        {
            return await GetChannelsInfoQueryable().OrderByDescending(x => x.VideoCount).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public ChannelInfoDTO GetChannelInfo2(int channelId)
        {
            return GetChannelsInfoQueryable().Where(x => x.Id == channelId).OrderByDescending(x => x.VideoCount).FirstOrDefault();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<ChannelInfoDTO> GetChannelInfoAsync2(int channelId)
        {
            return await GetChannelsInfoQueryable().Where(x => x.Id == channelId).OrderByDescending(x => x.VideoCount).FirstOrDefaultAsync();
        }

        #region ElasticSynchronization
        internal IQueryable<VDElasticChannelModel> GetChannelsForElasticQueryable()
        {
            var collect = from v in new VideoBusinessObject(UnitOfWork).Get()
                          join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                          join p in Get() on up.PartnerId equals p.Id
                          join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                          where
                          (
                            v.PrivacyId == 1 &&
                            v.ApproveStatusId == 2
                          )
                          select new
                          {
                              PartnerId = p.Id,
                              PartnerName = p.Name,
                              vs.ViewCount
                          };

            var group = from c in collect
                        group c by new { c.PartnerId, c.PartnerName } into s
                        select new
                        {
                            Id = s.Key.PartnerId,
                            Name = s.Key.PartnerName,
                            VideoCount = s.Count(),
                            ViewCount = s.Sum(x => x.ViewCount)
                        };

            var channelInfo = from g in @group
                              join p in Get() on g.Id equals p.Id
                              select new VDElasticChannelModel()
                              {
                                  Id = p.Id,
                                  AvatarUrl = p.AvatarUrl,
                                  BackgroundImageUrl = p.BackgroundImageUrl,
                                  CardImageUrl = p.CardImageUrl,
                                  Name = p.Name,
                                  Description = p.Description,
                                  VideoCount = g.VideoCount,
                                  ViewCount = g.ViewCount,
                                  CreatedAt = p.CreatedAt,
                                  UpdatedAt = p.UpdatedAt
                              };

            return channelInfo;
        }

        private List<int> GetChannelIdsForElasticSynchronization(DateTime elasticSyncDate)
        {
            var channelIds = from v in new VideoBusinessObject(UnitOfWork).Get()
                             join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                             join p in Get() on up.PartnerId equals p.Id
                             join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                             where
                             (
                               ((v.CreatedAt > elasticSyncDate || v.UpdatedAt > elasticSyncDate) ||
                               (p.CreatedAt > elasticSyncDate || p.UpdatedAt > elasticSyncDate) ||
                               (vs.CreatedAt > elasticSyncDate || vs.UpdatedAt > elasticSyncDate)) &&
                               v.PrivacyId == 1 &&
                               v.ApproveStatusId == 2
                             )
                             group p by p.Id into g
                             select g.Key;

            return channelIds.ToList();
        }

        private List<int> GetDeletedChannelIdsForElasticSynchronization()
        {
            var channelIds = from p in Get(null, true)
                             where p.IsActive == false
                             select p.Id;

            return channelIds.ToList();
        }

        public void SynchronizeElasticChannelDetails()
        {
            DateTime updatedAt = DateTime.Now;

            var elasticSearchSyncBusinessObject = new ElasticSearchSyncBusinessObject(UnitOfWork);
            CategoryBusinessObject categoryBusinessObject = new CategoryBusinessObject(UnitOfWork);
            var elasticSearchSyncEntity = elasticSearchSyncBusinessObject.Get(x => x.Type == "channel").FirstOrDefault();
            var channels = GetChannelIdsForElasticSynchronization(elasticSearchSyncEntity.UpdatedAt ?? DateTime.MinValue);

            List<VDElasticChannelModel> vdElasticChannelModels = GetChannelsForElasticQueryable().Where(x => channels.Contains(x.Id)).ToList();

            foreach (var channel in vdElasticChannelModels)
            {
                channel.CategoryInfos = categoryBusinessObject.GetCategoriesOfChannelForElastic(channel.Id).ToList();
            }

            var _client = new VDElasticClient<VDElasticChannelModel>().Client;
            var bulkAll = _client.BulkAll(vdElasticChannelModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));

            waitHandle.Wait();

            var deletedChannels = GetDeletedChannelIdsForElasticSynchronization();

            var descriptor = new BulkDescriptor();

            foreach (var id in deletedChannels)
                descriptor.Delete<VDElasticChannelModel>(x => x
                                                            .Id(id))
                                                            .Refresh(Refresh.WaitFor);

            var response = _client.Bulk(descriptor);

            //var bulkResponse = _client.DeleteMany<VDElasticChannelModel>(deletedChannels.Select(x => new VDElasticChannelModel() { Id = x }));

            elasticSearchSyncEntity.LastExecutionDateTime = updatedAt;
            elasticSearchSyncEntity.Description = channels.Count.ToString() + "records";
            elasticSearchSyncBusinessObject.Update(elasticSearchSyncEntity, updatedAt);
            elasticSearchSyncBusinessObject.Commit();
        }
        #endregion
    }
}
