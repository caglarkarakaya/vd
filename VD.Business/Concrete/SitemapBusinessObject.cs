﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using VD.Common.Cloud.Google;
using VD.DTO.Concrete;
using static VD.Common.Enums.SitemapEnums;
using System.IO;
using System.Configuration;

namespace VD.Business.Concrete
{
    public class SitemapBusinessObject
    {
        readonly string domain = "https://videodistribution.com/";
        readonly string webUrl = "https://videodistribution.com/#/";
        readonly string sitemapBucketFolder = "sitemap/";
        readonly string lastModifyDate = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
        List<string> sitemapUrls = new List<string>();
        GoogleCloudStorage googleCloudStorage;


        public SitemapBusinessObject()
        {
            googleCloudStorage = new GoogleCloudStorage(ConfigurationManager.AppSettings["GCPCertificateFilePath"], ConfigurationManager.AppSettings["GCPAccountEmail"]);
        }

        public void CreateSitemap()
        {
            CreateSitemapForStaticPages();
            CreateSitemapForCategories();
            CreateSitemapForChannels();
            CreateSitemapForPlaylists();
            CreateSitemapForVideos();
            CreateSitemapIndex();
        }

        public async Task CreateSitemapAsync()
        {
            await CreateSitemapForStaticPagesAsync();
            await CreateSitemapForCategoriesAsync();
            await CreateSitemapForChannelsAsync();
            await CreateSitemapForPlaylistsAsync();
            await CreateSitemapForVideosAsync();
            await CreateSitemapIndexAsync();
        }

        internal void CreateSitemapForStaticPages()
        {
            string initialFileName = sitemapBucketFolder + "static-sitemap-new.xml";
            string finalFileName = sitemapBucketFolder + "static-sitemap.xml";

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            //XDocument Start
            XDocument staticPagesDocument = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(ns + "urlset",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                    new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "channels"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "playlists"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "categories"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "privacy-policy"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.low.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "terms-of-use"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.monthly),
                        new XElement(ns + "priority", Priority.low.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "contact-us"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.monthly),
                        new XElement(ns + "priority", Priority.low.Value())
                    )
                )
            );

            //staticPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + fileName);
            sitemapUrls.Add(domain + finalFileName);

            using (MemoryStream ms = new MemoryStream())
            {
                staticPagesDocument.Save(ms);
                googleCloudStorage.UploadFile(ms, ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, "text/xml");
                googleCloudStorage.RenameFile(ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, finalFileName);
            }
        }

        internal async Task CreateSitemapForStaticPagesAsync()
        {
            string initialFileName = sitemapBucketFolder + "static-sitemap-new.xml";
            string finalFileName = sitemapBucketFolder + "static-sitemap.xml";

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            //XDocument Start
            XDocument staticPagesDocument = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(ns + "urlset",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                    new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "channels"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "playlists"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "categories"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.top.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "privacy-policy"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.daily),
                        new XElement(ns + "priority", Priority.low.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "terms-of-use"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.monthly),
                        new XElement(ns + "priority", Priority.low.Value())
                    ),
                    new XElement(ns + "url",
                        //Root Element
                        new XElement(ns + "loc", webUrl + "contact-us"),
                        new XElement(ns + "lastmod", lastModifyDate),
                        new XElement(ns + "changefreq", ChangeFrequency.monthly),
                        new XElement(ns + "priority", Priority.low.Value())
                    )
                )
            );

            //staticPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + fileName);
            sitemapUrls.Add(domain + finalFileName);

            using (MemoryStream ms = new MemoryStream())
            {
                staticPagesDocument.Save(ms);
                await googleCloudStorage.UploadFileAsync(ms, ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, "text/xml");
                await googleCloudStorage.RenameFileAsync(ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, finalFileName);
            }
        }

        internal void CreateSitemapForChannels()
        {
            string initialFileName = sitemapBucketFolder + "channels-sitemap-new";
            string finalFileName = sitemapBucketFolder + "channels-sitemap";

            PartnerBusinessObject partnerBusinessObject = new PartnerBusinessObject();
            List<ChannelInfoDTO> channelInfoDTOs = partnerBusinessObject.GetAllChannelIds();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            do
            {
                pageIndex++;
                //XDocument Start
                XDocument channelPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                        new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                        from c in channelInfoDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "channel/" + c.Id),
                            new XElement(ns + "lastmod", lastModifyDate),
                            new XElement(ns + "changefreq", ChangeFrequency.daily),
                            new XElement(ns + "priority", Priority.top.Value())
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //channelPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    channelPagesDocument.Save(ms);
                    googleCloudStorage.UploadFile(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    googleCloudStorage.RenameFile(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (channelInfoDTOs.Count > (pageCount * pageIndex));
        }

        internal async Task CreateSitemapForChannelsAsync()
        {
            string initialFileName = sitemapBucketFolder + "channels-sitemap-new";
            string finalFileName = sitemapBucketFolder + "channels-sitemap";

            PartnerBusinessObject partnerBusinessObject = new PartnerBusinessObject();
            List<ChannelInfoDTO> channelInfoDTOs = await partnerBusinessObject.GetAllChannelIdsAsync();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            do
            {
                pageIndex++;
                //XDocument Start
                XDocument channelPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                        new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                        from c in channelInfoDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "channel/" + c.Id),
                            new XElement(ns + "lastmod", lastModifyDate),
                            new XElement(ns + "changefreq", ChangeFrequency.daily),
                            new XElement(ns + "priority", Priority.top.Value())
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //channelPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    channelPagesDocument.Save(ms);
                    await googleCloudStorage.UploadFileAsync(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    await googleCloudStorage.RenameFileAsync(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (channelInfoDTOs.Count > (pageCount * pageIndex));
        }

        internal void CreateSitemapForCategories()
        {
            string initialFileName = sitemapBucketFolder + "category-sitemap-new";
            string finalFileName = sitemapBucketFolder + "category-sitemap";

            CategoryBusinessObject categoryBusinessObject = new CategoryBusinessObject();
            List<CategoryDTO> categoryDTOs = categoryBusinessObject.GetAllCategoryIds();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            do
            {
                pageIndex++;
                //XDocument Start
                XDocument categoryPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                        new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                        from c in categoryDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "category/" + c.Id),
                            new XElement(ns + "lastmod", lastModifyDate),
                            new XElement(ns + "changefreq", ChangeFrequency.daily),
                            new XElement(ns + "priority", Priority.top.Value())
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //categoryPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    categoryPagesDocument.Save(ms);
                    googleCloudStorage.UploadFile(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    googleCloudStorage.RenameFile(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (categoryDTOs.Count > (pageCount * pageIndex));
        }

        internal async Task CreateSitemapForCategoriesAsync()
        {
            string initialFileName = sitemapBucketFolder + "category-sitemap-new";
            string finalFileName = sitemapBucketFolder + "category-sitemap";

            CategoryBusinessObject categoryBusinessObject = new CategoryBusinessObject();
            List<CategoryDTO> categoryDTOs = await categoryBusinessObject.GetAllCategoryIdsAsync();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            do
            {
                pageIndex++;
                //XDocument Start
                XDocument categoryPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                        new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                        from c in categoryDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "category/" + c.Id),
                            new XElement(ns + "lastmod", lastModifyDate),
                            new XElement(ns + "changefreq", ChangeFrequency.daily),
                            new XElement(ns + "priority", Priority.top.Value())
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //categoryPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    categoryPagesDocument.Save(ms);
                    await googleCloudStorage.UploadFileAsync(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    await googleCloudStorage.RenameFileAsync(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (categoryDTOs.Count > (pageCount * pageIndex));
        }

        internal void CreateSitemapForPlaylists()
        {
            string initialFileName = sitemapBucketFolder + "playlist-sitemap-new";
            string finalFileName = sitemapBucketFolder + "playlist-sitemap";

            PlaylistBusinessObject playlistBusinessObject = new PlaylistBusinessObject();
            List<PlaylistInfoDTO> playlistDTOs = playlistBusinessObject.GetAllPublicPlaylistIds();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            do
            {
                pageIndex++;
                //XDocument Start
                XDocument playlistPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                        new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                        from c in playlistDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "playlist-play/" + c.Id),
                            new XElement(ns + "lastmod", lastModifyDate),
                            new XElement(ns + "changefreq", ChangeFrequency.daily),
                            new XElement(ns + "priority", Priority.top.Value())
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //playlistPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    playlistPagesDocument.Save(ms);
                    googleCloudStorage.UploadFile(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    googleCloudStorage.RenameFile(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (playlistDTOs.Count > (pageCount * pageIndex));
        }

        internal async Task CreateSitemapForPlaylistsAsync()
        {
            string initialFileName = sitemapBucketFolder + "playlist-sitemap-new";
            string finalFileName = sitemapBucketFolder + "playlist-sitemap";

            PlaylistBusinessObject playlistBusinessObject = new PlaylistBusinessObject();
            List<PlaylistInfoDTO> playlistDTOs = await playlistBusinessObject.GetAllPublicPlaylistIdsAsync();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsiNs = "http://www.w3.org/2001/XMLSchema-instance";
            do
            {
                pageIndex++;
                //XDocument Start
                XDocument playlistPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "xsi", xsiNs),
                        new XAttribute(xsiNs + "xhtml", "http://www.w3.org/1999/xhtml"),
                        from c in playlistDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "playlist-play/" + c.Id),
                            new XElement(ns + "lastmod", lastModifyDate),
                            new XElement(ns + "changefreq", ChangeFrequency.daily),
                            new XElement(ns + "priority", Priority.top.Value())
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //playlistPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    playlistPagesDocument.Save(ms);
                    await googleCloudStorage.UploadFileAsync(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    await googleCloudStorage.RenameFileAsync(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (playlistDTOs.Count > (pageCount * pageIndex));
        }

        public void CreateSitemapForVideos()
        {
            string initialFileName = sitemapBucketFolder + "videos-sitemap-new";
            string finalFileName = sitemapBucketFolder + "videos-sitemap";

            VideoBusinessObject videoBusinessObject = new VideoBusinessObject();
            List<SitemapVideoDetailDTO> videoDTOs = videoBusinessObject.GetAllVideos();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace videoNameSpace = "http://www.google.com/schemas/sitemap-video/1.1";
            do
            {
                pageIndex++;
                //XDocument Start

                XDocument videoPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "video", videoNameSpace.NamespaceName),
                        from v in videoDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "video-play/" + v.SlugKey),
                            new XElement(videoNameSpace + "video",
                                new XElement(videoNameSpace + "thumbnail_loc", "https:" + v.ThumbnailUrl),
                                new XElement(videoNameSpace + "title", v.Title),
                                new XElement(videoNameSpace + "description", v.Description),
                                new XElement(videoNameSpace + "content_loc", v.VideoFile.UrlSecure)
                            )
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //videoPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    videoPagesDocument.Save(ms);
                    googleCloudStorage.UploadFile(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    googleCloudStorage.RenameFile(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (videoDTOs.Count > (pageCount * pageIndex));
        }

        internal async Task CreateSitemapForVideosAsync()
        {
            string initialFileName = sitemapBucketFolder + "videos-sitemap-new";
            string finalFileName = sitemapBucketFolder + "videos-sitemap";

            VideoBusinessObject videoBusinessObject = new VideoBusinessObject();
            List<SitemapVideoDetailDTO> videoDTOs = await videoBusinessObject.GetAllVideosAsync();
            int pageCount = 5000;
            int pageIndex = 0;

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace videoNameSpace = "http://www.google.com/schemas/sitemap-video/1.1";
            do
            {
                pageIndex++;
                //XDocument Start

                XDocument videoPagesDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "no"),
                    new XElement(ns + "urlset",
                        new XAttribute(XNamespace.Xmlns + "video", videoNameSpace.NamespaceName),
                        from v in videoDTOs.Skip((pageIndex - 1) * pageCount).Take(5000)
                        select new XElement(ns + "url",
                            //Root Element
                            new XElement(ns + "loc", webUrl + "video-play/" + v.SlugKey),
                            new XElement(videoNameSpace + "video",
                                new XElement(videoNameSpace + "thumbnail_loc", "https:" + v.ThumbnailUrl),
                                new XElement(videoNameSpace + "title", v.Title),
                                new XElement(videoNameSpace + "description", v.Description),
                                new XElement(videoNameSpace + "content_loc", v.VideoFile.UrlSecure)
                            )
                        )
                    )
                );
                var currentInitialFileName = initialFileName + "-" + pageIndex + ".xml";
                var currentFinalFileName = finalFileName + "-" + pageIndex + ".xml";
                //videoPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + currentFileName);
                sitemapUrls.Add(domain + currentFinalFileName);

                using (MemoryStream ms = new MemoryStream())
                {
                    videoPagesDocument.Save(ms);
                    await googleCloudStorage.UploadFileAsync(ms, ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, "text/xml");
                    await googleCloudStorage.RenameFileAsync(ConfigurationManager.AppSettings["GCPBucketName"], currentInitialFileName, currentFinalFileName);
                }
            } while (videoDTOs.Count > (pageCount * pageIndex));
        }

        internal void CreateSitemapIndex()
        {
            string initialFileName = sitemapBucketFolder + "sitemap-index-new.xml";
            string finalFileName = sitemapBucketFolder + "sitemap-index.xml";

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";

            //XDocument Start
            XDocument videoPagesDocument = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(ns + "sitemapindex",
                    from s in sitemapUrls
                    select new XElement(ns + "sitemap",
                        //Root Element
                        new XElement(ns + "loc", s),
                        new XElement(ns + "lastmod", lastModifyDate)
                    )
                )
            );
            //videoPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + fileName);

            using (MemoryStream ms = new MemoryStream())
            {
                videoPagesDocument.Save(ms);
                googleCloudStorage.UploadFile(ms, ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, "text/xml");
                googleCloudStorage.RenameFile(ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, finalFileName);
            }
        }

        internal async Task CreateSitemapIndexAsync()
        {
            string initialFileName = sitemapBucketFolder + "sitemap-index-new.xml";
            string finalFileName = sitemapBucketFolder + "sitemap-index.xml";

            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";

            //XDocument Start
            XDocument videoPagesDocument = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(ns + "sitemapindex",
                    from s in sitemapUrls
                    select new XElement(ns + "sitemap",
                        //Root Element
                        new XElement(ns + "loc", s),
                        new XElement(ns + "lastmod", lastModifyDate)
                    )
                )
            );
            //videoPagesDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/" + fileName);

            using (MemoryStream ms = new MemoryStream())
            {
                videoPagesDocument.Save(ms);
                await googleCloudStorage.UploadFileAsync(ms, ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, "text/xml");
                await googleCloudStorage.RenameFileAsync(ConfigurationManager.AppSettings["GCPBucketName"], initialFileName, finalFileName);
            }
        }
    }
}
