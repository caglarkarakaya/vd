﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using VD.Common.Constants;
using VD.Common.Enums;
using VD.Common.Extensions;
using VD.Common.Models;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class MailBusinessObject
    {
        public async Task<bool> SendMail(EmailModel emailModel)
        {
            try
            {
                EmailConfigBusinessObject emailConfigBusinessObject = new EmailConfigBusinessObject();
                var configCode = Convert.ToString(emailModel.EmailConfigCode);
                EmailConfigEntity emailConfigEntity = emailConfigBusinessObject.Get(e => e.IsActive && e.Code == configCode).FirstOrDefault();

                if (emailConfigEntity == null)
                    return false;

                var fromAddress = new MailAddress(emailConfigEntity.Email, emailConfigEntity.DisplayName);
                var toAddress = new MailAddress(emailModel.ToEmail, emailModel.ToDisplayName);

                var smtp = new SmtpClient
                {
                    Host = emailConfigEntity.Host,
                    Port = emailConfigEntity.Port,
                    EnableSsl = emailConfigEntity.EnableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, emailConfigEntity.Password)
                };

                #region Prepare Html Body For Mail

                if (emailModel.UseTemplate && string.IsNullOrWhiteSpace(emailModel.MailBody))
                    emailModel.MailBody = ApplyTemplate(emailModel.TemplateParams, emailConfigEntity.Template);
                else
                    emailModel.MailBody = emailModel.IsSignatureNecessary ? GetHtmlBodyWithSignature(emailModel.MailBody, emailConfigEntity.Signature) : emailModel.MailBody;

                #endregion

                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = emailModel.MailSubject,
                    Body = emailModel.MailBody,
                    IsBodyHtml = true,
                    Bcc =
                {
                    new MailAddress(EmailConstants.VidisDefaultBccEmail, EmailConstants.VidisDefaultBccDisplayName),
                    new MailAddress(EmailConstants.VidisDefaultBccEmail2, EmailConstants.VidisDefaultBccDisplayName2)
                }
                })
                {
                    smtp.Send(message);

                    MailLogBusinessObject mailLogBusinessObject = new MailLogBusinessObject();
                    MailLogEntity mailLogEntity = new MailLogEntity()
                    {
                        FromName = emailConfigEntity.DisplayName,
                        FromEmail = emailConfigEntity.Email,
                        ToName = emailModel.ToDisplayName,
                        ToEmail = emailModel.ToEmail,
                        Cc = EmailConstants.VidisDefaultCcEmail,
                        Bcc = EmailConstants.VidisDefaultBccEmail,
                        Subject = emailModel.MailSubject,
                        Body = emailModel.MailBody,
                        SenderIp = emailModel.SenderIp,
                        CreatedAt = DateTime.Now
                    };

                    mailLogBusinessObject.Insert(mailLogEntity);
                    await mailLogBusinessObject.CommitAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> SendContactUsMail(ContactUsMailModel contactUsMailModel)
        {
            var companyName = !string.IsNullOrWhiteSpace(contactUsMailModel.Company) ? contactUsMailModel.Company : contactUsMailModel.Name.FirstCharToUpper() + contactUsMailModel.LastName.FirstCharToUpper();
            var mailBody = @"Hello Admin,<br/><br/>
                                         A new user fill contact form from Vidis home page. Please check it out all information below.<br/><br/><br/>
                                         <b>Name: </b>" + contactUsMailModel.Name + @"<br/>
                                         <b>Lastname: </b>" + contactUsMailModel.LastName + @"<br/>
                                         <b>Email: </b>" + contactUsMailModel.Email + @"<br/>
                                         <b>Phone: </b>" + contactUsMailModel.PhoneNumber + @"<br/>
                                         <b>Website: </b>" + contactUsMailModel.Website + @"<br/>
                                         <b>Company Name: </b>" + companyName + @"<br/>
                                         <b>Description: </b>" + contactUsMailModel.Description + @"<br/>";

            EmailModel emailModel = new EmailModel
            {
                EmailConfigCode = EmailTypeEnum.NOREPLY,
                ToEmail = EmailConstants.VidisAdminMailAddress,
                ToDisplayName = EmailConstants.VidisAdminDisplayName,
                MailSubject = "New contact user",
                MailBody = mailBody,
                IsSignatureNecessary = true,
                UseTemplate = false,
                TemplateParams = null
            };

            return await SendMail(emailModel);
        }

        private static string ApplyTemplate(Dictionary<string, string> parameters, string template)
        {
            foreach (var param in parameters)
            {
                template = template.Replace(param.Key, param.Value);
            }
            return template;
        }

        private static string GetHtmlBodyWithSignature(string htmlBody, string signature)
        {
            return htmlBody + "<br/><hr/><br/>" + signature;
        }
    }
}
