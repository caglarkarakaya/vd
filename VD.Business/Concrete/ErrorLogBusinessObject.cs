﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class ErrorLogBusinessObject : VDBusinessObjectBase<ErrorLogEntity>
    {
        public ErrorLogBusinessObject()
        {

        }

        public ErrorLogBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }
}
