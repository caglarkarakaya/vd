﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.DTO.Concrete;
using VD.Elastic.Client;
using VD.Elastic.Extentions;
using VD.Elastic.Models;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class VideoBusinessObject : VDBusinessObjectBase<VideoEntity>
    {
        public VideoBusinessObject()
        {

        }

        public VideoBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        #region IQueryables
        internal IQueryable<HottestVideoDTO> GetHottestVideosQueryable()
        {
            var hottestVideosQueryable = from v in Get(x => x.CreatedAt > DbFunctions.AddDays(DateTime.Now, -60))
                                         join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                         join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                         select new HottestVideoDTO()
                                         {
                                             CreatedAt = v.CreatedAt,
                                             Duration = v.Duration,
                                             SlugKey = s.Key,
                                             ThumbnailUrl = v.Thumbnail,
                                             Title = v.Title,
                                             VideoId = v.Id,
                                             ViewCount = vs.ViewCount,
                                             VideoMD5 = v.VideoMD5
                                         };
            return hottestVideosQueryable.OrderByDescending(x => x.ViewCount);
        }

        internal IQueryable<TrendingVideoDTO> GetTrendingVideosQueryable()
        {
            var trendingVideosQueryable = from v in Get(x => x.CreatedAt <= DbFunctions.AddDays(DateTime.Now, -60))
                                          join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                          join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                          select new TrendingVideoDTO()
                                          {
                                              CreatedAt = v.CreatedAt,
                                              Duration = v.Duration,
                                              SlugKey = s.Key,
                                              ThumbnailUrl = v.Thumbnail,
                                              Title = v.Title,
                                              VideoId = v.Id,
                                              ViewCount = vs.ViewCount,
                                              VideoMD5 = v.VideoMD5
                                          };
            return trendingVideosQueryable.OrderByDescending(x => x.ViewCount);
        }

        internal IQueryable<VideoInfoDTO> GetVideosByCategoryQueryable(int categoryId)
        {
            var videoInfoQueryable = from v in Get()
                                     join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                     join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                     where v.CategoryId == categoryId
                                     select new VideoInfoDTO()
                                     {
                                         CreatedAt = v.CreatedAt,
                                         Duration = v.Duration,
                                         SlugKey = s.Key,
                                         ThumbnailUrl = v.Thumbnail,
                                         Title = v.Title,
                                         VideoId = v.Id,
                                         ViewCount = vs.ViewCount,
                                         VideoMD5 = v.VideoMD5,
                                         CategoryId = v.CategoryId
                                     };
            return videoInfoQueryable.OrderByDescending(x => x.CreatedAt);
        }

        internal IQueryable<VideoInfoDTO> GetVideosByChannelsCategoriesQueryable(int channelId, int categoryId)
        {
            var videoInfoDTOs = from p in new PartnerBusinessObject(UnitOfWork).Get()
                                join up in new UserPartnerBusinessObject(UnitOfWork).Get() on p.Id equals up.PartnerId
                                join v in Get() on up.Id equals v.UserPartnerId
                                join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                where (p.Id == channelId && v.CategoryId == categoryId)
                                select new VideoInfoDTO()
                                {
                                    CategoryId = v.CategoryId,
                                    ChannelName = p.Name,
                                    CreatedAt = v.CreatedAt,
                                    Duration = v.Duration,
                                    SlugKey = s.Key,
                                    ThumbnailUrl = v.Thumbnail,
                                    Title = v.Title,
                                    VideoId = v.Id,
                                    ViewCount = vs.ViewCount,
                                    VideoMD5 = v.VideoMD5
                                };

            return videoInfoDTOs;
        }

        internal IQueryable<VideoDetailDTO> GetVideoDetailQueryable()
        {
            var videoDetailDTOs = from v in Get()
                                  join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                  join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                                  join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                  join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                  join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                                  select new VideoDetailDTO()
                                  {
                                      CategoryId = v.CategoryId,
                                      CreatedAt = v.CreatedAt,
                                      Duration = v.Duration,
                                      SlugKey = s.Key,
                                      ThumbnailUrl = v.Thumbnail,
                                      Title = v.Title,
                                      VideoId = v.Id,
                                      ViewCount = vs.ViewCount,
                                      VideoMD5 = v.VideoMD5,
                                      ChannelName = p.Name,
                                      Description = v.Description,
                                      CategoryName = c.Title,
                                      ChannelId = p.Id,
                                      ChannelAvatarUrl = p.AvatarUrl
                                  };
            return videoDetailDTOs;
        }

        internal IQueryable<VideoInfoDTO> GetRelatedVideosByPartnerQueryable(string slugKey)
        {
            UserPartnerBusinessObject userPartnerBusinessObject = new UserPartnerBusinessObject(UnitOfWork);
            SlugBusinessObject slugBusinessObject = new SlugBusinessObject(UnitOfWork);

            var videoInfoDTOs = from v in Get()
                                join up in userPartnerBusinessObject.Get() on v.UserPartnerId equals up.Id
                                join up2 in userPartnerBusinessObject.Get() on up.PartnerId equals up2.PartnerId
                                join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                join v2 in Get() on up2.Id equals v2.UserPartnerId
                                join s in slugBusinessObject.Get(x => x.Key == slugKey) on v.Id equals s.VideoId
                                join s2 in slugBusinessObject.Get() on v2.Id equals s2.VideoId
                                join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v2.Id equals vs.VideoId
                                where (v.Id != v2.Id)
                                select new VideoInfoDTO()
                                {
                                    CategoryId = v2.CategoryId,
                                    CreatedAt = v2.CreatedAt,
                                    Duration = v2.Duration,
                                    SlugKey = s2.Key,
                                    ThumbnailUrl = v2.Thumbnail,
                                    Title = v2.Title,
                                    VideoId = v2.Id,
                                    ViewCount = vs.ViewCount,
                                    VideoMD5 = v2.VideoMD5,
                                    ChannelName = p.Name
                                };

            return videoInfoDTOs;
        }

        internal IQueryable<VideoInfoDTO> GetRelatedVideoByCategory()
        {
            var videoInfoDTOs = from v in Get()
                                join v2 in Get() on v.UserPartnerId equals v2.UserPartnerId
                                join s in new SlugBusinessObject(UnitOfWork).Get() on v2.Id equals s.VideoId
                                join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v2.Id equals vs.VideoId
                                where (v.CategoryId == v2.CategoryId && v.Id != v2.Id)
                                select new VideoInfoDTO()
                                {
                                    CategoryId = v.CategoryId,
                                    CreatedAt = v.CreatedAt,
                                    Duration = v.Duration,
                                    SlugKey = s.Key,
                                    ThumbnailUrl = v.Thumbnail,
                                    Title = v.Title,
                                    VideoId = v.Id,
                                    ViewCount = vs.ViewCount,
                                    VideoMD5 = v.VideoMD5
                                };

            return videoInfoDTOs;
        }

        internal IQueryable<VideoInfoDTO> GetVideosByPlaylistQueryable(int playlistId)
        {
            var videoInfos = from p in new PlaylistBusinessObject(UnitOfWork).Get()
                             join pv in new PlaylistVideoBusinessObject(UnitOfWork).Get() on p.Id equals pv.PlaylistId
                             join v in Get() on pv.VideoId equals v.Id
                             join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                             join pa in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals pa.Id
                             join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                             join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                             orderby pv.Order ascending
                             where p.Id == playlistId
                             select new VideoInfoDTO()
                             {
                                 CategoryId = v.CategoryId,
                                 CreatedAt = v.CreatedAt,
                                 Duration = v.Duration,
                                 SlugKey = s.Key,
                                 ThumbnailUrl = v.Thumbnail,
                                 Title = v.Title,
                                 VideoId = v.Id,
                                 ViewCount = vs.ViewCount,
                                 VideoMD5 = v.VideoMD5,
                                 ChannelName = pa.Name
                             };

            return videoInfos;
        }

        internal IQueryable<StunningVideoDTO> GetStunningVideosQueryable()
        {
            var stunningVideosQueryable = from v in Get()
                                          join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                          join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                          select new StunningVideoDTO()
                                          {
                                              CreatedAt = v.CreatedAt,
                                              Duration = v.Duration,
                                              SlugKey = s.Key,
                                              ThumbnailUrl = v.Thumbnail,
                                              Title = v.Title,
                                              VideoId = v.Id,
                                              ViewCount = vs.ViewCount,
                                              VideoMD5 = v.VideoMD5,
                                              CategoryId = v.CategoryId.Value
                                          };

            return stunningVideosQueryable;
        }

        internal IQueryable<VideoInfoDTO> GetVideosByChannelQueryable(int channelId)
        {
            var videoInfoDTOs = from p in new PartnerBusinessObject(UnitOfWork).Get()
                                join up in new UserPartnerBusinessObject(UnitOfWork).Get() on p.Id equals up.PartnerId
                                join v in Get() on up.Id equals v.UserPartnerId
                                join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                where (p.Id == channelId)
                                select new VideoInfoDTO()
                                {
                                    CategoryId = v.CategoryId,
                                    ChannelName = p.Name,
                                    CreatedAt = v.CreatedAt,
                                    Duration = v.Duration,
                                    SlugKey = s.Key,
                                    ThumbnailUrl = v.Thumbnail,
                                    Title = v.Title,
                                    VideoId = v.Id,
                                    ViewCount = vs.ViewCount,
                                    VideoMD5 = v.VideoMD5
                                };

            return videoInfoDTOs;
        }
        #endregion

        #region ElasticSynchronization

        internal IQueryable<VDElasticVideoDetailModel> GetElasticVideoDetailQueryable()
        {
            var videoDetailDTOs = from v in Get()
                                  join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                  join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                                  join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                  join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                  join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                                  where v.ApproveStatusId == 2
                                  select new VDElasticVideoDetailModel()
                                  {
                                      CategoryId = v.CategoryId.Value,
                                      CategoryName = c.Title,
                                      CreatedAt = v.CreatedAt,
                                      Duration = v.Duration,
                                      SlugKey = s.Key,
                                      ThumbnailUrl = v.Thumbnail,
                                      Title = v.Title,
                                      VideoId = v.Id,
                                      ViewCount = vs.ViewCount,
                                      VideoMD5 = v.VideoMD5,
                                      ChannelName = p.Name,
                                      Description = v.Description,
                                      ChannelId = p.Id,
                                      UpdatedAt = v.UpdatedAt,
                                      ChannelAvatarUrl = p.AvatarUrl,
                                      PrivacyId = v.PrivacyId.Value
                                  };
            return videoDetailDTOs;
        }

        internal IQueryable<VDElasticPlaylistVideoModel> GetVideosByPlaylistForElasticQueryable(int playlistId)
        {
            var videoInfos = from p in new PlaylistBusinessObject(UnitOfWork).Get()
                             join pv in new PlaylistVideoBusinessObject(UnitOfWork).Get() on p.Id equals pv.PlaylistId
                             join v in Get() on pv.VideoId equals v.Id
                             join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                             join pa in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals pa.Id
                             join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                             join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                             join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                             orderby pv.Order ascending
                             where
                             (
                                p.Id == playlistId &&
                                v.PrivacyId == 1 &&
                                v.ApproveStatusId == 2
                             )
                             select new VDElasticPlaylistVideoModel()
                             {
                                 CategoryId = v.CategoryId.Value,
                                 CreatedAt = v.CreatedAt,
                                 Duration = v.Duration,
                                 SlugKey = s.Key,
                                 ThumbnailUrl = v.Thumbnail,
                                 Title = v.Title,
                                 VideoId = v.Id,
                                 Order = pv.Order,
                                 ViewCount = vs.ViewCount,
                                 VideoMD5 = v.VideoMD5,
                                 ChannelName = pa.Name,
                                 CategoryName = c.Title,
                                 ChannelId = pa.Id,
                                 Description = v.Description,
                                 UpdatedAt = v.UpdatedAt,
                                 ChannelAvatarUrl = pa.AvatarUrl,
                                 PrivacyId = v.PrivacyId.Value
                             };

            return videoInfos;
        }

        public List<VDElasticVideoDetailModel> GetElasticVideoDetails()
        {
            return GetElasticVideoDetailQueryable().ToList();
        }

        internal IQueryable<VideoPictureDTO> GetVideoCardImages(List<int> videoIds)
        {
            //Gets pictures with smallest resolution greater than 250x150.
            //These pictures will be used in video cards.
            var videoPictures = from vp in new VideoPictureBusinessObject(UnitOfWork).Get()
                                where
                                (
                                    videoIds.Contains(vp.VideoId) &&
                                    vp.Width > 250 &&
                                    vp.Height > 150
                                )
                                group vp by vp.VideoId into g
                                select new
                                {
                                    VideoId = g.Key,
                                    Width = g.Min(p => p.Width),
                                    Height = g.Min(p => p.Height)
                                };

            var pictures = (from vp in videoPictures
                            join p in new VideoPictureBusinessObject(UnitOfWork).Get() on new { vp.VideoId, vp.Width, vp.Height } equals new { p.VideoId, p.Width, p.Height }
                            select new VideoPictureDTO()
                            {
                                VideoId = p.VideoId,
                                Url = p.Url,
                            });

            return pictures;
        }

        internal IQueryable<VideoPictureDTO> GetVideoCardImages(DateTime elasticSyncDate)
        {
            //Gets pictures with smallest resolution greater than 250x150.
            //These pictures will be used in video cards.
            var videoPictures = from v in Get()
                                join vp in new VideoPictureBusinessObject(UnitOfWork).Get() on v.Id equals vp.VideoId
                                where
                                (
                                    (v.CreatedAt > elasticSyncDate || v.UpdatedAt > elasticSyncDate) &&
                                    vp.Width > 250 &&
                                    vp.Height > 150
                                )
                                group vp by vp.VideoId into g
                                select new
                                {
                                    VideoId = g.Key,
                                    Width = g.Min(p => p.Width),
                                    Height = g.Min(p => p.Height)
                                };

            var pictures = (from vp in videoPictures
                            join p in new VideoPictureBusinessObject(UnitOfWork).Get() on new { vp.VideoId, vp.Width, vp.Height } equals new { p.VideoId, p.Width, p.Height }
                            select new VideoPictureDTO()
                            {
                                VideoId = p.VideoId,
                                Url = p.Url,
                            });

            return pictures;
        }

        internal IQueryable<VideoFileDTO> GetVideoFiles(List<int> videoIds)
        {
            var videoFiles = from vf in new VideoFileBusinessObject(UnitOfWork).Get(vf => videoIds.Contains(vf.VideoId))
                             select new VideoFileDTO()
                             {
                                 CreatedAt = vf.CreatedAt,
                                 Fps = vf.Fps,
                                 Height = vf.Height,
                                 Quality = vf.Quality,
                                 Size = vf.Size,
                                 Url = vf.Url,
                                 UrlSecure = vf.UrlSecure,
                                 VideoId = vf.VideoId,
                                 VideoMD5 = vf.VideoMD5,
                                 Width = vf.Width
                             };

            return videoFiles;
        }

        public List<VDElasticVideoDetailModel> GetElasticVideoDetailsToSynchronize(DateTime elasticSyncDate)
        {
            //Includes private videos as well. 
            var videoDetailDTOs = (from v in Get()
                                   join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                   join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                                   join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                   join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                   join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                                   where
                                   (
                                     ((v.CreatedAt > elasticSyncDate || v.UpdatedAt > elasticSyncDate) ||
                                     (p.CreatedAt > elasticSyncDate || p.UpdatedAt > elasticSyncDate) ||
                                     (vs.CreatedAt > elasticSyncDate || vs.UpdatedAt > elasticSyncDate)) &&
                                     v.ApproveStatusId == 2
                                   )
                                   select new VDElasticVideoDetailModel()
                                   {
                                       CategoryId = v.CategoryId.Value,
                                       CategoryName = c.Title,
                                       CreatedAt = v.CreatedAt,
                                       Duration = v.Duration,
                                       SlugKey = s.Key,
                                       ThumbnailUrl = v.Thumbnail,
                                       Title = v.Title,
                                       VideoId = v.Id,
                                       ViewCount = vs.ViewCount,
                                       VideoMD5 = v.VideoMD5,
                                       ChannelName = p.Name,
                                       Description = v.Description,
                                       ChannelId = p.Id,
                                       UpdatedAt = v.UpdatedAt,
                                       ChannelAvatarUrl = p.AvatarUrl,
                                       PrivacyId = v.PrivacyId.Value
                                   }).ToList();

            List<int> videoIds = new List<int>();
            foreach (var item in videoDetailDTOs)
            {
                videoIds.Add(item.VideoId);
            }

            //Performance issue for videopictures.
            var pictures = GetVideoCardImages(videoIds).ToList();

            //When left joined to VideoPictures the query doesnt respond in timely manner.
            foreach (var item in videoDetailDTOs)
            {
                item.VideoCardImageUrl = pictures.Where(x => x.VideoId == item.VideoId).Select(x => x.Url.Replace("http://", "//").Replace("https://", "//")).FirstOrDefault() ?? item.ThumbnailUrl;
            }

            var videos = GetVideoFiles(videoIds).ToList();

            foreach (var videoDetail in videoDetailDTOs)
            {
                videoDetail.VideoFiles = new List<VDElasticVideoFileModel>();
                foreach (var videoFile in videos)
                {
                    if (videoFile.VideoId == videoDetail.VideoId)
                    {
                        videoDetail.VideoFiles.Add(videoFile.AsVDElasticVideoFileModel());
                    }
                }
            }

            return videoDetailDTOs;
        }

        public List<int> GetDeletedVideoIdsForElasticSynchronization(DateTime elasticSyncDate)
        {
            var videoIds = from v in Get(null, true)
                           join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                           join p in new PartnerBusinessObject(UnitOfWork).Get(null, true) on up.PartnerId equals p.Id
                           where
                           (
                             ((v.CreatedAt > elasticSyncDate || v.UpdatedAt > elasticSyncDate) ||
                             (p.CreatedAt > elasticSyncDate || p.UpdatedAt > elasticSyncDate)) &&
                             (v.IsActive == false || p.IsActive == false)
                           )
                           select v.Id;

            return videoIds.ToList();
        }

        public List<VDElasticHottestVideoModel> GetHottestVideosForElastic()
        {
            var hottestVideosQueryable = from v in Get()
                                         join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                         join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                         join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                                         join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                                         join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                         join vp in GetVideoCardImages(DateTime.MinValue) on v.Id equals vp.VideoId
                                         where
                                         (
                                            v.PrivacyId == 1 &&
                                            v.ApproveStatusId == 2
                                         )
                                         select new VDElasticHottestVideoModel()
                                         {
                                             CreatedAt = v.CreatedAt,
                                             Duration = v.Duration,
                                             SlugKey = s.Key,
                                             ThumbnailUrl = v.Thumbnail,
                                             VideoCardImageUrl = vp.Url.Replace("http://", "//").Replace("https://", "//") ?? v.Thumbnail,
                                             Title = v.Title,
                                             VideoId = v.Id,
                                             ViewCount = vs.ViewCount,
                                             VideoMD5 = v.VideoMD5,
                                             CategoryId = c.Id,
                                             CategoryName = c.Title,
                                             ChannelAvatarUrl = p.AvatarUrl,
                                             ChannelId = p.Id,
                                             ChannelName = p.Name,
                                             Description = v.Description,
                                             UpdatedAt = v.UpdatedAt
                                         };
            var hottestVideos = hottestVideosQueryable.OrderByDescending(x => x.ViewCount).Take(100).ToList();
            return hottestVideos;
        }

        public List<VDElasticTrendingVideoModel> GetTrendingVideosForElastic()
        {
            var trendingVideosQueryable = from v in Get()
                                          join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                          join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                          join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                                          join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                                          join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                          join vp in GetVideoCardImages(DateTime.MinValue) on v.Id equals vp.VideoId
                                          where
                                          (
                                            v.PrivacyId == 1 &&
                                            v.ApproveStatusId == 2
                                          )
                                          select new VDElasticTrendingVideoModel()
                                          {
                                              CreatedAt = v.CreatedAt,
                                              Duration = v.Duration,
                                              SlugKey = s.Key,
                                              ThumbnailUrl = v.Thumbnail,
                                              VideoCardImageUrl = vp.Url.Replace("http://", "//").Replace("https://", "//") ?? v.Thumbnail,
                                              Title = v.Title,
                                              VideoId = v.Id,
                                              ViewCount = vs.ViewCount,
                                              VideoMD5 = v.VideoMD5,
                                              CategoryId = c.Id,
                                              CategoryName = c.Title,
                                              ChannelAvatarUrl = p.AvatarUrl,
                                              ChannelId = p.Id,
                                              ChannelName = p.Name,
                                              Description = v.Description,
                                              UpdatedAt = v.UpdatedAt
                                          };
            var trendingVideos = trendingVideosQueryable.OrderByDescending(x => x.ViewCount).Skip(100).Take(100).ToList();
            return trendingVideos;
        }

        public List<VDElasticRecommendedVideoModel> GetRecommendedVideosForElastic()
        {
            var recommendedVideosQueryable = from v in Get()
                                             join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                                             join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                                             join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                                             join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                                             join p in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals p.Id
                                             join vp in GetVideoCardImages(DateTime.MinValue) on v.Id equals vp.VideoId
                                             where
                                             (
                                               v.PrivacyId == 1 &&
                                               v.ApproveStatusId == 2
                                             )
                                             select new VDElasticRecommendedVideoModel()
                                             {
                                                 CreatedAt = v.CreatedAt,
                                                 Duration = v.Duration,
                                                 SlugKey = s.Key,
                                                 ThumbnailUrl = v.Thumbnail,
                                                 VideoCardImageUrl = vp.Url.Replace("http://", "//").Replace("https://", "//") ?? v.Thumbnail,
                                                 Title = v.Title,
                                                 VideoId = v.Id,
                                                 ViewCount = vs.ViewCount,
                                                 VideoMD5 = v.VideoMD5,
                                                 CategoryId = c.Id,
                                                 CategoryName = c.Title,
                                                 ChannelAvatarUrl = p.AvatarUrl,
                                                 ChannelId = p.Id,
                                                 ChannelName = p.Name,
                                                 Description = v.Description,
                                                 UpdatedAt = v.UpdatedAt
                                             };
            var recommendedVideos = recommendedVideosQueryable.OrderByDescending(x => x.ViewCount).Skip(200).Take(100).ToList();
            return recommendedVideos;
        }

        public void SynchronizeElasticHottestVideoDetails()
        {
            var vdElasticVideoDetailModels = GetHottestVideosForElastic();
            var elasticClient = new VDElasticClient<VDElasticHottestVideoModel>().Client;

            var bulkAll = elasticClient.BulkAll(vdElasticVideoDetailModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));
            waitHandle.Wait();

            var searchResponse = elasticClient.Search<VDElasticHottestVideoModel>(s => s
                                                                                    .Source(so => so
                                                                                        .Includes(i => i
                                                                                            .Field(f => f.VideoId)))
                                                                                    .Take(200));

            var existingIds = searchResponse.Hits.Select(x => x.Source.VideoId).ToList();
            var insertedIds = vdElasticVideoDetailModels.Select(x => x.VideoId).ToList();

            var toBeDeleted = existingIds.Where(x => !insertedIds.Contains(x)).ToList();
            //var ids = searchResponse.Hits.Select(x => vdElasticVideoDetailModels.Where(v => v.VideoId != x.Source.VideoId).Select(y => y.VideoId)).ToList();
            if (toBeDeleted.Count > 0)
            {
                var bulkResponse = elasticClient.DeleteMany<VDElasticHottestVideoModel>(toBeDeleted.Select(x => new VDElasticHottestVideoModel { VideoId = x }));
            }
        }

        public void SynchronizeElasticTrendingVideoDetails()
        {
            var vdElasticVideoDetailModels = GetTrendingVideosForElastic();
            var elasticClient = new VDElasticClient<VDElasticTrendingVideoModel>().Client;

            var bulkAll = elasticClient.BulkAll(vdElasticVideoDetailModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));
            waitHandle.Wait();

            var searchResponse = elasticClient.Search<VDElasticTrendingVideoModel>(s => s
                                                                                    .Source(so => so
                                                                                        .Includes(i => i
                                                                                            .Field(f => f.VideoId)))
                                                                                    .Take(200));

            var existingIds = searchResponse.Hits.Select(x => x.Source.VideoId).ToList();
            var insertedIds = vdElasticVideoDetailModels.Select(x => x.VideoId).ToList();

            var toBeDeleted = existingIds.Where(x => !insertedIds.Contains(x)).ToList();
            //var ids = searchResponse.Hits.Select(x => vdElasticVideoDetailModels.Where(v => v.VideoId != x.Source.VideoId).Select(y => y.VideoId)).ToList();
            if (toBeDeleted.Count > 0)
            {
                var bulkResponse = elasticClient.DeleteMany<VDElasticTrendingVideoModel>(toBeDeleted.Select(x => new VDElasticTrendingVideoModel { VideoId = x }));
            }
        }

        public void SynchronizeElasticRecommendedVideoDetails()
        {
            var vdElasticVideoDetailModels = GetRecommendedVideosForElastic();
            var elasticClient = new VDElasticClient<VDElasticRecommendedVideoModel>().Client;

            var bulkAll = elasticClient.BulkAll(vdElasticVideoDetailModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));
            waitHandle.Wait();

            var searchResponse = elasticClient.Search<VDElasticRecommendedVideoModel>(s => s
                                                                                    .Source(so => so
                                                                                        .Includes(i => i
                                                                                            .Field(f => f.VideoId)))
                                                                                    .Take(200));

            var existingIds = searchResponse.Hits.Select(x => x.Source.VideoId).ToList();
            var insertedIds = vdElasticVideoDetailModels.Select(x => x.VideoId).ToList();

            var toBeDeleted = existingIds.Where(x => !insertedIds.Contains(x)).ToList();
            //var ids = searchResponse.Hits.Select(x => vdElasticVideoDetailModels.Where(v => v.VideoId != x.Source.VideoId).Select(y => y.VideoId)).ToList();
            if (toBeDeleted.Count > 0)
            {
                var bulkResponse = elasticClient.DeleteMany(toBeDeleted.Select(x => new VDElasticRecommendedVideoModel { VideoId = x }));
            }
        }

        public void SynchronizeElasticVideoDetails()
        {
            DateTime updatedAt = DateTime.Now;

            var elasticSearchSyncBusinessObject = new ElasticSearchSyncBusinessObject(UnitOfWork);
            var elasticSearchSyncEntity = elasticSearchSyncBusinessObject.Get(x => x.Type == "video").FirstOrDefault();

            var vdElasticVideoDetailModels = GetElasticVideoDetailsToSynchronize(elasticSearchSyncEntity.UpdatedAt ?? DateTime.MinValue);
            var elasticClient = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var bulkAll = elasticClient.BulkAll(vdElasticVideoDetailModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));

            waitHandle.Wait();

            var deletedVideos = GetDeletedVideoIdsForElasticSynchronization(elasticSearchSyncEntity.UpdatedAt ?? DateTime.MinValue);

            var descriptor = new BulkDescriptor();

            foreach (var id in deletedVideos)
                descriptor.Delete<VDElasticVideoDetailModel>(x => x
                                                            .Id(id))
                                                            .Refresh(Refresh.WaitFor);

            var response = elasticClient.Bulk(descriptor);

            elasticSearchSyncEntity.LastExecutionDateTime = updatedAt;
            elasticSearchSyncEntity.Description = vdElasticVideoDetailModels.Count.ToString() + "records";
            elasticSearchSyncBusinessObject.Update(elasticSearchSyncEntity, updatedAt);
            elasticSearchSyncBusinessObject.Commit();
        }
        #endregion

        #region Data Fetch From Elastic
        public VideoDetailDTO GetVideoDetailsBySlugKey(string slugKey)
        {
            return GetVideoDetailsFromElasticBySlugKey(slugKey);
            //return GetVideoDetailQueryable().Where(x => x.SlugKey == slugKey).FirstOrDefault();
        }

        public async Task<VideoDetailDTO> GetVideoDetailsBySlugKeyAsync(string slugKey)
        {
            return await GetVideoDetailsFromElasticBySlugKeyAsync(slugKey);
        }

        public VideoDetailDTO GetVideoDetailsFromElasticBySlugKey(string slugKey)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Query(q => q
                                                                        .Bool(b => b
                                                                           .Must(mu => mu
                                                                               .Match(ma => ma
                                                                                   .Field(f => f.SlugKey)
                                                                                   .Query(slugKey)
                                                                                     )
                                                                                )
                                                                              )
                                                                    )
                                                                    .Take(1)
                                                                );

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<VideoDetailDTO> GetVideoDetailsFromElasticBySlugKeyAsync(string slugKey)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Query(q => q
                                                                        .Bool(b => b
                                                                           .Must(mu => mu
                                                                               .Match(ma => ma
                                                                                   .Field(f => f.SlugKey)
                                                                                   .Query(slugKey)
                                                                                     )
                                                                                )
                                                                              )
                                                                    )
                                                                    .Take(1)
                                                                );

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).FirstOrDefault();
            var result = searchResponse.Hits.Select(y => y.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public VideoDetailDTO GetVideoDetailsByVideoMD5(string videoMD5)
        {
            return GetVideoDetailsFromElasticByVideoMD5(videoMD5);
        }

        public VideoDetailDTO GetVideoDetailsFromElasticByVideoMD5(string videoMD5)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Query(q => q
                                                                        .Bool(b => b
                                                                           .Must(mu => mu
                                                                               .Match(ma => ma
                                                                                   .Field(f => f.VideoMD5)
                                                                                   .Query(videoMD5)
                                                                                     )
                                                                                )
                                                                              )
                                                                    )
                                                                    .Take(1)
                                                                );

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).FirstOrDefault();
            var result = searchResponse.Hits.Select(y => y.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<VideoDetailDTO> GetVideoDetailsFromElasticByVideoMD5Async(string videoMD5)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Query(q => q
                                                                        .Bool(b => b
                                                                           .Must(mu => mu
                                                                               .Match(ma => ma
                                                                                   .Field(f => f.VideoMD5)
                                                                                   .Query(videoMD5)
                                                                                     )
                                                                                )
                                                                              )
                                                                    )
                                                                    .Take(1)
                                                                );

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).FirstOrDefault();
            var result = searchResponse.Hits.Select(y => y.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<VideoDetailDTO> GetVideoDetailsByVideoMD5Async(string videoMD5)
        {
            return await GetVideoDetailsFromElasticByVideoMD5Async(videoMD5);
        }

        public VideoDetailDTO GetVideoDetailsByVideoId(int id)
        {
            return GetVideoDetailsFromElasticByVideoId(id);
        }

        public async Task<VideoDetailDTO> GetVideoDetailsByVideoIdAsync(int id)
        {
            return await GetVideoDetailsFromElasticByVideoIdAsync(id);
        }

        public VideoDetailDTO GetVideoDetailsFromElasticByVideoId(int videoId)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                            .Query(q => q
                                                                                .Ids(i => i
                                                                                    .Values(new Id(videoId)))));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).FirstOrDefault();
            var result = searchResponse.Hits.Select(y => y.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<VideoDetailDTO> GetVideoDetailsFromElasticByVideoIdAsync(int videoId)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Query(q => q
                                                                        .Ids(i => i
                                                                            .Values(new Id(videoId)
                                                                            )
                                                                        )
                                                                    )
                                                                );

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).FirstOrDefault();
            var result = searchResponse.Hits.Select(y => y.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public List<PlaylistVideoInfoDTO> GetVideosByPlaylist(int playlistId, int? top, int? skip = 0)
        {
            return GetVideosFromElasticByPlaylistId(playlistId, top, skip);
        }

        public async Task<IEnumerable<PlaylistVideoInfoDTO>> GetVideosByPlaylistAsync(int playlistId, int? top, int skip = 0)
        {
            return await GetVideosFromElasticByPlaylistIdAsync(playlistId, top, skip);
        }

        public List<PlaylistVideoInfoDTO> GetVideosFromElasticByPlaylistId(int playlistId, int? top, int? skip = 0)
        {
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = _client.Search<VDElasticPlaylistModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.VideoDetails)
                                                                        )
                                                                    )
                                                                    .Query(q => q
                                                                        .Ids(i => i
                                                                            .Values(new Id(playlistId)
                                                                            )
                                                                        )
                                                                    )
                                                                    .Skip(skip)
                                                                    .Take(top)
                                                                );

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).Skip(skip.Value).Take(top.Value).ToList();

            if (result != null && result[0] != null && result[0].VideoDetails != null)
            {
                return result[0]?.VideoDetails.OrderBy(x => x.Order).Skip(skip.Value).Take(top.Value).ToList().AsPlaylistVideoInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<PlaylistVideoInfoDTO>> GetVideosFromElasticByPlaylistIdAsync(int playlistId, int? top, int? skip = 0)
        {
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticPlaylistModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f
                                                                                .VideoDetails
                                                                                )
                                                                        )
                                                                    )
                                                                    .Query(q => q
                                                                        .Ids(i => i
                                                                            .Values(new Id(playlistId)
                                                                            )
                                                                        )
                                                                    )
                                                                );

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null && result[0] != null && result[0].VideoDetails != null)
            {
                return (top.HasValue) ?
                result[0]?.VideoDetails.OrderBy(x => x.Order).Skip(skip.Value).Take(top.Value).ToList().AsPlaylistVideoInfoDTOList() :
                result[0]?.VideoDetails.OrderBy(x => x.Order).Skip(skip.Value).ToList().AsPlaylistVideoInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public VideosDTO GetVideosByChannel(int channelId, int? skip = 0, int? top = 10)
        {
            return GetVideoDetailsFromElasticByChannelId(channelId, skip, top);
        }

        public async Task<VideosDTO> GetVideosByChannelAsync(int channelId, int skip = 0, int top = 10)
        {
            return await GetVideoDetailsFromElasticByChannelIdAsync(channelId, skip, top);
        }

        public VideosDTO GetVideoDetailsFromElasticByChannelId(int channelId, int? skip = 0, int? top = 10)
        {
            VideosDTO videosDTO = new VideosDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("videos_by_channel")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.ChannelId)
                                                                            .Value(channelId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value(1)))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                videosDTO.TotalCount = searchResponse.Total;
                videosDTO.VideoInfos = result.AsVideoInfoDTOList();
                return videosDTO;
            }
            else
            {
                return null;
            }
        }

        public async Task<VideosDTO> GetVideoDetailsFromElasticByChannelIdAsync(int channelId, int? skip = 0, int? top = 10)
        {
            VideosDTO videosDTO = new VideosDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("videos_by_channel")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.ChannelId)
                                                                            .Value(channelId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                videosDTO.TotalCount = searchResponse.Total;
                videosDTO.VideoInfos = result.AsVideoInfoDTOList();
                return videosDTO;
            }
            else
            {
                return null;
            }
        }

        public VideosDTO GetVideoDetailsFromElasticByChannelAndCategoryId(int channelId, int categoryId, int? skip = 0, int? top = 10)
        {
            VideosDTO videosDTO = new VideosDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("videos_by_channel")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.ChannelId)
                                                                            .Value(channelId)) &&
                                                                        q.Term(c => c
                                                                            .Name("videos_by_category")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                videosDTO.TotalCount = searchResponse.Total;
                videosDTO.VideoInfos = result.AsVideoInfoDTOList();
                return videosDTO;
            }
            else
            {
                return null;
            }
        }

        public async Task<VideosDTO> GetVideoDetailsFromElasticByChannelAndCategoryIdAsync(int channelId, int categoryId, int? skip = 0, int? top = 10)
        {
            VideosDTO videosDTO = new VideosDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("videos_by_channel")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.ChannelId)
                                                                            .Value(channelId)) &&
                                                                        q.Term(c => c
                                                                            .Name("videos_by_category")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                videosDTO.TotalCount = searchResponse.Total;
                videosDTO.VideoInfos = result.AsVideoInfoDTOList();
                return videosDTO;
            }
            else
            {
                return null;
            }
        }

        public SearchDTO SearchVideos(string searchText, int? skip = 0, int? top = 10)
        {
            SearchDTO searchDTO = new SearchDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;
            //var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
            //                                                                 .Query(q => q
            //                                                                     .Match(m => m
            //                                                                         .Field(f => f.Title)
            //                                                                         .Query(searchText)
            //                                                                         .Operator(Operator.Or)
            //                                                                      )
            //                                                             ));
            //var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
            //                        .Query(q => q
            //                            .Fuzzy(c => c
            //                                .Name("videosearch")
            //                                .Boost(1.1)
            //                                .Field(p => p.Title)
            //                                .Fuzziness(Fuzziness.Auto)
            //                                .Value(searchText.ToLowerInvariant())
            //                                .Rewrite(MultiTermQueryRewrite.ConstantScore)
            //                                .Transpositions(true)
            //                            )
            //                        )
            //                      );
            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                        .Query(q => q
                                                            .Match(m => m
                                                                //.Field(f => f.Description)
                                                                .Field(f => f.Title)
                                                                .Query(searchText)
                                                                .Operator(Operator.Or)
                                                                .Fuzziness(Fuzziness.Auto)))
                                                        .Skip(skip)
                                                        .Take(top));
            //var resultList = (from hits in searchResponse.Hits
            //                  select hits.Source).ToList();
            var resultList = searchResponse.Hits.Select(y => y.Source).ToList();

            if (resultList != null)
            {
                searchDTO.TotalCount = searchResponse.Total;
                searchDTO.VideoInfos = resultList.AsVideoInfoDTOList();
                return searchDTO;
            }
            else
            {
                return null;
            }
        }

        public VideosDTO GetVideosByChannelsCategories(int channelId, int categoryId, int? skip = 0, int? top = 10)
        {
            return GetVideoDetailsFromElasticByChannelAndCategoryId(channelId, categoryId, skip, top);
        }

        public async Task<VideosDTO> GetVideosByChannelsCategoriesAsync(int channelId, int categoryId, int? skip = 0, int? top = 10)
        {
            return await GetVideoDetailsFromElasticByChannelAndCategoryIdAsync(channelId, categoryId, skip, top);
        }

        public VideosDTO GetVideosByCategory(int categoryId, int? skip = 0, int? top = 10)
        {
            return GetVideoDetailsFromElasticByCategoryId(categoryId, skip, top);
        }

        public async Task<VideosDTO> GetVideosByCategoryAsync(int categoryId, int? skip = 0, int? top = 10)
        {
            return await GetVideoDetailsFromElasticByCategoryIdAsync(categoryId, skip, top);
        }

        public VideosDTO GetVideoDetailsFromElasticByCategoryId(int categoryId, int? skip = 0, int? top = 10)
        {
            VideosDTO videosDTO = new VideosDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("videos_by_channel")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                videosDTO.TotalCount = searchResponse.Total;
                videosDTO.VideoInfos = result.AsVideoInfoDTOList();
                return videosDTO;
            }
            else
            {
                return null;
            }
        }

        public async Task<VideosDTO> GetVideoDetailsFromElasticByCategoryIdAsync(int categoryId, int? skip = 0, int? top = 10)
        {
            VideosDTO videosDTO = new VideosDTO();
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q => q
                                                                        .Term(c => c
                                                                            .Name("videos_by_category")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                videosDTO.TotalCount = searchResponse.Total;
                videosDTO.VideoInfos = result.AsVideoInfoDTOList();
                return videosDTO;
            }
            else
            {
                return null;
            }
        }

        public List<HottestVideoDTO> GetHottestVideosFromElastic(int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticHottestVideoModel>().Client;

            var searchResponse = _client.Search<VDElasticHottestVideoModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.VideoCardImageUrl)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.CreatedAt)
                                                                                    .Field(f => f.ViewCount)
                                                                                    .Field(f => f.VideoMD5)
                                                                                    .Field(f => f.CategoryId)
                                                                                    .Field(f => f.ChannelName)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Skip(skip)
                                                                            .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsHottestVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<HottestVideoDTO>> GetHottestVideosFromElasticAsync(int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticHottestVideoModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticHottestVideoModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.VideoCardImageUrl)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.CreatedAt)
                                                                                    .Field(f => f.ViewCount)
                                                                                    .Field(f => f.VideoMD5)
                                                                                    .Field(f => f.CategoryId)
                                                                                    .Field(f => f.ChannelName)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Skip(skip)
                                                                            .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsHottestVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<TrendingVideoDTO> GetTrendingVideosFromElastic(int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticTrendingVideoModel>().Client;

            var searchResponse = _client.Search<VDElasticTrendingVideoModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.VideoCardImageUrl)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.CreatedAt)
                                                                                    .Field(f => f.ViewCount)
                                                                                    .Field(f => f.VideoMD5)
                                                                                    .Field(f => f.CategoryId)
                                                                                    .Field(f => f.ChannelName)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Skip(skip)
                                                                            .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsTrendingVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<TrendingVideoDTO>> GetTrendingVideosFromElasticAsync(int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticTrendingVideoModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticTrendingVideoModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.VideoCardImageUrl)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.CreatedAt)
                                                                                    .Field(f => f.ViewCount)
                                                                                    .Field(f => f.VideoMD5)
                                                                                    .Field(f => f.CategoryId)
                                                                                    .Field(f => f.ChannelName)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Skip(skip)
                                                                            .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsTrendingVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<HottestVideoDTO> GetHottestVideos(int? skip = 0, int? top = 10)
        {
            return GetHottestVideosFromElastic(skip, top);
            //return GetHottestVideosQueryable().Skip(skip.Value).Take(top.Value).ToList();
        }

        public async Task<List<HottestVideoDTO>> GetHottestVideosAsync(int? skip = 0, int? top = 10)
        {
            return await GetHottestVideosFromElasticAsync(skip, top);
            //return await GetHottestVideosQueryable().Skip(skip.Value).Take(top.Value).ToListAsync();
        }

        public List<TrendingVideoDTO> GetTrendingVideos(int? skip = 0, int? top = 10)
        {
            return GetTrendingVideosFromElastic(skip, top);
            //return GetTrendingVideosQueryable().Skip(skip.Value).Take(top.Value).ToList();
        }

        public async Task<List<TrendingVideoDTO>> GetTrendingVideosAsync(int? skip = 0, int? top = 10)
        {
            return await GetTrendingVideosFromElasticAsync(skip, top);
            //return await GetTrendingVideosQueryable().Skip(skip.Value).Take(top.Value).ToListAsync();
        }

        public List<RecommendedVideoDTO> GetRecommendedVideosFromElastic(int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticRecommendedVideoModel>().Client;

            var searchResponse = _client.Search<VDElasticRecommendedVideoModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.VideoCardImageUrl)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.CreatedAt)
                                                                                    .Field(f => f.ViewCount)
                                                                                    .Field(f => f.VideoMD5)
                                                                                    .Field(f => f.CategoryId)
                                                                                    .Field(f => f.ChannelName)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Skip(skip)
                                                                            .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsRecommendedVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<RecommendedVideoDTO>> GetRecommendedVideosFromElasticAsync(int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticRecommendedVideoModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticRecommendedVideoModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.VideoCardImageUrl)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.CreatedAt)
                                                                                    .Field(f => f.ViewCount)
                                                                                    .Field(f => f.VideoMD5)
                                                                                    .Field(f => f.CategoryId)
                                                                                    .Field(f => f.ChannelName)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Skip(skip)
                                                                            .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsRecommendedVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<StunningVideoDTO> GetStunningVideosFromElastic(int categoryId, int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("videos_by_category")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsStunningVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<StunningVideoDTO>> GetStunningVideosFromElasticAsync(int categoryId, int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.VideoCardImageUrl)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.CreatedAt)
                                                                            .Field(f => f.ViewCount)
                                                                            .Field(f => f.VideoMD5)
                                                                            .Field(f => f.CategoryId)
                                                                            .Field(f => f.ChannelName)
                                                                        ))
                                                                    .Query(q => q
                                                                        .Term(c => c
                                                                            .Name("videos_by_category")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId)) &&
                                                                        q.Term(p => p
                                                                            .Name("videos_by_privacy")
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value("1")))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Skip(skip)
                                                                    .Take(top));

            //var result = (from hits in searchResponse.Hits
            //              select hits.Source).ToList();
            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (result != null)
            {
                return result.AsStunningVideoDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<RecommendedVideoDTO> GetRecommendedVideos(int? skip = 0, int? top = 10)
        {
            return GetRecommendedVideosFromElastic(skip, top);
        }

        public async Task<List<RecommendedVideoDTO>> GetRecommendedVideosAsync(int? skip = 0, int? top = 10)
        {
            return await GetRecommendedVideosFromElasticAsync(skip, top);
        }

        public List<StunningVideoDTO> GetStunningVideos(int categoryId, int? skip = 0, int? top = 10)
        {
            return GetStunningVideosFromElastic(categoryId, skip, top);
            //return GetStunningVideosQueryable().Where(x => x.CategoryId == categoryId).OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value).ToList();
        }

        public async Task<IEnumerable<StunningVideoDTO>> GetStunningVideosAsync(int categoryId, int? skip = 0, int? top = 10)
        {
            return await GetStunningVideosFromElasticAsync(categoryId, skip, top);
            //return await GetStunningVideosQueryable().Where(x => x.CategoryId == categoryId).OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value).ToListAsync();
        }

        public List<VideoInfoDTO> GetUpNextVideos(string slugKey, int channelId, int categoryId, int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            //var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
            //                                                        .Query(q =>
            //                                                            q.Bool(b => b
            //                                                               .MustNot(mu => mu
            //                                                                   .Match(ma => ma
            //                                                                       .Field(f => f.SlugKey)
            //                                                                       .Query(slugKey)))
            //                                                               .Must(mu => mu
            //                                                                   .Match(ma => ma
            //                                                                       .Field(f => f.ChannelId)
            //                                                                       .Query(channelId.ToString())))
            //                                                               .Must(mu => mu
            //                                                                   .Match(ma => ma
            //                                                                       .Field(f => f.PrivacyId)
            //                                                                       .Query("1")))))
            //                                                        .Skip(skip)
            //                                                        .Take(top));

            var searchResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                            .Query(q =>
                                                                                q.Term(t => t
                                                                                    .Field(f => f.ChannelId)
                                                                                    .Value(channelId.ToString())) &&
                                                                                q.Term(t => t
                                                                                    .Field(f => f.PrivacyId)
                                                                                    .Value("1")) &&
                                                                                !q.Term(t => t
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Value(slugKey)))
                                                                            .Skip(skip)
                                                                            .Take(top));

            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (top > result.Count)
            {
                var searchByCategoryResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                                            .Query(q =>
                                                                                                !q.Term(t => t
                                                                                                    .Field(f => f.SlugKey)
                                                                                                    .Value(slugKey)) &&
                                                                                                q.Term(t => t
                                                                                                    .Field(f => f.CategoryId)
                                                                                                    .Value(categoryId.ToString())) &&
                                                                                                q.Term(t => t
                                                                                                    .Field(f => f.PrivacyId)
                                                                                                    .Value("1")))
                                                                                            //.Bool(b => b
                                                                                            //   .MustNot(mu => mu
                                                                                            //       .Match(ma => ma
                                                                                            //           .Field(f => f.SlugKey)
                                                                                            //           .Query(slugKey)))
                                                                                            //   .Must(mu => mu
                                                                                            //       .Match(ma => ma
                                                                                            //           .Field(f => f.CategoryId)
                                                                                            //           .Query(categoryId.ToString()))))
                                                                                            .Skip(skip)
                                                                                            .Take(top - result.Count));

                var resultByCategory = searchByCategoryResponse.Hits.Select(y => y.Source).ToList();

                resultByCategory.ForEach(x => result.Add(x));
            }

            if (result != null)
            {
                return result.AsVideoInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<VideoInfoDTO>> GetUpNextVideosAsync(string slugKey, int channelId, int categoryId, int? skip = 0, int? top = 10)
        {
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            //var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
            //                                                        .Query(q => q
            //                                                            .Bool(b => b
            //                                                               .MustNot(mu => mu
            //                                                                   //.Term(t => t
            //                                                                   //     .Field(f => f.SlugKey)
            //                                                                   //     .Value(slugKey))
            //                                                                   .Match(ma => ma
            //                                                                       .Field(f => f.SlugKey)
            //                                                                       .Query(slugKey)))
            //                                                               .Must(mu => mu
            //                                                                   .Match(ma => ma
            //                                                                       .Field(f => f.ChannelId)
            //                                                                       .Query(channelId.ToString())))
            //                                                               .Must(mu => mu
            //                                                                   .Match(ma => ma
            //                                                                       .Field(f => f.PrivacyId)
            //                                                                       .Query("1")))))
            //                                                        .Skip(skip)
            //                                                        .Take(top));

            var searchResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                                        .Query(q =>
                                                                                            q.Term(t => t
                                                                                                .Field(f => f.ChannelId)
                                                                                                .Value(channelId.ToString())) &&
                                                                                            q.Term(t => t
                                                                                                .Field(f => f.PrivacyId)
                                                                                                .Value("1")) &&
                                                                                            !q.Term(t => t
                                                                                                .Field(f => f.SlugKey)
                                                                                                .Value(slugKey)))
                                                                                        .Skip(skip)
                                                                                        .Take(top));

            var result = searchResponse.Hits.Select(y => y.Source).ToList();

            if (top > result.Count)
            {
                var searchByCategoryResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                                            .Query(q => 
                                                                                                !q.Term(t => t
                                                                                                    .Field(f => f.SlugKey)
                                                                                                    .Value(slugKey)) &&
                                                                                                q.Term(t => t
                                                                                                    .Field(f => f.CategoryId)
                                                                                                    .Value(categoryId.ToString())) &&
                                                                                                q.Term(t => t
                                                                                                    .Field(f => f.PrivacyId)
                                                                                                    .Value("1")))
                                                                                                           //.Bool(b => b
                                                                                                           //   .MustNot(mu => mu
                                                                                                           //       .Match(ma => ma
                                                                                                           //           .Field(f => f.SlugKey)
                                                                                                           //           .Query(slugKey)))
                                                                                                           //   .Must(mu => mu
                                                                                                           //       .Match(ma => ma
                                                                                                           //           .Field(f => f.CategoryId)
                                                                                                           //           .Query(categoryId.ToString()))))
                                                                                            .Skip(skip)
                                                                                            .Take(top - result.Count));

                var resultByCategory = searchByCategoryResponse.Hits.Select(y => y.Source).ToList();

                resultByCategory.ForEach(x => result.Add(x));
            }

            if (result != null)
            {
                return result.AsVideoInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<SitemapVideoDetailDTO> GetAllVideos()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            ISearchResponse<VDElasticVideoDetailModel> initialResponse = _client.Search<VDElasticVideoDetailModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.Title)
                                                                            .Field(f => f.ThumbnailUrl)
                                                                            .Field(f => f.SlugKey)
                                                                            .Field(f => f.VideoId)
                                                                            .Field(f => f.Duration)
                                                                            .Field(f => f.Description)
                                                                            .Field(f => f.VideoFiles)
                                                                        ))
                                                                    .Query(q => q
                                                                        .Term(c => c
                                                                            .Boost(1.1)
                                                                            .Field(f => f.PrivacyId)
                                                                            .Value(1)))
                                                                    .Sort(so => so.Descending(d => d.ViewCount))
                                                                    .Size(5000)
                                                                    .Scroll(scrollTimeout));

            List<VDElasticVideoDetailModel> results = new List<VDElasticVideoDetailModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticVideoDetailModel> loopingResponse = _client.Scroll<VDElasticVideoDetailModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            _client.ClearScroll(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsSitemapVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<SitemapVideoDetailDTO>> GetAllVideosAsync()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticVideoDetailModel>().Client;

            ISearchResponse<VDElasticVideoDetailModel> initialResponse = await _client.SearchAsync<VDElasticVideoDetailModel>(s => s
                                                                            .Source(so => so
                                                                                .Includes(i => i
                                                                                    .Field(f => f.Title)
                                                                                    .Field(f => f.ThumbnailUrl)
                                                                                    .Field(f => f.SlugKey)
                                                                                    .Field(f => f.VideoId)
                                                                                    .Field(f => f.Duration)
                                                                                    .Field(f => f.Description)
                                                                                    .Field(f => f.VideoFiles)
                                                                                ))
                                                                            .Query(q => q
                                                                                .Term(c => c
                                                                                    .Boost(1.1)
                                                                                    .Field(f => f.PrivacyId)
                                                                                    .Value(1)))
                                                                            .Sort(so => so.Descending(d => d.ViewCount))
                                                                            .Size(5000)
                                                                            .Scroll(scrollTimeout));

            List<VDElasticVideoDetailModel> results = new List<VDElasticVideoDetailModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticVideoDetailModel> loopingResponse = await _client.ScrollAsync<VDElasticVideoDetailModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            await _client.ClearScrollAsync(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsSitemapVideoDetailDTO();
            }
            else
            {
                return null;
            }
        }
        #endregion

        //TO BE DELETED
        [Obsolete]
        public List<VideoInfoDTO> GetRelated(string slugKey, int? skip = 0, int? top = 10)
        {
            var relatedVideos = GetRelatedVideosByPartnerQueryable(slugKey).OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value).ToList();

            if (relatedVideos.Count < top)
            {
                var relatedVideosByCategory = GetRelatedVideoByCategory().OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value - relatedVideos.Count).ToList();

                foreach (var relatedVideo in relatedVideosByCategory)
                {
                    relatedVideos.Add(relatedVideo);
                }
            }

            return relatedVideos;
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetRelatedAsync(string slugKey, int? skip = 0, int? top = 10)
        {
            var relatedVideos = await GetRelatedVideosByPartnerQueryable(slugKey).OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value).ToListAsync();

            if (relatedVideos.Count < top)
            {
                var relatedVideosByCategory = await GetRelatedVideoByCategory().OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value - relatedVideos.Count).ToListAsync();

                foreach (var item in relatedVideosByCategory)
                {
                    relatedVideos.Add(item);
                }
            }

            return relatedVideos;
        }

        //TO BE DELETED
        [Obsolete]
        public List<StunningVideoDTO> GetStunningVideos2(int categoryId, int? skip = 0, int? top = 10)
        {
            return GetStunningVideosQueryable().Where(x => x.CategoryId == categoryId).OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<StunningVideoDTO>> GetStunningVideosAsync2(int categoryId, int? skip = 0, int? top = 10)
        {
            return await GetStunningVideosQueryable().Where(x => x.CategoryId == categoryId).OrderByDescending(x => x.ViewCount).Skip(skip.Value).Take(top.Value).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<StunningVideoDTO> GetRandomVideos(int? top = 10)
        {
            Random random = new Random();
            var skip = random.Next(1, 100);
            return GetStunningVideosQueryable().OrderByDescending(x => x.ViewCount).Skip(skip).Take(top.Value).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<StunningVideoDTO>> GetRandomVideosAsync(int? top = 10)
        {
            Random random = new Random();
            var skip = random.Next(1, 100);
            return await GetStunningVideosQueryable().OrderByDescending(x => x.ViewCount).Skip(skip).Take(top.Value).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<VideoInfoDTO> GetVideosByCategory2(int categoryId, int skip = 0, int top = 10)
        {
            return GetVideosByCategoryQueryable(categoryId).Skip(skip).Take(top).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<List<VideoInfoDTO>> GetVideosByCategoryAsync2(int categoryId, int skip = 0, int top = 10)
        {
            return await GetVideosByCategoryQueryable(categoryId).Skip(skip).Take(top).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<VideoInfoDTO> GetVideosByChannelsCategories2(int channelId, int categoryId, int skip = 0, int top = 10)
        {
            return GetVideosByChannelsCategoriesQueryable(channelId, categoryId).Skip(skip).Take(top).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<List<VideoInfoDTO>> GetVideosByChannelsCategoriesAsync2(int channelId, int categoryId, int skip = 0, int top = 10)
        {
            return await GetVideosByChannelsCategoriesQueryable(channelId, categoryId).OrderByDescending(x => x.ViewCount).Skip(skip).Take(top).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public VideoDetailDTO GetVideoDetailsBySlugKey2(string slugKey)
        {
            return GetVideoDetailQueryable().Where(x => x.SlugKey == slugKey).FirstOrDefault();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<VideoDetailDTO> GetVideoDetailsBySlugKeyAsync2(string slugKey)
        {
            return await GetVideoDetailQueryable().Where(x => x.SlugKey == slugKey).FirstOrDefaultAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public VideoDetailDTO GetVideoDetailsByVideoMD52(string videoMD5)
        {
            return GetVideoDetailQueryable().Where(x => x.VideoMD5 == videoMD5).FirstOrDefault();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<VideoDetailDTO> GetVideoDetailsByVideoMD5Async2(string videoMD5)
        {
            return await GetVideoDetailQueryable().Where(x => x.VideoMD5 == videoMD5).FirstOrDefaultAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public VideoDetailDTO GetVideoDetailsByVideoId2(int id)
        {
            return GetVideoDetailQueryable().Where(x => x.VideoId == id).FirstOrDefault();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<VideoDetailDTO> GetVideoDetailsByVideoIdAsync2(int id)
        {
            return await GetVideoDetailQueryable().Where(x => x.VideoId == id).FirstOrDefaultAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<VideoInfoDTO> GetVideosByPlaylist2(int playlistId, int skip = 0, int top = 10)
        {
            return GetVideosByPlaylistQueryable(playlistId).Skip(skip).Take(top).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetVideosByPlaylistAsync2(int playlistId, int skip = 0, int top = 10)
        {
            //return await GetVideosFromElasticByPlaylistIdAsync(playlistId, skip, top);
            return await GetVideosByPlaylistQueryable(playlistId).Skip(skip).Take(top).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<VideoInfoDTO> GetVideosByChannel2(int channelId, int skip = 0, int top = 10)
        {
            return GetVideosByChannelQueryable(channelId).OrderByDescending(x => x.ViewCount).Skip(skip).Take(top).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<VideoInfoDTO>> GetVideosByChannelAsync2(int channelId, int skip = 0, int top = 10)
        {
            return await GetVideosByChannelQueryable(channelId).OrderByDescending(x => x.ViewCount).Skip(skip).Take(top).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<HottestVideoDTO> GetHottestVideos2(int? skip = 0, int? top = 10)
        {
            return GetHottestVideosQueryable().Skip(skip.Value).Take(top.Value).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<List<HottestVideoDTO>> GetHottestVideosAsync2(int? skip = 0, int? top = 10)
        {
            return await GetHottestVideosQueryable().Skip(skip.Value).Take(top.Value).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public List<TrendingVideoDTO> GetTrendingVideos2(int? skip = 0, int? top = 10)
        {
            return GetTrendingVideosQueryable().Skip(skip.Value).Take(top.Value).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<List<TrendingVideoDTO>> GetTrendingVideosAsync2(int? skip = 0, int? top = 10)
        {
            return await GetTrendingVideosQueryable().Skip(skip.Value).Take(top.Value).ToListAsync();
        }
    }
}
