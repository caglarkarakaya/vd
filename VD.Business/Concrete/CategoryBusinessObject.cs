﻿using Nest;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.DTO.Concrete;
using VD.Elastic.Client;
using VD.Elastic.Extentions;
using VD.Elastic.Models;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class CategoryBusinessObject : VDBusinessObjectBase<CategoryEntity>
    {
        public CategoryBusinessObject()
        {

        }

        public CategoryBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        internal IQueryable<CategoryDTO> GetCategoriesQueryable()
        {
            var categories = from c in Get()
                             select new CategoryDTO()
                             {
                                 Id = c.Id,
                                 Title = c.Title,
                                 IconUrl = c.IconUrl
                             };
            return categories;
        }

        internal IQueryable<VDElasticCategoryModel> GetCategoriesForElasticSynchronization()
        {
            var group = from v in new VideoBusinessObject(UnitOfWork).Get()
                        join c in Get() on v.CategoryId equals c.Id
                        where 
                        (
                            v.PrivacyId == 1 &&
                            v.ApproveStatusId == 2
                        )
                        group v by v.CategoryId into g
                        select new
                        {
                            CategoryId = g.Key,
                            Count = g.Count()
                        };

            var categories = from c in Get()
                             join g in @group on c.Id equals g.CategoryId into c2
                             from g in c2.DefaultIfEmpty()
                             select new VDElasticCategoryModel()
                             {
                                 Id = c.Id,
                                 Title = c.Title,
                                 IconUrl = c.IconUrl,
                                 VideoCount = g.Count != null ? g.Count : 0//due to the left join the count can be null even tho it says the otherwise.
                             };

            return categories;
        }

        public void SynchronizeElasticCategories()
        {
            DateTime updatedAt = DateTime.Now;

            var elasticSearchSyncBusinessObject = new ElasticSearchSyncBusinessObject(UnitOfWork);
            var elasticSearchSyncEntity = elasticSearchSyncBusinessObject.Get(x => x.Type == "category").FirstOrDefault();

            List<VDElasticCategoryModel> vdElasticCategoryModels = GetCategoriesForElasticSynchronization().ToList();

            var elasticClient = new VDElasticClient<VDElasticCategoryModel>().Client;

            var bulkAll = elasticClient.BulkAll(vdElasticCategoryModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));

            waitHandle.Wait();

            elasticSearchSyncEntity.LastExecutionDateTime = updatedAt;
            elasticSearchSyncEntity.Description = vdElasticCategoryModels.Count.ToString() + "records";
            elasticSearchSyncBusinessObject.Update(elasticSearchSyncEntity, updatedAt);
            elasticSearchSyncBusinessObject.Commit();
        }

        public List<CategoryDTO> GetCategoriesFromElastic()
        {
            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            var searchResponse = _client.Search<VDElasticCategoryModel>(s => s
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)
                                                                                .Field(f => f.Title)
                                                                                .Field(f => f.IconUrl)
                                                                                .Field(f => f.VideoCount)
                                                                            ))
                                                                        .Take(1000)
                                                                        .Query(q => q.MatchAll())
                                                                        .Sort(so => so.Ascending(d => d.Id)));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                return result.AsCategoryDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<CategoryDTO>> GetCategoriesFromElasticAsync()
        {
            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticCategoryModel>(s => s
                                                                                    .Source(so => so
                                                                                        .Includes(i => i
                                                                                            .Field(f => f.Id)
                                                                                            .Field(f => f.Title)
                                                                                            .Field(f => f.IconUrl)
                                                                                            .Field(f => f.VideoCount)
                                                                                        ))
                                                                                    .Take(1000)
                                                                                    .Query(q => q.MatchAll())
                                                                                    .Sort(so => so.Ascending(d => d.Id)));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                return result.AsCategoryDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<CategoryDTO> GetCategories()
        {
            return GetCategoriesFromElastic();
        }

        public async Task<List<CategoryDTO>> GetCategoriesAsync()
        {
            return await GetCategoriesFromElasticAsync();
        }

        public CategoryDTO GetCategoryFromElastic(int categoryId)
        {
            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            var searchResponse = _client.Search<VDElasticCategoryModel>(s => s
                                                                        .Query(q => q
                                                                            .Ids(i => i
                                                                                .Values(new Id(categoryId)))));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsCategoryDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<CategoryDTO> GetCategoryFromElasticAsync(int categoryId)
        {
            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticCategoryModel>(s => s
                                                                                    .Query(q => q
                                                                                        .Ids(i => i
                                                                                            .Values(new Id(categoryId)))));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsCategoryDTO();
            }
            else
            {
                return null;
            }
        }

        public CategoryDTO GetCategory(int categoryId)
        {
            return GetCategoryFromElastic(categoryId);
        }

        public async Task<CategoryDTO> GetCategoryAsync(int categoryId)
        {
            return await GetCategoryFromElasticAsync(categoryId);
        }

        public List<ChannelsCategoryInfoDTO> GetCategoriesOfChannelFromElasticByChannelId(int channelId, int skip = 0, int top = 10)
        {
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            var searchResponse = _client.Search<VDElasticChannelModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.CategoryInfos)
                                                                        )
                                                                    )
                                                                    .Query(q => q
                                                                        .Ids(i => i
                                                                            .Values(new Id(channelId)
                                                                            )
                                                                        )
                                                                    )
                                                                    .Skip(skip)
                                                                    .Take(top)
                                                                );

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null && result[0] != null && result[0].CategoryInfos != null)
            {
                return result[0]?.CategoryInfos?.AsChannelsCategoryInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<ChannelsCategoryInfoDTO>> GetCategoriesOfChannelFromElasticByChannelIdAsync(int channelId, int skip = 0, int top = 10)
        {
            var _client = new VDElasticClient<VDElasticChannelModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticChannelModel>(s => s
                                                                    .Source(so => so
                                                                        .Includes(i => i
                                                                            .Field(f => f.CategoryInfos)
                                                                        )
                                                                    )
                                                                    .Query(q => q
                                                                        .Ids(i => i
                                                                            .Values(new Id(channelId)
                                                                            )
                                                                        )
                                                                    )
                                                                    .Skip(skip)
                                                                    .Take(top)
                                                                );

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null && result[0] != null && result[0].CategoryInfos != null)
            {
                return result[0]?.CategoryInfos?.AsChannelsCategoryInfoDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<ChannelsCategoryInfoDTO> GetCategoriesOfChannel(int channelId)
        {
            return GetCategoriesOfChannelFromElasticByChannelId(channelId);
        }

        public async Task<List<ChannelsCategoryInfoDTO>> GetCategoriesOfChannelAsync(int channelId)
        {
            return await GetCategoriesOfChannelFromElasticByChannelIdAsync(channelId);
        }

        public List<CategoryDTO> GetAllCategoryIds()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            ISearchResponse<VDElasticCategoryModel> initialResponse = _client.Search<VDElasticCategoryModel>(scr => scr
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)))
                                                                        .Size(5000)
                                                                        .Scroll(scrollTimeout)
                                                                        );

            List<VDElasticCategoryModel> results = new List<VDElasticCategoryModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticCategoryModel> loopingResponse = _client.Scroll<VDElasticCategoryModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            _client.ClearScroll(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsCategoryDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<CategoryDTO>> GetAllCategoryIdsAsync()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            ISearchResponse<VDElasticCategoryModel> initialResponse = await _client.SearchAsync<VDElasticCategoryModel>(scr => scr
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)))
                                                                        .Size(5000)
                                                                        .Scroll(scrollTimeout)
                                                                        );

            List<VDElasticCategoryModel> results = new List<VDElasticCategoryModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticCategoryModel> loopingResponse = await _client.ScrollAsync<VDElasticCategoryModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            await _client.ClearScrollAsync(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsCategoryDTOList();
            }
            else
            {
                return null;
            }
        }

        //TO BE DELETED
        [Obsolete]
        public List<CategoryDTO> GetCategories2()
        {
            return GetCategoriesQueryable().ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<List<CategoryDTO>> GetCategoriesAsync2()
        {
            return await GetCategoriesQueryable().ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public CategoryDTO GetCategory2(int categoryId)
        {
            return GetCategoriesQueryable().Where(x => x.Id == categoryId).FirstOrDefault();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<CategoryDTO> GetCategoryAsync2(int categoryId)
        {
            return await GetCategoriesQueryable().Where(x => x.Id == categoryId).FirstOrDefaultAsync();
        }

        internal IQueryable<ChannelsCategoryInfoDTO> GetCategoriesOfChannelQueryable(int channelId)
        {
            var group = from p in new PartnerBusinessObject(UnitOfWork).Get(x => x.Id == channelId)
                        join up in new UserPartnerBusinessObject(UnitOfWork).Get() on p.Id equals up.PartnerId
                        join v in new VideoBusinessObject(UnitOfWork).Get() on up.Id equals v.UserPartnerId
                        join c in Get() on v.CategoryId equals c.Id
                        group v by new { v.CategoryId, c.Title } into g
                        select new ChannelsCategoryInfoDTO
                        {
                            CategoryId = g.Key.CategoryId.Value,
                            CategoryName = g.Key.Title,
                            Count = g.Count()
                        };

            return group;
        }

        //TO BE DELETED
        [Obsolete]
        public IEnumerable<ChannelsCategoryInfoDTO> GetCategoriesOfChannel2(int channelId)
        {
            return GetCategoriesOfChannelQueryable(channelId).OrderByDescending(x => x.Count).AsEnumerable();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<ChannelsCategoryInfoDTO>> GetCategoriesOfChannelAsync2(int channelId)
        {
            return await GetCategoriesOfChannelQueryable(channelId).OrderByDescending(x => x.Count).ToListAsync();
        }

        internal IQueryable<VDElasticChannelsCategoryInfoModel> GetCategoriesOfChannelForElasticQueryable(int channelId)
        {
            var group = from p in new PartnerBusinessObject(UnitOfWork).Get(x => x.Id == channelId)
                        join up in new UserPartnerBusinessObject(UnitOfWork).Get() on p.Id equals up.PartnerId
                        join v in new VideoBusinessObject(UnitOfWork).Get() on up.Id equals v.UserPartnerId
                        join c in Get() on v.CategoryId equals c.Id
                        where 
                        (
                            v.PrivacyId == 1 &&
                            v.ApproveStatusId == 2
                        )
                        group v by new { v.CategoryId, c.Title } into g
                        select new VDElasticChannelsCategoryInfoModel
                        {
                            CategoryId = g.Key.CategoryId.Value,
                            CategoryName = g.Key.Title,
                            Count = g.Count()
                        };

            return group;
        }

        public IEnumerable<VDElasticChannelsCategoryInfoModel> GetCategoriesOfChannelForElastic(int channelId)
        {
            return GetCategoriesOfChannelForElasticQueryable(channelId).OrderByDescending(x => x.Count).AsEnumerable();
        }

        public async Task<IEnumerable<VDElasticChannelsCategoryInfoModel>> GetCategoriesOfChannelAsyncForElastic(int channelId)
        {
            return await GetCategoriesOfChannelForElasticQueryable(channelId).OrderByDescending(x => x.Count).ToListAsync();
        }

        internal IQueryable<VDElasticCategoryModel> GetCategoriesForElasticQueryable()
        {
            var categories = from c in Get()
                             select new VDElasticCategoryModel()
                             {
                                 Id = c.Id,
                                 Title = c.Title
                             };

            return categories;
        }

        public void CreateCategoryIndexInElastic()
        {
            var categories = GetCategoriesForElasticQueryable().ToList();

            var _client = new VDElasticClient<VDElasticCategoryModel>().Client;

            var bulkAll = _client.BulkAll(categories, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));

            waitHandle.Wait();
        }
    }
}
