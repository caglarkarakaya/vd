﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class PlaylistVideoBusinessObject : VDBusinessObjectBase<PlaylistVideoEntity>
    {
        public PlaylistVideoBusinessObject()
        {

        }

        public PlaylistVideoBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }
}
