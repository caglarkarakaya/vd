﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.DTO.Concrete;
using VD.Elastic.Client;
using VD.Elastic.Extentions;
using VD.Elastic.Models;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class PlaylistBusinessObject : VDBusinessObjectBase<PlaylistEntity>
    {
        public PlaylistBusinessObject()
        {

        }

        public PlaylistBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        internal IQueryable<PlaylistInfoDTO> GetPlaylistsQueryable()
        {
            var playlistsByGroup = from pv in new PlaylistVideoBusinessObject(UnitOfWork).Get()
                                   join v in new VideoBusinessObject(UnitOfWork).Get() on pv.VideoId equals v.Id
                                   group pv by pv.PlaylistId into g
                                   select new
                                   {
                                       PlaylistId = g.Key,
                                       Count = g.Count()
                                   };

            var playlistList = from p in Get()
                               join pg in playlistsByGroup on p.Id equals pg.PlaylistId
                               select new PlaylistInfoDTO()
                               {
                                   Id = p.Id,
                                   CreatedAt = p.CreatedAt,
                                   Description = p.Description,
                                   IsPublic = p.IsPublic,
                                   Name = p.Name,
                                   ThumbnailUrl = p.Thumbnail,
                                   Count = pg.Count
                               };

            return playlistList;
        }

        public PlaylistsDTO GetPlaylistsFromElastic(bool? isPublic, int skip = 0, int top = 10)
        {
            PlaylistsDTO playlistsDTO = new PlaylistsDTO();
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = _client.Search<VDElasticPlaylistModel>(s => s
                                                                    .Query(q => q
                                                                        .Term(c => c
                                                                            .Name("playlists_by_ispublic")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.IsPublic)
                                                                            .Value(isPublic ?? true)
                                                                        ))
                                                                    .Sort(so => so.Descending(d => d.Count))
                                                                    .Skip(skip)
                                                                    .Take(top));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                playlistsDTO.TotalCount = searchResponse.Total;
                playlistsDTO.PlaylistInfos = result.AsPlaylistDTOList();
                return playlistsDTO;
            }
            else
            {
                return null;
            }
        }

        public async Task<PlaylistsDTO> GetPlaylistsFromElasticAsync(bool? isPublic, int skip = 0, int top = 10)
        {
            PlaylistsDTO playlistsDTO = new PlaylistsDTO();
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticPlaylistModel>(s => s
                                                                    .Query(q => q
                                                                        .Term(c => c
                                                                            .Name("playlists_by_ispublic")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.IsPublic)
                                                                            .Value(isPublic ?? true)
                                                                        ))
                                                                    .Sort(so => so.Descending(d => d.Count))
                                                                    .Skip(skip)
                                                                    .Take(top));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                playlistsDTO.TotalCount = searchResponse.Total;
                playlistsDTO.PlaylistInfos = result.AsPlaylistDTOList();
                return playlistsDTO;
            }
            else
            {
                return null;
            }
        }

        public List<PlaylistInfoDTO> GetAllPublicPlaylistIds()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            ISearchResponse<VDElasticPlaylistModel> initialResponse = _client.Search<VDElasticPlaylistModel>(scr => scr
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)
                                                                                .Field(f => f.IsPublic)))
                                                                        .Query(q => q
                                                                            .Term(c => c
                                                                                .Name("playlists_by_ispublic")
                                                                                .Boost(1.1)
                                                                                .Field(p => p.IsPublic)
                                                                                .Value(true)))
                                                                        .Size(5000)
                                                                        .Scroll(scrollTimeout)
                                                                    );

            List<VDElasticPlaylistModel> results = new List<VDElasticPlaylistModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticPlaylistModel> loopingResponse = _client.Scroll<VDElasticPlaylistModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            _client.ClearScroll(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsPlaylistDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<PlaylistInfoDTO>> GetAllPublicPlaylistIdsAsync()
        {
            string scrollTimeout = "2m";
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            ISearchResponse<VDElasticPlaylistModel> initialResponse = await _client.SearchAsync<VDElasticPlaylistModel>(scr => scr
                                                                        .Source(so => so
                                                                            .Includes(i => i
                                                                                .Field(f => f.Id)
                                                                                .Field(f => f.IsPublic)))
                                                                        .Query(q => q
                                                                            .Term(c => c
                                                                                .Name("playlists_by_ispublic")
                                                                                .Boost(1.1)
                                                                                .Field(p => p.IsPublic)
                                                                                .Value(true)))
                                                                        .Size(5000)
                                                                        .Scroll(scrollTimeout)
                                                                        );

            List<VDElasticPlaylistModel> results = new List<VDElasticPlaylistModel>();
            if (initialResponse.Documents.Any())
                results.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<VDElasticPlaylistModel> loopingResponse = await _client.ScrollAsync<VDElasticPlaylistModel>(scrollTimeout, scrollid);
                if (loopingResponse.IsValid)
                {
                    results.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            await _client.ClearScrollAsync(new ClearScrollRequest(scrollid));

            if (results != null)
            {
                return results.AsPlaylistDTOList();
            }
            else
            {
                return null;
            }
        }

        public PlaylistsDTO GetPlaylists(bool? isPublic, int skip = 0, int top = 10)
        {
            return GetPlaylistsFromElastic(isPublic, skip, top);
        }

        public async Task<PlaylistsDTO> GetPlaylistsAsync(bool? isPublic, int skip = 0, int top = 10)
        {
            return await GetPlaylistsFromElasticAsync(isPublic, skip, top);
        }

        public List<PlaylistWithCategoryDTO> GetPlaylistsWithCategoryFromElastic(bool? isPublic, int categoryId, int skip = 0, int top = 10)
        {
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = _client.Search<VDElasticPlaylistModel>(s => s
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("playlists_by_ispublic")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.IsPublic)
                                                                            .Value(isPublic ?? true)) &&
                                                                        q.Term(c => c
                                                                            .Name("playlists_by_category")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.CategoryId)
                                                                            .Value(categoryId))
                                                                        )
                                                                    .Sort(so => so.Descending(d => d.Count))
                                                                    .Skip(skip)
                                                                    .Take(top));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                return result.AsPlaylistWithCategoryDTOList();
            }
            else
            {
                return null;
            }
        }

        public async Task<List<PlaylistWithCategoryDTO>> GetPlaylistsWithCategoryFromElasticAsync(bool? isPublic, int categoryId, int skip = 0, int top = 10)
        {
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticPlaylistModel>(s => s
                                                                     .Query(q =>
                                                                         q.Term(c => c
                                                                             .Name("playlists_by_ispublic")
                                                                             .Boost(1.1)
                                                                             .Field(p => p.IsPublic)
                                                                             .Value(isPublic ?? true)) &&
                                                                         q.Term(c => c
                                                                             .Name("playlists_by_category")
                                                                             .Boost(1.1)
                                                                             .Field(p => p.CategoryId)
                                                                             .Value(categoryId))
                                                                         )
                                                                     .Sort(so => so.Descending(d => d.Count))
                                                                     .Skip(skip)
                                                                     .Take(top));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).ToList();

            if (result != null)
            {
                return result.AsPlaylistWithCategoryDTOList();
            }
            else
            {
                return null;
            }
        }

        public List<PlaylistWithCategoryDTO> GetPlaylistsWithCategory(bool? isPublic, int categoryId, int skip = 0, int top = 10)
        {
            return GetPlaylistsWithCategoryFromElastic(isPublic, categoryId, skip, top);
        }

        public async Task<IEnumerable<PlaylistWithCategoryDTO>> GetPlaylistsWithCategoryAsync(bool? isPublic, int categoryId, int skip = 0, int top = 10)
        {
            return await GetPlaylistsWithCategoryFromElasticAsync(isPublic, categoryId, skip, top);
        }

        public PlaylistInfoDTO GetPlaylistDetailFromElastic(bool? isPublic, int playlistId)
        {
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = _client.Search<VDElasticPlaylistModel>(s => s
                                                                    .Query(q => q
                                                                        .Ids(i => i
                                                                            .Values(new Id(playlistId))
                                                                        )
                                                                        //.Term(c => c
                                                                        //    .Name("playlists_by_ispublic")
                                                                        //    .Boost(1.1)
                                                                        //    .Field(p => p.IsPublic)
                                                                        //    .Value(isPublic ?? true))
                                                                        )
                                                                    .Sort(so => so.Descending(d => d.Count)));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsPlaylistDTO();
            }
            else
            {
                return null;
            }
        }

        public async Task<PlaylistInfoDTO> GetPlaylistDetailFromElasticAsync(bool? isPublic, int playlistId)
        {
            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;

            var searchResponse = await _client.SearchAsync<VDElasticPlaylistModel>(s => s
                                                                    .Query(q =>
                                                                        q.Term(c => c
                                                                            .Name("playlists_by_ispublic")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.IsPublic)
                                                                            .Value(isPublic ?? true)) &&
                                                                        q.Term(c => c
                                                                            .Name("playlists_by_Id")
                                                                            .Boost(1.1)
                                                                            .Field(p => p.Id)
                                                                            .Value(playlistId))
                                                                        )
                                                                    .Sort(so => so.Descending(d => d.Count)));

            var result = (from hits in searchResponse.Hits
                          select hits.Source).FirstOrDefault();

            if (result != null)
            {
                return result.AsPlaylistDTO();
            }
            else
            {
                return null;
            }
        }

        public PlaylistInfoDTO GetPlaylistDetail(int playlistId, bool? isPublic)
        {
            return GetPlaylistDetailFromElastic(isPublic, playlistId);
        }

        public async Task<PlaylistInfoDTO> GetPlaylistDetailAsync(int playlistId, bool? isPublic)
        {
            return await GetPlaylistDetailFromElasticAsync(isPublic, playlistId);
        }

        //TO BE DELETED
        [Obsolete]
        public List<PlaylistInfoDTO> GetPlaylists2(bool? isPublic, int skip = 0, int top = 10)
        {
            return GetPlaylistsQueryable().OrderByDescending(x => x.Count).Where(x => (isPublic.HasValue ? x.IsPublic == isPublic : true)).Skip(skip).Take(top).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<PlaylistInfoDTO>> GetPlaylistsAsync2(bool? isPublic, int skip = 0, int top = 10)
        {
            return await GetPlaylistsQueryable().OrderByDescending(x => x.Count).Where(x => (isPublic.HasValue ? x.IsPublic == isPublic : true)).Skip(skip).Take(top).ToListAsync();
        }

        internal IQueryable<PlaylistWithCategoryDTO> GetPlaylistsByCategoryQueryable()
        {
            var playlistCategoryGroup = from v in new VideoBusinessObject(UnitOfWork).Get()
                                        join pv in new PlaylistVideoBusinessObject(UnitOfWork).Get() on v.Id equals pv.VideoId
                                        group v by new { v.CategoryId, pv.PlaylistId } into g
                                        select new
                                        {
                                            g.Key.PlaylistId,
                                            g.Key.CategoryId,
                                            Count = g.Count()
                                        };
            var playlistCategory = from x in playlistCategoryGroup
                                   group x by x.PlaylistId into g
                                   let max = g.Max(x => x.Count)
                                   select new
                                   {
                                       PlaylistId = g.Key,
                                       g.FirstOrDefault(x => x.Count == max).CategoryId,
                                       Count = g.Count()
                                   };

            var playlist = from p in GetPlaylistsQueryable()
                           join pc in playlistCategory on p.Id equals pc.PlaylistId
                           select new PlaylistWithCategoryDTO()
                           {
                               Id = p.Id,
                               CreatedAt = p.CreatedAt,
                               Description = p.Description,
                               IsPublic = p.IsPublic,
                               Name = p.Name,
                               Count = p.Count,
                               CategoryId = pc.CategoryId ?? 0,
                               ThumbnailUrl = p.ThumbnailUrl
                           };

            return playlist;
        }

        //TO BE DELETED
        [Obsolete]
        public List<PlaylistWithCategoryDTO> GetPlaylistsWithCategory2(bool? isPublic, int? categoryId = null, int skip = 0, int top = 10)
        {
            return GetPlaylistsByCategoryQueryable().Where(x => ((isPublic.HasValue ? x.IsPublic == isPublic : true) && (categoryId.HasValue ? x.CategoryId == categoryId : true))).OrderByDescending(x => x.Count).Skip(skip).Take(top).ToList();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<IEnumerable<PlaylistWithCategoryDTO>> GetPlaylistsWithCategoryAsync2(bool? isPublic, int? categoryId = null, int skip = 0, int top = 10)
        {
            return await GetPlaylistsByCategoryQueryable().Where(x => ((isPublic.HasValue ? x.IsPublic == isPublic : true) && (categoryId.HasValue ? x.CategoryId == categoryId : true))).OrderByDescending(x => x.Count).Skip(skip).Take(top).ToListAsync();
        }

        //TO BE DELETED
        [Obsolete]
        public PlaylistInfoDTO GetPlaylistDetail2(int playlistId, bool? isPublic)
        {
            return GetPlaylistsQueryable().Where(x => (x.Id == playlistId && (isPublic.HasValue ? x.IsPublic == isPublic : true))).FirstOrDefault();
        }

        //TO BE DELETED
        [Obsolete]
        public async Task<PlaylistInfoDTO> GetPlaylistDetailAsync2(int playlistId, bool? isPublic)
        {
            return await GetPlaylistsQueryable().Where(x => (x.Id == playlistId && (isPublic.HasValue ? x.IsPublic == isPublic : true))).FirstOrDefaultAsync();
        }

        internal IQueryable<VDElasticPlaylistModel> GetPlaylistsForElasticQueryable()
        {
            var playlistsByGroup = from pv in new PlaylistVideoBusinessObject(UnitOfWork).Get()
                                   join v in new VideoBusinessObject(UnitOfWork).Get() on pv.VideoId equals v.Id
                                   where
                                   (
                                    v.PrivacyId == 1 &&
                                    v.ApproveStatusId == 2
                                   )
                                   group pv by pv.PlaylistId into g
                                   select new
                                   {
                                       PlaylistId = g.Key,
                                       Count = g.Count()
                                   };

            var playlistList = from p in Get()
                               join pg in playlistsByGroup on p.Id equals pg.PlaylistId
                               select new VDElasticPlaylistModel()
                               {
                                   Id = p.Id,
                                   CreatedAt = p.CreatedAt,
                                   Description = p.Description,
                                   IsPublic = p.IsPublic,
                                   Name = p.Name,
                                   ThumbnailUrl = p.Thumbnail,
                                   Count = pg.Count,
                                   UpdatedAt = p.UpdatedAt,
                                   CategoryId = 0,
                                   CategoryName = null
                               };

            return playlistList;
        }

        internal IQueryable<VDElasticPlaylistModel> GetPlaylistsForElasticByCategoryQueryable()
        {
            var playlistCategoryGroup = from v in new VideoBusinessObject(UnitOfWork).Get()
                                        join pv in new PlaylistVideoBusinessObject(UnitOfWork).Get() on v.Id equals pv.VideoId
                                        where
                                        (
                                            v.PrivacyId == 1 &&
                                            v.ApproveStatusId == 2
                                        )
                                        group v by new { v.CategoryId, pv.PlaylistId } into g
                                        select new
                                        {
                                            g.Key.PlaylistId,
                                            g.Key.CategoryId,
                                            Count = g.Count()
                                        };

            var playlistCategory = from x in playlistCategoryGroup
                                   group x by x.PlaylistId into g
                                   let max = g.Max(x => x.Count)
                                   select new
                                   {
                                       PlaylistId = g.Key,
                                       g.FirstOrDefault(x => x.Count == max).CategoryId,
                                       Count = g.Count()
                                   };

            var playlist = from p in GetPlaylistsForElasticQueryable()
                           join pc in playlistCategory on p.Id equals pc.PlaylistId
                           join c in new CategoryBusinessObject(UnitOfWork).Get() on pc.CategoryId.Value equals c.Id
                           select new VDElasticPlaylistModel()
                           {
                               Id = p.Id,
                               CreatedAt = p.CreatedAt,
                               Description = p.Description,
                               IsPublic = p.IsPublic,
                               Name = p.Name,
                               ThumbnailUrl = p.ThumbnailUrl,
                               Count = p.Count,
                               UpdatedAt = p.UpdatedAt,
                               CategoryId = pc.CategoryId ?? 0,
                               CategoryName = c.Title
                           };

            return playlist;
        }

        private List<int> GetPlaylistIdsForElasticSynchronization(DateTime elasticSyncDate)
        {
            var playlists = from p in Get()
                            join pv in new PlaylistVideoBusinessObject(UnitOfWork).Get() on p.Id equals pv.PlaylistId
                            join v in new VideoBusinessObject(UnitOfWork).Get() on pv.VideoId equals v.Id
                            join up in new UserPartnerBusinessObject(UnitOfWork).Get() on v.UserPartnerId equals up.Id
                            join pa in new PartnerBusinessObject(UnitOfWork).Get() on up.PartnerId equals pa.Id
                            join s in new SlugBusinessObject(UnitOfWork).Get() on v.Id equals s.VideoId
                            join vs in new VideoStatisticBusinessObject(UnitOfWork).Get() on v.Id equals vs.VideoId
                            join c in new CategoryBusinessObject(UnitOfWork).Get() on v.CategoryId equals c.Id
                            where
                            (
                               ((p.CreatedAt > elasticSyncDate || p.UpdatedAt > elasticSyncDate) ||
                               (v.CreatedAt > elasticSyncDate || v.UpdatedAt > elasticSyncDate) ||
                               (pa.CreatedAt > elasticSyncDate || pa.UpdatedAt > elasticSyncDate) ||
                               (vs.CreatedAt > elasticSyncDate || vs.UpdatedAt > elasticSyncDate)) &&
                               v.PrivacyId == 1 &&
                               v.ApproveStatusId == 2
                            )
                            group p by p.Id into g
                            select g.Key;

            return playlists.ToList();
        }

        private List<int> GetDeletedPlaylistIdsForElasticSynchronization()
        {
            var playlistIds = from p in Get(null, true)
                              where p.IsActive == false
                              select p.Id;

            return playlistIds.ToList();
        }

        public void SynchronizeElasticPlaylistDetails()
        {
            DateTime updatedAt = DateTime.Now;

            var elasticSearchSyncBusinessObject = new ElasticSearchSyncBusinessObject(UnitOfWork);
            var videoBusinessObject = new VideoBusinessObject(UnitOfWork);
            var elasticSearchSyncEntity = elasticSearchSyncBusinessObject.Get(x => x.Type == "playlist").FirstOrDefault();
            var playlists = GetPlaylistIdsForElasticSynchronization(elasticSearchSyncEntity.UpdatedAt ?? DateTime.MinValue);

            var vdElasticPlaylistModels = GetPlaylistsForElasticByCategoryQueryable().Where(x => playlists.Contains(x.Id)).ToList();
            List<int> videoIds = new List<int>();

            foreach (var playlist in vdElasticPlaylistModels)
            {
                playlist.VideoDetails = videoBusinessObject.GetVideosByPlaylistForElasticQueryable(playlist.Id).ToList();
                foreach (var item in playlist.VideoDetails)
                {
                    videoIds.Add(item.VideoId);
                }
            }
            var pictures = videoBusinessObject.GetVideoCardImages(videoIds).ToList();

            foreach (var playlist in vdElasticPlaylistModels)
            {
                foreach (var item in playlist.VideoDetails)
                {
                    item.VideoCardImageUrl = pictures.Where(x => x.VideoId == item.VideoId).Select(x => x.Url.Replace("http://", "//").Replace("https://", "//")).FirstOrDefault() ?? item.ThumbnailUrl;
                }
            }

            var _client = new VDElasticClient<VDElasticPlaylistModel>().Client;
            var bulkAll = _client.BulkAll(vdElasticPlaylistModels, b => b);

            var waitHandle = new CountdownEvent(1);

            bulkAll.Subscribe(new BulkAllObserver(
                onNext: (b) => { Console.Write("."); },
                onError: (e) => { throw e; },
                onCompleted: () => waitHandle.Signal()
            ));

            waitHandle.Wait();

            var deletedPlaylists = GetDeletedPlaylistIdsForElasticSynchronization();

            var descriptor = new BulkDescriptor();

            foreach (var id in deletedPlaylists)
                descriptor.Delete<VDElasticPlaylistModel>(x => x
                                                            .Id(id))
                                                            .Refresh(Refresh.WaitFor);

            var response = _client.Bulk(descriptor);

            elasticSearchSyncEntity.LastExecutionDateTime = updatedAt;
            elasticSearchSyncEntity.Description = playlists.Count.ToString() + "records";
            elasticSearchSyncBusinessObject.Update(elasticSearchSyncEntity, updatedAt);
            elasticSearchSyncBusinessObject.Commit();
        }
    }
}

