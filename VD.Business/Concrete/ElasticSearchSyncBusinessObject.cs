﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Business.Abstract;
using VD.Core.DataAccess.UnitOfWork;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;
using VD.Entity.Concrete;

namespace VD.Business.Concrete
{
    public class ElasticSearchSyncBusinessObject : VDBusinessObjectBase<ElasticSearchSyncEntity>
    {
        public ElasticSearchSyncBusinessObject()
        {

        }

        public ElasticSearchSyncBusinessObject(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }
}
