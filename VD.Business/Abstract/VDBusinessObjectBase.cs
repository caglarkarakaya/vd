﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Business;
using VD.Core.DataAccess.Repository;
using VD.Core.DataAccess.UnitOfWork;
using VD.Core.Entity;
using VD.DataAccess.Concrete.EntityFramework.UnitOfWork;

namespace VD.Business.Abstract
{
    public class VDBusinessObjectBase<T> : IBusinessObject<T> where T : class, IEntity, new()
    {
        private IUnitOfWork _unitOfWork;
        private IRepository<T> _repository;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                    _unitOfWork = new UnitOfWork();

                return _unitOfWork;
            }
            set
            {
                _unitOfWork = value;
            }
        }

        public IRepository<T> Repository
        {
            get
            {
                if (_repository == null)
                    SetRepository();
                return _repository;
            }
            set
            {
                _repository = value;
            }
        }

        public VDBusinessObjectBase()
        {
        }

        public VDBusinessObjectBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        private void SetRepository()
        {
            Repository = UnitOfWork.GetRepository<T>();
        }

        public T GetById(int id, bool ignoreSoftDelete = false)
        {
            return Get(x => x.Id.Equals(id), ignoreSoftDelete).FirstOrDefault();
        }

        public async Task<T> GetByIdAsync(int id, bool ignoreSoftDelete = false)
        {
            return await Get(x => x.Id.Equals(id), ignoreSoftDelete).FirstOrDefaultAsync();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> filter = null, bool ignoreSoftDelete = false)
        {
            //If the record is soft deleted, it's IsActive field would be set to false.
            //But not all of the tables have soft delete. 
            //So if there is IsActive field in the table we need to fetch the records with the IsActive = true ones.
            if (!ignoreSoftDelete)
            {
                //Since not all of the tables have IsActive field, VDEntityBase class doesn't have IsActive field. 
                //Therefore we need to control if it has this column in the runtime.
                if (typeof(T).GetProperty("IsActive") != null)
                {
                    var item = Expression.Parameter(typeof(T), "x");
                    var property = Expression.Property(item, "IsActive");
                    var value = Expression.Constant(!ignoreSoftDelete);
                    var equal = Expression.Equal(property, value);
                    var lambda = Expression.Lambda<Func<T, bool>>(equal, item);

                    return Repository.Get(filter).Where(lambda);
                }
                else
                {
                    return Repository.Get(filter);
                }
            }
            else
            {
                return Repository.Get(filter);
            }
        }

        public void DeleteById(int id)
        {
            //TODO: Ensure that no exception is thrown if there is no record returned from the get operation.
            T entity = GetById(id);
            Delete(entity);
        }

        public void Delete(T entity)
        {
            //If there is IsActive column in the table the record can be soft deleted. But not all of the tables have soft delete. 
            //So if there is IsActive field on the table we need to set this column to false.
            //Since not all of the tables have IsActive field, VDEntityBase class doesn't have IsActive field. 
            //Therefore we need to control if it has this column in the runtime.
            if (entity.GetType().GetProperty("IsActive") != null)
            {
                entity.GetType().GetProperty("IsActive").SetValue(entity, false);

                if (entity.GetType().GetProperty("UpdatedAt") != null)
                {
                    entity.GetType().GetProperty("UpdatedAt").SetValue(entity, DateTime.Now);
                }

                Repository.Update(entity);
            }
            else
            {
                Repository.Delete(entity);
            }
        }

        public void Delete(Expression<Func<T, bool>> filter = null)
        {
            //TODO: Ensure that no exception is thrown if there is no record returned from the get operation.
            List<T> entityList = Get(filter).ToList();
            foreach (var entity in entityList)
            {
                Delete(entity);
            }
        }

        public async Task DeleteAsync(Expression<Func<T, bool>> filter = null)
        {
            //TODO: Ensure that no exception is thrown if there is no record returned from the get operation.
            List<T> entityList = await Get(filter).ToListAsync();
            foreach (var entity in entityList)
            {
                Delete(entity);
            }
        }

        public T Insert(T entity)
        {
            if (entity.GetType().GetProperty("CreatedAt") != null)
            {
                entity.GetType().GetProperty("CreatedAt").SetValue(entity, DateTime.Now);
            }

            return Repository.Insert(entity);
        }

        public T Update(T entity, DateTime? updatedAt = null)
        {
            if (entity.GetType().GetProperty("UpdatedAt") != null)
            {
                entity.GetType().GetProperty("UpdatedAt").SetValue(entity, updatedAt ?? DateTime.Now);
            }

            return Repository.Update(entity);
        }

        public void Commit()
        {
            UnitOfWork.Save();
        }

        public async Task<int> CommitAsync()
        {
            return await UnitOfWork.SaveAsync();
        }
    }
}
