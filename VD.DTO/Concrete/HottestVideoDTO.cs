﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace VD.DTO.Concrete
{
    public class HottestVideoDTO
    {
        public string Title { get; set; }
        public string SlugKey { get; set; }
        public string ThumbnailUrl { get; set; }
        public string VideoCardImageUrl { get; set; }
        public int VideoId { get; set; }
        public double? Duration { get; set; }
        public DateTime CreatedAt { get; set; }
        public long ViewCount { get; set; }
        public string VideoMD5 { get; set; }
    }
}
