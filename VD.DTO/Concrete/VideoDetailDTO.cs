﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class VideoDetailDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string SlugKey { get; set; }
        public string ThumbnailUrl { get; set; }
        public string VideoCardImageUrl { get; set; }
        public int VideoId { get; set; }
        public double? Duration { get; set; }
        public DateTime CreatedAt { get; set; }
        public long ViewCount { get; set; }
        public string VideoMD5 { get; set; }
        public int? CategoryId { get; set; }
        public int ChannelId { get; set; }
        public string CategoryName { get; set; }
        public string ChannelName { get; set; }
        public string ChannelAvatarUrl { get; set; }
        public List<VideoFileDTO> VideoFiles { get; set; }
    }
}
