﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class PlaylistWithCategoryDTO : PlaylistInfoDTO
    {
        public int CategoryId { get; set; }
    }
}
