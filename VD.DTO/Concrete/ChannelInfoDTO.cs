﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class ChannelInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int VideoCount { get; set; }
        public long ViewCount { get; set; }
        public string CardImageUrl { get; set; }
        public string BackgroundImageUrl { get; set; }
        public string AvatarUrl { get; set; }
        public string Description { get; set; }
    }
}
