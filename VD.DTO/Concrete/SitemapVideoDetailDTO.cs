﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class SitemapVideoDetailDTO
    {
        public int VideoId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SlugKey { get; set; }
        public string ThumbnailUrl { get; set; }
        public double? Duration { get; set; }
        public VideoFileDTO VideoFile { get; set; }
    }
}
