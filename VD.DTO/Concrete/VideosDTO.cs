﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class VideosDTO : VDDTOBase
    {
        public List<VideoInfoDTO> VideoInfos { get; set; }
    }
}
