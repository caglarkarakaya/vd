﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class VideoPictureDTO
    {
        public int VideoId { get; set; }
        public string Url { get; set; }
    }
}
