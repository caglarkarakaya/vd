﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string IconUrl { get; set; }
        public int VideoCount { get; set; }
    }
}
