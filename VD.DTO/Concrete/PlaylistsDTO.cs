﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class PlaylistsDTO : VDDTOBase
    {
        public List<PlaylistInfoDTO> PlaylistInfos { get; set; }
    }
}
