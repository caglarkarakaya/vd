﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VD.DTO.Concrete
{
    public class ChannelsDTO : VDDTOBase
    {
        public List<ChannelInfoDTO> ChannelInfos { get; set; }
    }
}
