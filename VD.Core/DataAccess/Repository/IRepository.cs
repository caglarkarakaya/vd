﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Entity;

namespace VD.Core.DataAccess.Repository
{
    public interface IRepository<T> where T : class, IEntity, new()
    {
        IQueryable<T> Get(Expression<Func<T, bool>> filter = null);
        T Insert(T entity);
        T Update(T entity);
        void Delete(T entity);
    }
}
