﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VD.Core.DataAccess.Repository;
using VD.Core.Entity;

namespace VD.Core.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        void Dispose();
        void Save();
        Task<int> SaveAsync();
        IRepository<T> GetRepository<T>() where T : class, IEntity, new();
    }
}
