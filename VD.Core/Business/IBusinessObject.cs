﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VD.Core.Entity;

namespace VD.Core.Business
{
    public interface IBusinessObject<T> where T : class, IEntity, new()
    {
        T GetById(int id, bool ignoreSoftDelete = false);
        Task<T> GetByIdAsync(int id, bool ignoreSoftDelete = false);
        IQueryable<T> Get(Expression<Func<T, bool>> filter = null, bool ignoreSoftDelete = false);
        T Insert(T entity);
        T Update(T entity, DateTime? updatedAt = null);
        void DeleteById(int id);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> filter = null);
        Task DeleteAsync(Expression<Func<T, bool>> filter = null);
        void Commit();
        Task<int> CommitAsync();
    }
}
